<?php
/**
 * Created by NiNaCoder.
 * Date: 2019-05-24
 * Time: 13:24
 */

namespace App;

use App\Scopes\PublishedScope;
use App\Scopes\VisibilityScope;
use App\Traits\SanitizedRequest;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
use Carbon\Carbon;

class Artist extends Model implements HasMedia
{
    use InteractsWithMedia, SanitizedRequest;

    protected $fillable = ['genre', 'mood', 'title', 'artworkId', 'bio', 'name'];

    protected $appends = ['artwork_url', 'permalink_url'];

    protected $hidden = ['media', 'bio', 'visibility', 'created_at', 'updated_at'];

    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope(new VisibilityScope);
    }

    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('sm')
            ->width(60)
            ->height(60)
            ->performOnCollections('artwork')->nonQueued();

        $this->addMediaConversion('md')
            ->width(120)
            ->height(120)
            ->performOnCollections('artwork')->nonQueued();

        $this->addMediaConversion('lg')
            ->width(300)
            ->height(300)
            ->performOnCollections('artwork')->nonQueued();
    }

    public function getArtworkUrlAttribute($value)
    {
        $media = $this->getFirstMedia('artwork');
        if(! $media) {
            return asset( 'common/default/artist.png');
        } else {
            if($media->disk == 's3') {
                return $media->getTemporaryUrl(Carbon::now()->addMinutes(intval(config('settings.s3_signed_time', 5))),'lg');
            } else {
                return $media->getFullUrl('lg');
            }
        }
    }

    public function getMoodsAttribute()
    {
        return Mood::whereIn('id', explode(',', $this->attributes['mood']))->get();
    }

    public function getGenresAttribute($value)
    {

        $this->attributes['genres'] = Genre::whereIn('id', explode(',', $this->attributes['genre']))->get();

        return $this->attributes['genres'];
    }

    public function getPermalinkUrlAttribute($value)
    {
        return route('frontend.artist', ['id' => $this->attributes['id'], 'slug' => str_slug($this->attributes['name'])]);
    }

    public function getSongCountAttribute($value)
    {
        return Song::where('artistIds', 'REGEXP', '(^|,)(' . $this->id . ')(,|$)')->count();
    }

    public function getAlbumCountAttribute($value)
    {
        return Album::where('artistIds', 'REGEXP', '(^|,)(' . $this->id . ')(,|$)')->count();
    }

    public function getLovedAttribute()
    {
        if(auth()->user()) {
            return $this->morphOne(Love::class, 'loveable')->where('user_id', auth()->user()->id) ? true : false;
        }
        return false;
    }

    public function getFavoriteAttribute($value) {
        if(auth()->check()){
            return Love::where('user_id', auth()->user()->id)->where('loveable_id', $this->id)->where('loveable_type', $this->getMorphClass())->exists();
        } else {
            return false;
        }
    }

    public function songs()
    {
        return Song::where('artistIds', 'REGEXP', '(^|,)(' . $this->id . ')(,|$)');
    }

    public function albums()
    {
        return Album::where('artistIds', 'REGEXP', '(^|,)(' . $this->id . ')(,|$)');
    }

    public function similar()
    {
        return Artist::whereIn('genre', explode(',', $this->genre));
    }

    public function comments()
    {
        return $this->morphMany(Comment::class, 'commentable');
    }

    public function followers()
    {
        return $this->belongsToMany(User::class, 'love', 'loveable_id', 'user_id')->where('loveable_type', 'App\Artist');
    }

    public function activities(){
        return Activity::where('activityable_type', 'App\Artist')
            ->where('activityable_id', $this->id)
            ->where('action', '!=', 'addEvent');
    }

    public function events(){
        return $this->hasMany(Event::class);
    }

    public function delete()
    {
        Comment::withoutGlobalScopes()->where('commentable_type', $this->getMorphClass())->where('commentable_id', $this->id)->delete();
        Love::withoutGlobalScopes()->where('loveable_type', $this->getMorphClass())->where('loveable_id', $this->id)->delete();
        User::withoutGlobalScopes()->where('artist_id', $this->id)->update(['artist_id' => null]);
        ArtistRequest::withoutGlobalScopes()->where('artist_id', $this->id)->delete();
        Activity::where('activityable_type', $this->getMorphClass())->where('activityable_id', $this->id)->delete();

        return parent::delete();
    }
}