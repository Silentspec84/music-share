<?php
/**
 * Created by NiNaCoder.
 * Date: 2019-05-24
 * Time: 13:24
 */

namespace App;

use App\Traits\SanitizedRequest;
use Illuminate\Database\Eloquent\Model;

class ArtistRequest extends Model
{
    use SanitizedRequest;

    protected $table = 'artist_requests';

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function artist()
    {
        return $this->hasOne(Artist::class, 'id', 'artist_id');
    }
}