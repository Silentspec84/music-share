<?php
/**
 * Created by NiNaCoder.
 * Date: 2019-05-25
 * Time: 21:22
 */

namespace App;

use App\Traits\SanitizedRequest;
use Illuminate\Database\Eloquent\Model;
use DB;

class Channel extends Model
{
    use SanitizedRequest;

    protected $appends = ['objects'];

    public function getObjectsAttribute($value)
    {
        if ($this->attributes['object_type'] == "artist") {
            return Artist::whereIn('id', explode(',', $this->attributes['object_ids']))->orderBy(DB::raw('FIELD(id, ' .  $this->attributes['object_ids']. ')', 'FIELD'))->paginate(20);
        } elseif ($this->attributes['object_type'] == "song") {
            return Song::whereIn('id', explode(',', $this->attributes['object_ids']))->orderBy(DB::raw('FIELD(id, ' .  $this->attributes['object_ids']. ')', 'FIELD'))->paginate(20);
        } elseif ($this->attributes['object_type'] == "playlist") {
            return Playlist::with('user')->whereIn('id', explode(',', $this->attributes['object_ids']))->orderBy(DB::raw('FIELD(id, ' .  $this->attributes['object_ids']. ')', 'FIELD'))->paginate(20);
        } elseif ($this->attributes['object_type'] == "album") {
            return Album::whereIn('id', explode(',', $this->attributes['object_ids']))->orderBy(DB::raw('FIELD(id, ' .  $this->attributes['object_ids']. ')', 'FIELD'))->paginate(20);
        } elseif ($this->attributes['object_type'] == "station") {
            return Station::whereIn('id', explode(',', $this->attributes['object_ids']))->orderBy(DB::raw('FIELD(id, ' .  $this->attributes['object_ids']. ')', 'FIELD'))->paginate(20);
        } elseif ($this->attributes['object_type'] == "user") {
            return User::whereIn('id', explode(',', $this->attributes['object_ids']))->orderBy(DB::raw('FIELD(id, ' .  $this->attributes['object_ids']. ')', 'FIELD'))->paginate(20);
        }
    }

}