<?php

namespace App;

use App\Traits\SanitizedRequest;
use Illuminate\Database\Eloquent\Model;

class HashTag extends Model
{
    use SanitizedRequest;

    protected $table = 'hash_tags';
}