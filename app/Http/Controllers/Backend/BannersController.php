<?php
/**
 * Created by NiNaCoder.
 * Date: 2019-05-25
 * Time: 09:01
 */

namespace App\Http\Controllers\Backend;

use Carbon\Carbon;
use Illuminate\Http\Request;
use DB;
use View;
use App\Banner;
use Image;
use Cache;

class BannersController
{
    private $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function index(Request $request)
    {
        $banners = Banner::paginate(20);

        return view('backend.banners.index')->with('banners', $banners);
    }

    public function delete()
    {
        Banner::where('id', '=', $this->request->route('id'))->delete();
        return redirect()->back()->with('status', 'success')->with('message', 'banners successfully deleted!');
    }

    public function add()
    {
        return view('backend.banners.form');
    }

    public function addPost()
    {
        $this->request->validate([
            'banner_tag' => 'required|string|alpha_dash|regex:/^[a-z0-9_]+$/|min:4|max:30',
            'description' => 'required|string',
            'started_at' => 'nullable|date_format:Y/m/d H:i',
            'ended_at' => 'nullable|date_format:Y/m/d H:i|after:' . Carbon::now(),
            'code' => 'nullable|string',
        ]);

        $banner = new Banner();

        $banner->banner_tag = $this->request->input('banner_tag');
        $banner->description= $this->request->input('description');

        if($this->request->input('started_at'))
        {
            $banner->started_at = Carbon::parse($this->request->input('started_at'));
        }

        if($this->request->input('ended_at'))
        {
            $banner->ended_at = Carbon::parse($this->request->input('ended_at'));
        }

        $banner->approved = $this->request->input('disabled') ? 0 : 1;
        $banner->code = $this->request->input('code');
        $banner->save();

        Cache::forget('banners');

        return redirect()->route('backend.banners')->with('status', 'success')->with('message', 'Banner successfully added!');
    }

    public function edit()
    {
        $banner = Banner::withoutGlobalScopes()->findOrFail($this->request->route('id'));
        return view('backend.banners.form')
            ->with('banner', $banner);
    }

    public function editPost()
    {
        $this->request->validate([
            'banner_tag' => 'required|string|alpha_dash|regex:/^[a-z0-9_]+$/|min:4|max:30',
            'description' => 'required|string',
            'started_at' => 'nullable|date_format:Y/m/d H:i',
            'ended_at' => 'nullable|date_format:Y/m/d H:i|after:' . Carbon::now(),
            'code' => 'nullable|string',
        ]);

        $banner = Banner::findOrFail($this->request->route('id'));

        $banner->banner_tag = $this->request->input('banner_tag');
        $banner->description= $this->request->input('description');

        if($this->request->input('started_at'))
        {
            $banner->started_at = Carbon::parse($this->request->input('started_at'));
        }

        if($this->request->input('ended_at'))
        {
            $banner->ended_at = Carbon::parse($this->request->input('ended_at'));
        }

        $banner->approved = $this->request->input('disabled') ? 0 : 1;
        $banner->code = $this->request->input('code');
        $banner->save();

        Cache::forget('banners');

        return redirect()->route('backend.banners')->with('status', 'success')->with('message', 'Banner successfully edited!');
    }

    public function trackList()
    {
        $album = Album::withoutGlobalScopes()->findOrFail($this->request->route('id'));
        $album->setRelation('songs', $album->songs()->withoutGlobalScopes()->get());
        return view('backend.albums.tracklist')
            ->with('album', $album);
    }

    public function trackListMassAction()
    {
        if( ! $this->request->input('action') ) {
            $songIds = $this->request->input('songIds');
            foreach ($songIds as $index => $songId) {
                DB::table('album_songs')
                    ->where('album_id', $this->request->route('id'))
                    ->where('song_id', $songId)
                    ->update(['priority' => $index]);
            }
            return redirect()->back()->with('status', 'success')->with('message', 'Priority successfully changed!');
        } else {
            $this->request->validate([
                'action' => 'required|string',
                'ids' => 'required|array',
            ]);

            if($this->request->input('action') == 'remove_from_album') {
                $ids = $this->request->input('ids');
                foreach($ids as $id) {
                    $song = Song::withoutGlobalScopes()->where('id', $id)->first();
                    DB::table('album_songs')->where('song_id', $song->id)->delete();
                }
                return redirect()->back()->with('status', 'success')->with('message', 'Songs successfully removed from the album!');
            } else if($this->request->input('action') == 'delete') {
                $ids = $this->request->input('ids');
                foreach($ids as $id) {
                    $song = Song::withoutGlobalScopes()->where('id', $id)->first();
                    $song->delete();
                }
                return redirect()->back()->with('status', 'success')->with('message', 'Songs successfully deleted!');
            }
        }

    }

    public function upload()
    {
        $album = Album::withoutGlobalScopes()->findOrFail($this->request->route('id'));
        return view('backend.albums.upload')
            ->with('album', $album);;
    }

    public function reject()
    {
        $this->request->validate([
            'comment' => 'nullable|string',
        ]);
        $album = Album::withoutGlobalScopes()->findOrFail($this->request->route('id'));

        (new Email)->rejectedAlbum($album->user, $album, $this->request->input('comment'));

        Album::withoutGlobalScopes()->where('id', $this->request->route('id'))->delete();
        return redirect()->route('backend.albums')->with('status', 'success')->with('message', 'Album successfully rejected!');
    }

    public function massAction()
    {
        $this->request->validate([
            'action' => 'required|string',
            'ids' => 'required|array',
        ]);

        if($this->request->input('action') == 'add_genre') {
            $message = 'Add genre';
            $subMessage = 'Add Genre for Chosen Albums (<strong>' . count($this->request->input('ids')) . '</strong>)';
            return view('backend.commons.mass_genre')
                ->with('message', $message)
                ->with('subMessage', $subMessage)
                ->with('action', $this->request->input('action'))
                ->with('ids', $this->request->input('ids'));
        } else if($this->request->input('action') == 'save_add_genre') {
            $ids = $this->request->input('ids');
            foreach($ids as $id) {
                $album = Album::find($id);
                if(isset($album->id)){
                    $currentGenre = explode(',', $album->genre);
                    $newGenre = array_unique(array_merge($currentGenre, $this->request->input('genre')));
                    $album->genre = implode(',', $newGenre);
                    $album->save();
                }
            }
            return redirect()->route('backend.albums')->with('status', 'success')->with('message', 'Albums successfully saved!');
        } elseif($this->request->input('action') == 'change_genre') {
            $message = 'Change genre';
            $subMessage = 'Change Genre for Chosen Albums (<strong>' . count($this->request->input('ids')) . '</strong>)';
            return view('backend.commons.mass_genre')
                ->with('message', $message)
                ->with('subMessage', $subMessage)
                ->with('action', $this->request->input('action'))
                ->with('ids', $this->request->input('ids'));
        } else if($this->request->input('action') == 'save_change_genre') {
            $ids = $this->request->input('ids');
            foreach($ids as $id) {
                $album = Album::withoutGlobalScopes()->find($id);
                if(isset($album->id)){
                    $album->genre = implode(',', $this->request->input('genre'));
                    $album->save();
                }
            }
            return redirect()->route('backend.albums')->with('status', 'success')->with('message', 'Albums successfully saved!');
        } elseif($this->request->input('action') == 'add_mood') {
            $message = 'Add mood';
            $subMessage = 'Add Mood for Chosen Albums (<strong>' . count($this->request->input('ids')) . '</strong>)';
            return view('backend.commons.mass_mood')
                ->with('message', $message)
                ->with('subMessage', $subMessage)
                ->with('action', $this->request->input('action'))
                ->with('ids', $this->request->input('ids'));
        } else if($this->request->input('action') == 'save_add_mood') {
            $ids = $this->request->input('ids');
            foreach($ids as $id) {
                $album = Album::withoutGlobalScopes()->find($id);
                if(isset($album->id)){
                    $currentMood = explode(',', $album->mood);
                    $newMood = array_unique(array_merge($currentMood, $this->request->input('genre')));
                    $album->mood = implode(',', $newMood);
                    $album->save();
                }
            }
            return redirect()->route('backend.albums')->with('status', 'success')->with('message', 'Albums successfully saved!');
        } elseif($this->request->input('action') == 'change_mood') {
            $message = 'Change mood';
            $subMessage = 'Change Mood for Chosen Albums (<strong>' . count($this->request->input('ids')) . '</strong>)';
            return view('backend.commons.mass_mood')
                ->with('message', $message)
                ->with('subMessage', $subMessage)
                ->with('action', $this->request->input('action'))
                ->with('ids', $this->request->input('ids'));
        } else if($this->request->input('action') == 'save_change_mood') {
            $ids = $this->request->input('ids');
            foreach($ids as $id) {
                $album = Album::withoutGlobalScopes()->find($id);
                if(isset($album->id)){
                    $album->mood = implode(',', $this->request->input('mood'));
                    $album->save();
                }
            }
            return redirect()->route('backend.albums')->with('status', 'success')->with('message', 'Albums successfully saved!');
        } elseif($this->request->input('action') == 'change_artist') {
            $message = 'Change artist';
            $subMessage = 'Change Album for Chosen Albums (<strong>' . count($this->request->input('ids')) . '</strong>)';
            return view('backend.commons.mass_artist')
                ->with('message', $message)
                ->with('subMessage', $subMessage)
                ->with('action', $this->request->input('action'))
                ->with('ids', $this->request->input('ids'));
        } else if($this->request->input('action') == 'save_change_artist') {
            $ids = $this->request->input('ids');
            foreach($ids as $id) {
                $album = Album::withoutGlobalScopes()->find($id);
                $artistIds = $this->request->input('artistIds');
                if(isset($album->id)){
                    if(is_array($artistIds))
                    {
                        $album->artistIds = implode(",", $this->request->input('artistIds'));
                    }
                    $album->save();
                }
            }
            return redirect()->route('backend.albums')->with('status', 'success')->with('message', 'Albums successfully saved!');
        } else if($this->request->input('action') == 'approve') {
            $ids = $this->request->input('ids');
            foreach($ids as $id) {
                $album = Album::withoutGlobalScopes()->find($id);
                if(isset($album->id)){
                    $album->approved = 1;
                    $album->save();
                }
            }
            return redirect()->route('backend.albums')->with('status', 'success')->with('message', 'Albums successfully saved!');
        } else if($this->request->input('action') == 'not_approve') {
            $ids = $this->request->input('ids');
            foreach($ids as $id) {
                $album = Album::withoutGlobalScopes()->find($id);
                if(isset($album->id)){
                    $album->approved = 0;
                    $album->save();
                }
            }
            return redirect()->route('backend.albums')->with('status', 'success')->with('message', 'Albums successfully saved!');
        } else if($this->request->input('action') == 'comments') {
            $ids = $this->request->input('ids');
            foreach($ids as $id) {
                $album = Album::withoutGlobalScopes()->find($id);
                if(isset($album->id)){
                    $album->allow_comments = 1;
                    $album->save();
                }
            }
            return redirect()->route('backend.albums')->with('status', 'success')->with('message', 'Albums successfully saved!');
        } else if($this->request->input('action') == 'not_comments') {
            $ids = $this->request->input('ids');
            foreach($ids as $id) {
                $album = Album::withoutGlobalScopes()->find($id);
                if(isset($album->id)){
                    $album->allow_comments = 0;
                    $album->save();
                }
            }
            return redirect()->route('backend.albums')->with('status', 'success')->with('message', 'Albums successfully saved!');
        } else if($this->request->input('action') == 'clear_count') {
            $ids = $this->request->input('ids');
            foreach($ids as $id) {
                $album = Album::withoutGlobalScopes()->find($id);
                if(isset($album->id)){
                    $album->plays = 0;
                    $album->save();
                }
            }
            return redirect()->route('backend.albums')->with('status', 'success')->with('message', 'Albums successfully saved!');
        } else if($this->request->input('action') == 'delete') {
            $ids = $this->request->input('ids');
            foreach($ids as $id) {
                $album = Album::withoutGlobalScopes()->where('id', $id)->first();
                $album->delete();
            }
            return redirect()->route('backend.albums')->with('status', 'success')->with('message', 'Albums successfully deleted!');
        }
    }
}