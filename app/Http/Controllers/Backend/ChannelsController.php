<?php
/**
 * Created by NiNaCoder.
 * Date: 2019-05-25
 * Time: 20:58
 */

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use DB;
use App\Channel;
use App\Artist;
use App\Album;
use App\Playlist;
use App\User;
use App\Song;
use App\Station;
use App\Genre;
use App\Mood;
use App\Radio;
use Carbon\Carbon;
use Auth;
use Cache;

class ChannelsController
{
    private $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function index()
    {
        $channels = DB::table('channels')->select('*')->orderBy('priority', 'asc');

        if($this->request->route()->getName() == 'backend.channels.home')
        {
            $channels = $channels->where('allow_home', 1);
        }

        if($this->request->route()->getName() == 'backend.channels.discover')
        {
            $channels = $channels->where('allow_discover', 1);
        }

        if($this->request->route()->getName() == 'backend.channels.radio')
        {
            $channels = $channels->where('allow_radio', 1);
        }

        if($this->request->route()->getName() == 'backend.channels.community')
        {
            $channels = $channels->where('allow_community', 1);
        }

        if($this->request->route()->getName() == 'backend.channels.trending')
        {
            $channels = $channels->where('allow_trending', 1);
        }

        if($this->request->route()->getName() == 'backend.channels.genre')
        {
            $channels = $channels->whereRaw("genre REGEXP '(^|,)(" . $this->request->route('id') . ")(,|$)'");
        }

        if($this->request->route()->getName() == 'backend.channels.mood')
        {
            $channels = $channels->whereRaw("mood REGEXP '(^|,)(" . $this->request->route('id') . ")(,|$)'");
        }

        if($this->request->route()->getName() == 'backend.channels.station-category')
        {
            $channels = $channels->whereRaw("radio REGEXP '(^|,)(" . $this->request->route('id') . ")(,|$)'");
        }

        $channels = $channels->get();

        $genres = (new Genre)->get();

        $moods = (new Mood)->get();

        $radio = (new Radio)->get();

        return view('backend.channels.index')
            ->with('channels', $channels)
            ->with('genres', $genres)
            ->with('moods', $moods)
            ->with('radio', $radio);

    }

    public function sort()
    {
        $object_ids = $this->request->input('object_ids');

        foreach ($object_ids AS $index => $album_id) {
            DB::table('channels')
                ->where('id', $album_id)
                ->update(['priority' => $index + 1]);
        }

        return redirect()->route('backend.channels.overview')->with('status', 'success')->with('message', 'Priority successfully changed!');
    }

    public function delete()
    {
        DB::table('channels')
            ->where('id', $this->request->route('id'))
            ->delete();

        return redirect()->route('backend.channels.overview')->with('status', 'success')->with('message', 'Channel successfully deleted!');
    }

    public function add()
    {
        return view('backend.channels.form');
    }

    public function addPost()
    {
        $this->request->validate([
            'title' => 'required|string|max:255',
            'object_type' => 'required',
            'object_ids' => 'required|array',
            'meta_title' => 'nullable|string|max:255',
            'visibility' => 'required|boolean',
            'allow_home' => 'required|boolean',
            'allow_discover' => 'required|boolean',
            'allow_radio' => 'required|boolean',
            'allow_community' => 'required|boolean',
            'allow_trending' => 'required|boolean',
        ]);

        $title = $this->request->input('title');
        $description = $this->request->input('description');
        $object_type = $this->request->input('object_type');
        $object_ids = implode(',', $this->request->input('object_ids'));
        $meta_title = $this->request->input('meta_title');
        $meta_description = $this->request->input('meta_description');
        $visibility = $this->request->input('visibility');
        $allow_home = $this->request->input('allow_home');
        $allow_discover = $this->request->input('allow_discover');
        $allow_radio = $this->request->input('allow_radio');
        $allow_community = $this->request->input('allow_community');
        $allow_trending = $this->request->input('allow_trending');

        $genre = $this->request->input('genre');

        if(is_array($genre))
        {
            $genre = implode(",", $this->request->input('genre'));

        }

        $mood = $this->request->input('mood');

        if(is_array($mood))
        {
            $mood = implode(",", $this->request->input('mood'));

        }

        $radio = $this->request->input('radio');

        if(is_array($radio))
        {
            $radio = implode(",", $this->request->input('radio'));

        }

        DB::table('channels')
            ->insert([
                'user_id' => auth()->user()->id,
                'title' => $title,
                'description' => $description,
                'alt_name' => str_slug($title),
                'object_type' => $object_type,
                'object_ids' => $object_ids,
                'meta_description' => $meta_description,
                'meta_title' => $meta_title,
                'visibility' => $visibility,
                'allow_home' => $allow_home,
                'allow_radio' => $allow_radio,
                'allow_discover' => $allow_discover,
                'allow_community' => $allow_community,
                'allow_trending' => $allow_trending,
                'genre' => $genre,
                'mood' => $mood,
                'radio' => $radio,
                'created_at' => Carbon::now()->format('Y/m/d H:i:s'),
                'updated_at' => Carbon::now()->format('Y/m/d H:i:s'),
            ]);

        /**
         * Clear homage cache
         */
        Cache::clear('homepage');

        return redirect()->route('backend.channels.overview')->with('status', 'success')->with('message', 'Channel successfully added!');
    }

    public function edit()
    {
        $channel = DB::table('channels')->select('*')->where('id', $this->request->route('id'))->first();

        if(! isset($channel->id)) abort(404);

        $songs = (object) array();
        $artists = (object) array();
        $playlists = (object) array();
        $albums = (object) array();
        $stations = (object) array();
        $users = (object) array();

        if($channel->object_type == "artist"){
            $artists = Artist::whereIn('id', explode(',', $channel->object_ids))->orderBy(DB::raw('FIELD(id, ' .  $channel->object_ids. ')', 'FIELD'))->get();
        } elseif ($channel->object_type == "song"){
            $songs =  Song::whereIn('id', explode(',', $channel->object_ids))->orderBy(DB::raw('FIELD(id, ' .  $channel->object_ids. ')', 'FIELD'))->get();
        } elseif ($channel->object_type == "playlist"){
            $playlists = Playlist::whereIn('id', explode(',', $channel->object_ids))->orderBy(DB::raw('FIELD(id, ' .  $channel->object_ids. ')', 'FIELD'))->get();
        } elseif ($channel->object_type == "album"){
            $albums = Album::whereIn('id', explode(',', $channel->object_ids))->orderBy(DB::raw('FIELD(id, ' .  $channel->object_ids. ')', 'FIELD'))->get();
        } elseif ($channel->object_type == "station"){
            $stations = Station::whereIn('id', explode(',', $channel->object_ids))->orderBy(DB::raw('FIELD(id, ' .  $channel->object_ids. ')', 'FIELD'))->get();
        } elseif ($channel->object_type == "user"){
            $users = User::whereIn('id', explode(',', $channel->object_ids))->orderBy(DB::raw('FIELD(id, ' .  $channel->object_ids. ')', 'FIELD'))->get();
        }

        return view('backend.channels.form')
            ->with('channel', $channel)
            ->with('artists', $artists)
            ->with('songs', $songs)
            ->with('playlists', $playlists)
            ->with('stations', $stations)
            ->with('albums', $albums)
            ->with('users', $users);
    }

    public function editPost()
    {
        $this->request->validate([
            'title' => 'required|string|max:255',
            'object_type' => 'required',
            'object_ids' => 'required|array',
            'meta_title' => 'nullable|string|max:255',
            'visibility' => 'required|boolean',
            'allow_home' => 'required|boolean',
            'allow_discover' => 'required|boolean',
            'allow_radio' => 'required|boolean',
            'allow_community' => 'required|boolean',
            'allow_trending' => 'required|boolean',
        ]);

        $title = $this->request->input('title');
        $description = $this->request->input('description');
        $object_type = $this->request->input('object_type');
        $object_ids = implode(',', $this->request->input('object_ids'));
        $meta_title = $this->request->input('meta_title');
        $meta_description = $this->request->input('meta_description');
        $visibility = $this->request->input('visibility');
        $allow_home = $this->request->input('allow_home');
        $allow_discover = $this->request->input('allow_discover');
        $allow_radio = $this->request->input('allow_radio');
        $allow_community = $this->request->input('allow_community');
        $allow_trending = $this->request->input('allow_trending');

        $genre = $this->request->input('genre');

        if(is_array($genre))
        {
            $genre = implode(",", $this->request->input('genre'));

        }

        $mood = $this->request->input('mood');

        if(is_array($mood))
        {
            $mood = implode(",", $this->request->input('mood'));

        }

        $radio = $this->request->input('radio');

        if(is_array($radio))
        {
            $radio = implode(",", $this->request->input('radio'));

        }

        DB::table('channels')
            ->where('id', $this->request->route('id'))
            ->update([
                'title' => $title,
                'description' => $description,
                'alt_name' => str_slug($title),
                'object_type' => $object_type,
                'object_ids' => $object_ids,
                'meta_description' => $meta_description,
                'meta_title' => $meta_title,
                'visibility' => $visibility,
                'allow_home' => $allow_home,
                'allow_radio' => $allow_radio,
                'allow_discover' => $allow_discover,
                'allow_community' => $allow_community,
                'allow_trending' => $allow_trending,
                'genre' => $genre,
                'mood' => $mood,
                'radio' => $radio,
                'updated_at' => Carbon::now()->format('Y/m/d H:i:s'),
            ]);

        /**
         * Clear homage cache
         */
        Cache::clear('homepage');

        return redirect()->route('backend.channels.overview')->with('status', 'success')->with('message', 'Channel successfully added!');
    }
}