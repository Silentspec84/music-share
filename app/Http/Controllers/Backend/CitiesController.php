<?php
/**
 * Created by NiNaCoder.
 * Date: 2019-05-26
 * Time: 10:54
 */

namespace App\Http\Controllers\Backend;

use Carbon\Carbon;
use Illuminate\Http\Request;
use DB;
use App\City;
use App\Country;
use Image;

class CitiesController
{
    private $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function index() {

        $cities = City::withoutGlobalScopes();

        if ($this->request->has('term'))
        {
            $cities = $cities->where('name', 'like', '%' . $this->request->input('term') . '%');
        }

        if ($this->request->has('fixed'))
        {
            $cities = $cities->where('fixed', '=', 1);
        }

        if ($this->request->has('hidden'))
        {
            $cities = $cities->where('visibility', '=', 0);
        }

        if ($this->request->has('country'))
        {
            $cities = $cities->where('country_code', '=', $this->request->input('country'));
        }

        if ($this->request->has('name'))
        {
            $cities = $cities->orderBy('name', $this->request->input('name'));
        }

        if ($this->request->has('results_per_page'))
        {
            $cities = $cities->paginate(intval($this->request->input('results_per_page')));
        } else {
            $cities = $cities->orderBy('fixed', 'desc')->paginate(20);
        }

        $governmentForms = [];
        foreach(Country::select('government_form')->groupBy('government_form')->get() as $country) {
            $governmentForms = array_merge($governmentForms, array(
                "{$country->government_form}" => $country->government_form
            ));
        }

        return view('backend.cities.index')
            ->with('cities', $cities)
            ->with('governmentForms', $governmentForms);
    }

    public function add()
    {
        return view('backend.cities.form');
    }

    public function addPost()
    {
        $this->request->validate([
            'title' => 'required|string',
            'category' => 'required|int',
            'streamUrl' => 'required|string',
        ]);

        $uploader = artworkUploader($this->request, "radio");

        if ($uploader->success == true)
        {
            $title = $this->request->input('title');
            $category = $this->request->input('category');
            $description = $this->request->input('description');
            $streamUrl = $this->request->input('streamUrl');

            DB::table('stations')
                ->insert([
                    'title' => $title,
                    'category' => $category,
                    'description' => $description,
                    'streamUrl' => $streamUrl,
                    'artworkId' => $uploader->artworkId
                ]);

            return redirect('admin/radios/' . $this->request->route('id') . '/stations')->with('status', 'success')->with('message', 'Station successfully added!');
        }
    }

    public function edit()
    {
        $country = City::where('code', '=', $this->request->route('code'))->firstOrFail();;

        return view('backend.cities.form')->with('country', $country);
    }

    public function editPost()
    {
        $this->request->validate([
            'title' => 'required|string',
            'stream_url' => 'required|string',
        ]);

        $country = City::where('code', '=', $this->request->route('code'))->firstOrFail();;


        $station->title = $this->request->input('title');
        $station->description = $this->request->input('description');
        $station->stream_url = $this->request->input('stream_url');
        $station->country_code = $this->request->input('country_code');
        $station->city_id = $this->request->input('city_id');
        $station->language_id = $this->request->input('language_id');

        if ($this->request->hasFile('artwork'))
        {
            $this->request->validate([
                'artwork' => 'required|image|mimes:jpeg,png,jpg,gif|max:' . config('settings.max_image_file_size')
            ]);

            $station->clearMediaCollection('artwork');
            $station->addMediaFromBase64(base64_encode(Image::make($this->request->file('artwork'))->fit(intval(config('settings.image_artwork_max', 500)),  intval(config('settings.image_artwork_max', 500)))->encode('jpg', config('settings.image_jpeg_quality', 90))->encoded))
                ->usingFileName(time(). '.jpg')
                ->toMediaCollection('artwork', config('settings.storage_artwork_location', 'public'));
        }

        $station->save();

        return redirect()->route('backend.stations')->with('status', 'success')->with('message', 'Station successfully edited!');
    }

    public function delete()
    {
        return redirect('admin/radios/' . $this->request->route('id') . '/stations')->with('status', 'success')->with('message', 'Station successfully deleted!');
    }
}