<?php
/**
 * Created by NiNaCoder.
 * Date: 2019-05-24
 * Time: 20:12
 */

namespace App\Http\Controllers\Backend;

use App\Activity;
use App\User;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Auth;
use View;
use App\Comment;
use Storage;
use App\Artist;
use App\Playlist;
use App\Song;
use App\Album;
use App\Station;
use Carbon\Carbon;
use Route;

class CommentsController
{
    private $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function index()
    {
        if(Route::currentRouteName() == 'backend.comments.approved') {
            $comments = Comment::withoutGlobalScopes()->where('approved', 1)->latest()->paginate(20);
        } else {
            $comments = Comment::withoutGlobalScopes()->where('approved', 0)->latest()->paginate(20);
        }

        return view('backend.comments.index')
            ->with('comments', $comments);
    }

    public function delete()
    {
        $comment = Comment::withoutGlobalScopes()->where('id', $this->request->route('id'))->firstOrFail();

        if($comment->parent_id) {
            Comment::withoutGlobalScopes()->where('id', $comment->parent_id)->decrement('reply_count');
        } else {
            switch ($comment->commentable_type) {
                case 'App\Activity':
                    Activity::where('id', $comment->commentable_id)->decrement('comment_count');
                    break;
                case 'App\Album':
                    Album::where('id', $comment->commentable_id)->decrement('comment_count');
                    break;
                case 'App\Artist':
                    Artist::where('id', $comment->commentable_id)->decrement('comment_count');
                    break;
                case 'App\Playlist':
                    Playlist::where('id', $comment->commentable_id)->decrement('comment_count');
                    break;
                case 'App\User':
                    User::where('id', $comment->commentable_id)->decrement('comment_count');
                    break;
                case 'App\Song':
                    Song::where('id', $comment->commentable_id)->decrement('comment_count');
                    break;
            }
        }

        $comment->delete();

        return redirect()->route('backend.comments')->with('status', 'success')->with('message', 'Comment successfully deleted!');
    }

    public function edit()
    {
        $comment = Comment::withoutGlobalScopes()->findOrFail($this->request->route('id'));

        return view('backend.comments.edit')
            ->with('comment', $comment);
    }

    public function editPost()
    {
        $this->request->validate([
            'content' => 'required|string',
            'created_at' => 'date_format:Y/m/d H:i',
            'approved' => 'required|boolean',
        ]);

        $comment = Comment::withoutGlobalScopes()->findOrFail($this->request->route('id'));
        $comment->content = $this->request->input('content');
        $comment->created_at = Carbon::parse($this->request->input('created_at'))->format('Y/m/d H:i');
        $comment->approved = $this->request->input('approved');
        $comment->save();

        return redirect()->route('backend.comments')->with('status', 'success')->with('message', 'Comment successfully updated!');
    }
}