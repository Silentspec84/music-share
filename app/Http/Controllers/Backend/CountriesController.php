<?php
/**
 * Created by NiNaCoder.
 * Date: 2019-05-26
 * Time: 10:54
 */

namespace App\Http\Controllers\Backend;

use App\Region;
use Carbon\Carbon;
use Illuminate\Http\Request;
use DB;
use App\Country;
use Image;

class CountriesController
{
    private $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function index() {

        $countries = Country::withoutGlobalScopes();

        if ($this->request->has('name'))
        {
            $countries = $countries->where('title', 'like', '%' . $this->request->input('term') . '%');
        }

        if ($this->request->has('fixed'))
        {
            $countries = $countries->where('fixed', '=', 1);
        }

        if ($this->request->has('hidden'))
        {
            $countries = $countries->where('visibility', '=', 0);
        }

        if ($this->request->has('government_form'))
        {
            $countries = $countries->where('government_form', 'like', '%' . $this->request->input('government_form') . '%');
        }

        if ($this->request->has('region'))
        {
            $regionId = intval($this->request->input('region'));
            $countries = $countries->whereHas('region', function($query) use($regionId) {
                $query->where('id', '=', $regionId);
            });
        }


        if ($this->request->has('name'))
        {
            $countries = $countries->orderBy('name', $this->request->input('name'));
        }

        if ($this->request->has('results_per_page'))
        {
            $countries = $countries->paginate(intval($this->request->input('results_per_page')));
        } else {
            $countries = $countries->orderBy('fixed', 'desc')->paginate(20);
        }

        $governmentForms = [];
        foreach(Country::select('government_form')->groupBy('government_form')->get() as $country) {
            $governmentForms = array_merge($governmentForms, array(
                "{$country->government_form}" => $country->government_form
            ));
        }

        return view('backend.countries.index')
            ->with('countries', $countries)
            ->with('governmentForms', $governmentForms);
    }

    public function add()
    {
        return view('backend.countries.form');
    }

    public function edit()
    {
        $country = Country::where('code', '=', $this->request->route('code'))->firstOrFail();;

        return view('backend.countries.form')->with('country', $country);
    }

    public function editPost()
    {
        $this->request->validate([
            'code' => 'required|string|max:3',
            'name' => 'required|string',
            'continent' => 'nullable|string',
            'region' => 'nullable|string',
            'local_name' => 'nullable|string',
            'government_form' => 'nullable|string',
        ]);

        if(request()->route()->getName() == 'backend.countries.add.post') {
            $country = new Country();
        } else {
            $country = Country::where('code', '=', $this->request->route('code'))->firstOrFail();;
        }

        $country->code = $this->request->input('code');
        $country->name = $this->request->input('name');
        $country->continent = $this->request->input('continent');
        $country->region = $this->request->input('region');
        $country->local_name = $this->request->input('local_name');
        $country->government_form = $this->request->input('government_form');

        if ($this->request->hasFile('artwork'))
        {
            $this->request->validate([
                'artwork' => 'required|image|mimes:jpeg,png,jpg,gif|max:' . config('settings.max_image_file_size')
            ]);

            if(request()->route()->getName() == 'backend.countries.edit.post') {
                $country->clearMediaCollection('artwork');
            }

            $country->addMediaFromBase64(base64_encode(Image::make($this->request->file('artwork'))->fit(intval(config('settings.image_artwork_max', 500)), intval(500 * 0.5625))->encode('jpg', config('settings.image_jpeg_quality', 90))->encoded))
                ->usingFileName(time(). '.jpg')
                ->toMediaCollection('artwork', config('settings.storage_artwork_location', 'public'));
        }

        $country->save();

        return redirect()->route('backend.countries')->with('status', 'success')->with('message', 'Country successfully edited!');
    }

    public function delete()
    {
        return redirect('admin/radios/' . $this->request->route('id') . '/stations')->with('status', 'success')->with('message', 'Station successfully deleted!');
    }
}