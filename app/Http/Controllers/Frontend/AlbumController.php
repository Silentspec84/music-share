<?php


namespace App\Http\Controllers\Frontend;

use App\Artist;
use Illuminate\Http\Request;
use App\Song;
use App\Album;
use App\Genre;
use DB;
use View;
use App\Role;

class AlbumController
{
    private $request;

    public function __construct(Request $request)
    {
        $this->request = $request;

    }

    public function index()
    {

        if( $this->request->is('api*') )
        {
            $album = Album::withoutGlobalScopes()->findOrFail($this->request->route('id'));
            $album->setRelation('songs', $album->songs()->withoutGlobalScopes()->get());

            if($this->request->get('callback'))
            {
                return response()->jsonp($this->request->get('callback'), $album->songs)->header('Content-Type', 'application/javascript');
            }
            return response()->json($album);
        }

        $album = Album::withoutGlobalScopes()->findOrFail($this->request->route('id'));

        if(! $album->approved && auth()->check() && Role::getValue('admin_albums')) {

        } else {
            if(! isset($album->id)) {
                abort(404);
            } elseif(auth()->check() && ! $album->visibility && ($album->user_id != auth()->user()->id)) {
                abort(404);
            }  elseif(! auth()->check() && ! $album->visibility) {
                abort(404);
            } elseif(! $album->approved) {
                abort(404);
            }
        }

        $album->setRelation('songs', $album->songs()->paginate(20));

        $view = View::make('album.index')
            ->with('album',  $album);

        if(count($album->artists) == 1) {
            $artist = $album->artists->first();
            $artist->setRelation('songs', $artist->songs()->paginate(5));
            $artist->setRelation('similar', $artist->similar()->paginate(5));
            $artistTopSongs = $artist->songs;
            $view = $view->with('artistTopSongs', $artistTopSongs)->with('artist', $artist);
        }

        if($this->request->ajax()) {
            $sections = $view->renderSections();
            return $sections['content'];
        }

        getMetatags($album);

        return $view;
    }

    public function related (){
        if(auth()->check() && Role::getValue('admin_albums')) {
            $album = Album::withoutGlobalScopes()->findOrFail($this->request->route('id'));
        } else {
            $album = Album::findOrFail($this->request->route('id'));
        }

        $related = Album::where('id', '!=', $album->id)->paginate(20);

        $view = View::make('album.related')
            ->with('album', $album)
            ->with('related', $related);

        if($this->request->ajax()) {
            $sections = $view->renderSections();
            return $sections['content'];
        }

        return $view;
    }
}