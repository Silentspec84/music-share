<?php
/**
 * Created by NiNaCoder.
 * Date: 2019-05-28
 * Time: 15:13
 */

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use View;
use App\Artist;
use App\Song;
use App\Album;
use App\User;
use App\Genre;
use Auth;
use App\Comment;
use App\Post;

class ArtistController extends Controller
{
    private $request;
    private $artist;

    public function __construct(Request $request)
    {
        $this->request = $request;

    }
    public function index()
    {
        $this->artist = Artist::findOrFail($this->request->route('id'));
        $this->artist->setRelation('albums', $this->artist->albums()->latest()->limit(4)->get());

        if( $this->request->is('api*') )
        {
            $this->artist->setRelation('songs', $this->artist->songs()->get());

            if($this->request->get('callback'))
            {
                return response()->jsonp($this->request->get('callback'), $this->artist->songs)->header('Content-Type', 'application/javascript');
            }

            return response()->json(array(
                'artist' => $this->artist,
                'songs' => $this->artist->songs
            ));
        } else {
            $this->artist->setRelation('songs', $this->artist->songs()->paginate(20));
            $this->artist->setRelation('activities', $this->artist->activities()->latest()->paginate(10));
            $this->artist->setRelation('similar', $this->artist->similar()->paginate(5));
        }

        $view = View::make('artist.index')
            ->with('artist', $this->artist);

        if($this->request->ajax()) {
            $sections = $view->renderSections();
            if($this->request->input('page') && intval($this->request->input('page')) > 1)
            {
                return $sections['pagination'];
            } else {
                return $sections['content'];
            }
        }

        getMetatags($this->artist);

        return $view;
    }

    public function albums()
    {
        $this->artist = Artist::findOrFail($this->request->route('id'));
        $this->artist->setRelation('albums', $this->artist->albums()->paginate(20));

        $view = View::make('artist.albums')->with('artist', $this->artist);

        if($this->request->ajax()) {
            $sections = $view->renderSections();
            return $sections['content'];
        }

        getMetatags($this->artist);

        return $view;
    }

    public function similar()
    {
        $this->artist = Artist::findOrFail($this->request->route('id'));
        $this->artist->setRelation('similar', $this->artist->similar()->paginate(20));

        $view = View::make('artist.similar-artists')
            ->with('artist', $this->artist)
            ->with('similar', $this->artist->similar);

        if($this->request->ajax()) {
            $sections = $view->renderSections();
            return $sections['content'];
        }

        getMetatags($this->artist);

        return $view;
    }

    public function followers()
    {
        $this->artist = Artist::with('followers')->findOrFail($this->request->route('id'));

        $view = View::make('artist.followers')->with('artist', $this->artist);

        if($this->request->ajax()) {
            $sections = $view->renderSections();
            return $sections['content'];
        }

        getMetatags($this->artist);

        return $view;
    }

    public function events()
    {
        $this->artist = Artist::with('followers')->findOrFail($this->request->route('id'));

        $view = View::make('artist.events')->with('artist', $this->artist);

        if($this->request->ajax()) {
            $sections = $view->renderSections();
            return $sections['content'];
        }

        getMetatags($this->artist);

        return $view;
    }

}