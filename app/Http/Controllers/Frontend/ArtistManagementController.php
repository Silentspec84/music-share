<?php
/**
 * Created by NiNaCoder.
 * Date: 2019-05-28
 * Time: 15:13
 */

namespace App\Http\Controllers\Frontend;

use App\Activity;
use App\Http\Controllers\Controller;
use App\Upload;
use Illuminate\Http\Request;
use DB;
use View;
use App\Artist;
use App\Song;
use App\Album;
use App\User;
use App\Genre;
use App\Mood;
use App\Event;
use Auth;
use Carbon\Carbon;
use Image;
use App\Role;

class ArtistManagementController extends Controller
{
    private $request;
    private $artist;
    private $artistId;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function index()
    {
        $this->artist = Artist::findOrFail(auth()->user()->artist_id);
        $this->artist->setRelation('albums', $this->artist->albums()->withoutGlobalScopes()->paginate(20));
        $this->artist->setRelation('songs', $this->artist->songs()->withoutGlobalScopes()->orderBy('plays', 'desc')->paginate(10));
        $counts = DB::table('popular')
            ->select(DB::raw('sum(plays) AS playSong'), DB::raw('sum(favorites) AS favoriteSong'),  DB::raw('sum(collections) AS collectSong'))
            ->where('artist_id', $this->artist->id)
            ->first();

        if( $this->request->is('api*') )
        {
            return response()->json(array(
                'artist' => $this->artist,
                'albums' => $this->artist->albums,
                'songs' => $this->artist->songs,
                'counts' => $counts
            ));
        }

        $view = View::make('artist-management.index')
            ->with('songs', $this->artist->songs)
            ->with('albums', $this->artist->albums)
            ->with('artist', $this->artist)
            ->with('counts', $counts);

        if($this->request->ajax()) {
            $sections = $view->renderSections();
            return $sections['content'];
        }

        return $view;
    }

    public function reports()
    {
        $this->artist = Artist::findOrFail(auth()->user()->artist_id);

        $view = View::make('artist-management.reports')
            ->with('artist', $this->artist);

        if($this->request->ajax()) {
            $sections = $view->renderSections();
            return $sections['content'];
        }

        return $view;
    }

    public function events()
    {
        $this->artist = Artist::findOrFail(auth()->user()->artist_id);
        $this->artist->setRelation('songs', $this->artist->songs()->paginate(20));

        $view = View::make('artist-management.events')
            ->with('songs', $this->artist->songs)
            ->with('artist', $this->artist);

        if($this->request->ajax()) {
            $sections = $view->renderSections();
            return $sections['content'];
        }

        return $view;
    }

    public function uploaded()
    {
        $this->artist = Artist::findOrFail(auth()->user()->artist_id);
        $this->artist->setRelation('songs', $this->artist->songs()->withoutGlobalScopes()->orderBy('approved', 'asc')->paginate(20));

        $view = View::make('artist-management.uploaded')
            ->with('songs', $this->artist->songs)
            ->with('artist', $this->artist);

        if($this->request->ajax()) {
            $sections = $view->renderSections();
            if($this->request->input('page') && intval($this->request->input('page')) > 1)
            {
                return $sections['pagination'];
            } else {
                return $sections['content'];
            }
        }

        return $view;
    }


    public function profile()
    {
        $this->artist = Artist::findOrFail(auth()->user()->artist_id);

        $view = View::make('artist-management.profile')
            ->with('artist', $this->artist);

        if($this->request->ajax()) {
            $sections = $view->renderSections();
            return $sections['content'];
        }

        return $view;
    }

    public function saveProfile()
    {
        $artist = Artist::findOrFail(auth()->user()->artist_id);

        $this->request->validate([
            'name' => 'required|max:100',
            'location' => 'nullable|max:100',
            'website' => 'nullable|url|max:100',
            'facebook' => 'nullable|url|max:100',
            'twitter' => 'nullable|url|max:100',
            'bio' => 'nullable|max:180',
            'genre' => 'nullable|array',
            'mood' => 'nullable|array',
        ]);

        $artist->name = $this->request->input('name');
        $artist->location = $this->request->input('location');
        $artist->website = $this->request->input('website');
        $artist->facebook = $this->request->input('facebook');
        $artist->twitter = $this->request->input('twitter');
        $artist->bio = $this->request->input('bio');

        $genre = $this->request->input('genre');
        $mood = $this->request->input('mood');

        if(is_array($genre))
        {
            $allow_genres = Role::getValue('artist_allow_genre');
            if(!array_diff($this->request->input('genre'), $allow_genres)) {
                $artist->genre = implode(",", $this->request->input('genre'));
            } else {
                abort(403, 'Your do not have permission to upload music to this genre.');
            }
        } else {
            $artist->genre = null;
        }

        if(is_array($mood))
        {
            $allow_moods = Role::getValue('artist_allow_mood');
            if(!array_diff($this->request->input('mood'), $allow_moods)) {
                $artist->mood = implode(",", $this->request->input('mood'));
            } else {
                abort(403, 'Your do not have permission to upload music to this mood.');
            }
        } else {
            $artist->mood = null;
        }

        if ($this->request->hasFile('artwork')) {
            $this->request->validate([
                'artwork' => 'required|image|mimes:jpeg,png,jpg,gif|max:' . config('settings.max_image_file_size')
            ]);
            $artist->clearMediaCollection('artwork');
            $artist->addMediaFromBase64(base64_encode(Image::make($this->request->file('artwork'))->fit(intval(config('settings.image_artwork_max', 500)),  intval(config('settings.image_artwork_max', 500)))->encode('jpg', config('settings.image_jpeg_quality', 90))->encoded))
                ->usingFileName(time(). '.jpg')
                ->toMediaCollection('artwork', config('settings.storage_artwork_location', 'public'));
        }

        $artist->save();

        return response()->json($artist);
    }

    public function editSongPost()
    {
        $this->request->validate([
            'id' => 'required|numeric',
            'title' => 'required|max:100',
            'genre' => 'nullable|array',
            'mood' => 'nullable|array',
            'copyright' => 'nullable|string|max:100',
            'created_at' => 'nullable|date_format:m/d/Y|after:' . Carbon::now(),
        ]);

        /**
         * Validate if song belong to artist (by user_id)
         */

        if(Song::withoutGlobalScopes()->where('user_id', '=', auth()->user()->id)->where('id', '=', $this->request->input('id'))->exists()) {
            $song = Song::withoutGlobalScopes()->findOrFail($this->request->input('id'));
            if(intval(Role::getValue('artist_day_edit_limit')) != 0 && Carbon::parse($song->created_at)->addDay(Role::getValue('artist_day_edit_limit'))->lt(Carbon::now())) {
                return response()->json([
                    'message' => 'React the limited time to edit',
                    'errors' => array('message' => array(__('web.POPUP_EDIT_SONG_DENIED')))
                ], 403);
            } else {
                /**
                 * Change artwork if have to
                 */

                if ($this->request->hasFile('artwork')) {
                    $this->request->validate([
                        'artwork' => 'required|image|mimes:jpeg,png,jpg,gif|max:' . config('settings.max_image_file_size')
                    ]);
                    $song->clearMediaCollection('artwork');
                    $song->addMediaFromBase64(base64_encode(Image::make($this->request->file('artwork'))->fit(intval(config('settings.image_artwork_max', 500)),  intval(config('settings.image_artwork_max', 500)))->encode('jpg', config('settings.image_jpeg_quality', 90))->encoded))
                        ->usingFileName(time(). '.jpg')
                        ->toMediaCollection('artwork', config('settings.storage_artwork_location', 'public'));
                }

                $song->title = $this->request->input('title');
                $genre = $this->request->input('genre');
                $mood = $this->request->input('mood');

                if($this->request->input('created_at'))
                {
                    $song->created_at = Carbon::parse($this->request->input('created_at'));
                }

                if(is_array($genre))
                {
                    $allow_genres = Role::getValue('artist_allow_genre');
                    if(!array_diff($this->request->input('genre'), $allow_genres)) {
                        $song->genre = implode(",", $this->request->input('genre'));
                    } else {
                        abort(403, 'Your do not have permission to upload music to this genre.');
                    }
                } else {
                    $song->genre = null;
                }

                if(is_array($mood))
                {
                    $allow_moods = Role::getValue('artist_allow_mood');
                    if(!array_diff($this->request->input('mood'), $allow_moods)) {
                        $song->mood = implode(",", $this->request->input('mood'));
                    } else {
                        abort(403, 'Your do not have permission to upload music to this mood.');
                    }
                } else {
                    $song->mood = null;
                }

                if(! $song->approved && is_array($genre)  && Role::getValue('artist_mod')) {
                    if(Role::getValue('artist_trusted_genre')) {
                        $trustedSection = Role::getValue('artist_trusted_genre');
                        if(!array_diff($genre, $trustedSection)) {
                            $song->approved = 1;
                        } else {
                            $song->approved = 0;
                        }
                    }
                }

                if($this->request->input('visibility')) {
                    $song->visibility = 1;
                } else {
                    $song->visibility = 0;
                }

                if($this->request->input('downloadable')) {
                    $song->allow_download = 1;
                } else {
                    $song->allow_download = 0;
                }

                if($this->request->input('notification')) {
                    if($this->request->input('created_at')) {
                        makeActivity(
                            auth()->user()->id,
                            auth()->user()->artist_id,
                            (new Artist)->getMorphClass(),
                            'addSong',
                            $song->id,
                            false,
                            Carbon::parse($this->request->input('created_at'))
                        );
                    } else {
                        makeActivity(
                            auth()->user()->id,
                            auth()->user()->artist_id,
                            (new Artist)->getMorphClass(),
                            'addSong',
                            $song->id
                        );
                    }
                }

                $song->copyright = $this->request->input('copyright');
                $song->save();

                return response()->json($song);
            }
        } else {
            abort(403, 'Not your song.');
        }
    }

    /**
     * Get Available Genres (set available genre in Admin panel user group and role)
     * @return array
     */

    public function genres()
    {
        $allowGenres = Genre::findMany(Role::getValue('artist_allow_genre'));

        $selectedGenres = array();

        if($this->request->input('object_type')) {

            if($this->request->input('object_type') == 'song') {
                $song = Song::withoutGlobalScopes()->findOrFail($this->request->input('id'));
                $selectedGenres = explode(',', $song->genre);
            } else if($this->request->input('object_type') == 'album') {
                if($this->request->input('id')) {
                    $song = Album::withoutGlobalScopes()->findOrFail($this->request->input('id'));
                    $selectedGenres = explode(',', $song->genre);
                } else {
                    $selectedGenres = [];
                }
            }

        }

        $allowGenres = $allowGenres->map(function($genre) use ($selectedGenres) {
            if( in_array($genre->id, $selectedGenres) ) $genre->selected = true;

            else $genre->selected = false;

            return $genre;
        });

        if($this->request->ajax()) {
            response()->json($allowGenres);
        }

        return $allowGenres;
    }

    /**
     * Get Available Moods (set available moods in Admin panel user group and role)
     * @return array
     */

    public function moods()
    {
        $allowMoods = Mood::findMany(Role::getValue('artist_allow_mood'));

        $selectedMoods = array();

        if($this->request->input('object_type')) {

            if($this->request->input('object_type') == 'song') {
                $song = Song::withoutGlobalScopes()->findOrFail($this->request->input('id'));
                $selectedMoods = explode(',', $song->mood);
            } else if($this->request->input('object_type') == 'album') {

                if($this->request->input('id')) {
                    $song = Album::withoutGlobalScopes()->findOrFail($this->request->input('id'));
                    $selectedMoods = explode(',', $song->mood);
                } else {
                    $selectedMoods = [];
                }
            }

        }

        $allowMoods = $allowMoods->map(function($mood) use ($selectedMoods) {
            if( in_array($mood->id, $selectedMoods) ) $mood->selected = true;

            else $mood->selected = false;

            return $mood;
        });

        if($this->request->ajax()) {
            response()->json($allowMoods);
        }

        return $allowMoods;
    }

    public function artistChart()
    {
        $this->artist = Artist::findOrFail(auth()->user()->artist_id);

        $fromDate = Carbon::now()->subDay(15)->format('Y/m/d H:i:s');
        $toDate = Carbon::now()->format('Y/m/d H:i:s');

        $rows = DB::table('popular')
            ->select(DB::raw('sum(plays) AS playSong'), DB::raw('sum(favorites) AS favoriteSong'),  DB::raw('sum(collections) AS collectSong'), DB::raw('DATE(created_at) as date'))
            ->where('popular.created_at', '<=',  $toDate)
            ->where('popular.created_at', '>=',  $fromDate)
            ->where('artist_id', $this->artist->id)
            ->groupBy('date')
            ->limit(50)
            ->get();

        $rows = insertMissingData($rows, ['playSong', 'favoriteSong', 'collectSong'], $fromDate, $toDate);

        $data = array();

        foreach ($rows as $item) {
            $item = (array) $item;
            $data['playSong'][] = $item['playSong'];
            $data['favoriteSong'][] = $item['favoriteSong'];
            $data['collectSong'][] = $item['collectSong'];
            $data['period'][] = Carbon::parse($item['date'])->format('m/d');
        }

        return response()->json(array(
            'success' => true,
            'data' => $data

        ));
    }

    public function songChart()
    {
        $this->artist = Artist::findOrFail(auth()->user()->artist_id);

        $start_date = date ( "Y-m-d", (time() - (30*24*3600)) );
        $end_date = date ( "Y-m-d", time() );

        $from = strtotime( date("Y-m-d")) - (14 * 24 * 60 * 60);
        $from = date("Y-m-d", $from);

        $play_data = DB::table('popular')
            ->select(DB::raw('sum(plays) AS playSong'), DB::raw('sum(favorites) AS favoriteSong'),  DB::raw('sum(channels) AS collectSong'), 'created_at')
            ->where('popular.created_at', '<=',  date("Y-m-d"))
            ->where('popular.created_at', '>=',  $from)
            ->where('popular.song_id', $this->request->route('id'))
            //->groupBy('popular.trackId')
            ->groupBy('popular.created_at')
            ->limit(50)
            ->get();

        $data = insertMissingDate($play_data, "playSong", $from, date("Y-m-d"));

        return response()->json(array(
            'success' => true,
            'data' => $data

        ));
    }

    public function albums()
    {
        $this->artist = Artist::findOrFail(auth()->user()->artist_id);
        $this->artist->setRelation('albums', $this->artist->albums()->withoutGlobalScopes()->paginate(20));

        $view = View::make('artist-management.albums')
            ->with('artist', $this->artist);

        if($this->request->ajax()) {
            $sections = $view->renderSections();
            return $sections['content'];
        }

        return $view;
    }

    public function createAlbum()
    {
        $this->request->validate([
            'title' => 'required|string|max:50',
            'type' => 'required|numeric|between:1,10',
            'description' => 'nullable|string|max:1000',
            'created_at' => 'nullable|date_format:m/d/Y|after:' . Carbon::now(),
            'released_at' => 'nullable|date_format:m/d/Y|before:' . Carbon::now(),
            'artwork' => 'required|image|mimes:jpeg,png,jpg,gif|max:' . config('settings.max_image_file_size')
        ]);

        $album = new Album();

        $album->title = $this->request->input('title');
        $album->artistIds = auth()->user()->artist_id;
        $genre = $this->request->input('genre');
        $mood = $this->request->input('mood');
        $album->type = $this->request->input('type');
        $album->description = $this->request->input('description');
        $album->visibility = $this->request->input('visibility');
        $album->user_id = auth()->user()->id;

        if($this->request->input('released_at'))
        {
            $album->created_at = Carbon::parse($this->request->input('released_at'));
        }

        if($this->request->input('created_at'))
        {
            $album->created_at = Carbon::parse($this->request->input('created_at'));
        }

        if(is_array($genre))
        {
            $allow_genres = Role::getValue('artist_allow_genre');
            if(!array_diff($this->request->input('genre'), $allow_genres)) {
                $album->genre = implode(",", $this->request->input('genre'));
            } else {
                abort(403, 'Your do not have permission to upload music to this genre.');
            }
        }

        if(is_array($mood))
        {
            $allow_moods = Role::getValue('artist_allow_mood');
            if(!array_diff($this->request->input('mood'), $allow_moods)) {
                $album->mood = implode(",", $this->request->input('mood'));
            } else {
                abort(403, 'Your do not have permission to upload music to this mood.');
            }
        }

        if(is_array($genre) && Role::getValue('artist_mod')) {
            if(Role::getValue('artist_trusted_genre')) {
                $trustedSection = Role::getValue('artist_trusted_genre');
                if(!array_diff($this->request->input('genre'), $trustedSection)) {
                    $album->approved = 1;
                } else {
                    $album->approved = 0;
                }
            }
        }

        $album->addMediaFromBase64(base64_encode(Image::make($this->request->file('artwork'))->fit(intval(config('settings.image_artwork_max', 500)),  intval(config('settings.image_artwork_max', 500)))->encode('jpg', config('settings.image_jpeg_quality', 90))->encoded))
            ->usingFileName(time(). '.jpg')
            ->toMediaCollection('artwork', config('settings.storage_artwork_location', 'public'));

        if($this->request->input('visibility')) {
            $album->visibility = 1;
        } else {
            $album->visibility = 0;
        }

        $album->save();

        return $album->makeVisible(['approved']);
    }


    public function showAlbum()
    {
        $this->artist = Artist::findOrFail(auth()->user()->artist_id);
        $album = Album::withoutGlobalScopes()->findOrFail($this->request->route('id'));
        $album->setRelation('songs', $album->songs()->withoutGlobalScopes()->get());

        $view = View::make('artist-management.edit-album')
            ->with('artist', $this->artist)
            ->with('album', $album);

        if($this->request->ajax()) {
            $sections = $view->renderSections();
            return $sections['content'];
        }

        return $view;
    }

    public function deleteAlbum()
    {
        $this->artist = Artist::findOrFail(auth()->user()->artist_id);
        if(Album::withoutGlobalScopes()->where('user_id', '=', auth()->user()->id)->where('id', '=', $this->request->input('id'))->exists()) {
            $album = Album::withoutGlobalScopes()->findOrFail($this->request->input('id'));
            if(intval(Role::getValue('artist_day_edit_limit')) != 0 && Carbon::parse($album->created_at)->addDay(Role::getValue('artist_day_edit_limit'))->lt(Carbon::now())) {
                return response()->json([
                    'message' => 'React the limited time to edit',
                    'errors' => array('message' => array(__('web.POPUP_DELETE_ALBUM_DENIED')))
                ], 403);
            } else {
                $album->delete();
                return response()->json(array('success' => true));
            }
        } else {
            abort(403, 'Not your album.');
        }
    }

    public function deleteSong()
    {
        $this->artist = Artist::findOrFail(auth()->user()->artist_id);
        if(Song::withoutGlobalScopes()->where('user_id', '=', auth()->user()->id)->where('id', '=', $this->request->input('id'))->exists()) {
            $song = Song::withoutGlobalScopes()->findOrFail($this->request->input('id'));
            if(intval(Role::getValue('artist_day_edit_limit')) != 0 && Carbon::parse($song->created_at)->addDay(Role::getValue('artist_day_edit_limit'))->lt(Carbon::now())) {
                return response()->json([
                    'message' => 'React the limited time to edit',
                    'errors' => array('message' => array(__('web.POPUP_DELETE_SONG_DENIED')))
                ], 403);
            } else {
                $song->delete();
                return response()->json(array('success' => true));
            }
        } else {
            abort(403, 'Not your song.');
        }
    }

    public function sortAlbumSongs()
    {
        $this->request->validate([
            'album_id' => 'required|int',
            'removeIds' => 'nullable|string',
            'nextOrder' => 'required|string',
        ]);

        $album_id = $this->request->input('album_id');
        $removeIds = json_decode($this->request->input('removeIds'));
        $nextOrder = json_decode($this->request->input('nextOrder'));

        if(is_array($removeIds))
        {
            foreach ($removeIds as $trackId){
                DB::table('album_songs')
                    ->where('album_id', $album_id)
                    ->where('song_id', $trackId)
                    ->delete();
            }
        }

        if(is_array($nextOrder))
        {
            foreach ($nextOrder as $index => $trackId) {
                DB::table('album_songs')
                    ->where('album_id', $album_id)
                    ->where('song_id', $trackId)
                    ->update(['priority' => $index]);
            }
        }

        return response()->json(array("success" => true));
    }


    public function editAlbum()
    {
        $this->request->validate([
            'title' => 'required|string|max:50',
            'type' => 'required|numeric|between:1,10',
            'description' => 'nullable|string|max:1000',
            'created_at' => 'nullable|date_format:m/d/Y|after:' . Carbon::now(),
            'released_at' => 'nullable|date_format:m/d/Y|before:' . Carbon::now(),
        ]);

        if(Album::withoutGlobalScopes()->where('user_id', '=', auth()->user()->id)->where('id', '=', $this->request->input('id'))->exists()) {
            $album = Album::withoutGlobalScopes()->findOrFail($this->request->input('id'));
            if(intval(Role::getValue('artist_day_edit_limit')) != 0 && Carbon::parse($album->created_at)->addDay(Role::getValue('artist_day_edit_limit'))->lt(Carbon::now())) {
                return response()->json([
                    'message' => 'React the limited time to edit',
                    'errors' => array('message' => array(__('web.POPUP_EDIT_ALBUM_DENIED')))
                ], 403);
            } else {
                if ($this->request->hasFile('artwork'))
                {
                    $this->request->validate([
                        'artwork' => 'required|image|mimes:jpeg,png,jpg,gif|max:' . config('settings.max_image_file_size')
                    ]);

                    $album->clearMediaCollection('artwork');
                    $album->addMediaFromBase64(base64_encode(Image::make($this->request->file('artwork'))->fit(intval(config('settings.image_artwork_max', 500)),  intval(config('settings.image_artwork_max', 500)))->encode('jpg', config('settings.image_jpeg_quality', 90))->encoded))
                        ->usingFileName(time(). '.jpg')
                        ->toMediaCollection('artwork', config('settings.storage_artwork_location', 'public'));
                }

                $album->title = $this->request->input('title');
                $album->description = $this->request->input('description');
                $album->visibility = $this->request->input('visibility');
                $genre = $this->request->input('genre');
                $mood = $this->request->input('mood');
                $album->type = $this->request->input('type');
                $album->description = $this->request->input('description');

                if(is_array($genre))
                {
                    $allow_genres = Role::getValue('artist_allow_genre');
                    if(!array_diff($this->request->input('genre'), $allow_genres)) {
                        $album->genre = implode(",", $this->request->input('genre'));
                    } else {
                        abort(403, 'Your do not have permission to upload music to this genre.');
                    }
                } else {
                    $album->genre = null;
                }

                if(is_array($mood))
                {
                    $allow_moods = Role::getValue('artist_allow_mood');
                    if(!array_diff($this->request->input('mood'), $allow_moods)){
                        $album->mood = implode(",", $this->request->input('mood'));
                    } else {
                        abort(403, 'Your do not have permission to upload music to this mood.');
                    }
                } else {
                    $album->mood = null;
                }

                if($this->request->input('visibility')) {
                    $album->visibility = 1;
                } else {
                    $album->visibility = 0;
                }

                $album->save();

                return response()->json($album);
            }
        } else {
            abort(403, 'Not your song.');
        }
    }

    public function uploadAlbum()
    {
        $this->artist = Artist::findOrFail(auth()->user()->artist_id);
        $album = Album::withoutGlobalScopes()->findOrFail($this->request->route('id'));
        $allowGenres = Genre::findMany(Role::getValue('artist_allow_genre'));
        $allowMoods = Mood::findMany(Role::getValue('artist_allow_mood'));

        $view = View::make('artist-management.upload')
            ->with('artist', $this->artist)
            ->with('album', $album)
            ->with('allowGenres', $allowGenres)
            ->with('allowMoods', $allowMoods);

        if($this->request->ajax()) {
            $sections = $view->renderSections();
            return $sections['content'];
        }

        return $view;
    }

    public function handleUpload()
    {
        $this->artist = Artist::findOrFail(auth()->user()->artist_id);
        $album = Album::withoutGlobalScopes()->findOrFail($this->request->route('id'));

        /** Check if user have permission to upload */

        if(! Role::getValue('artist_allow_upload')) {
            abort(403);
        }

        $res = (new Upload)->handle($this->request, $artistIds = auth()->user()->artist_id, $album->id);

        return response()->json($res);
    }

    public function createEvent()
    {
        $this->request->validate([
            'title' => 'required|string|max:50',
            'location' => 'required|string|max:100',
            'link' => 'nullable|string|max:100',
            'started_at' => 'nullable|date_format:m/d/Y|after:' . Carbon::now(),
        ]);

        $event = new Event();
        $event->artist_id = auth()->user()->artist_id;
        $event->title = $this->request->input('title');
        $event->location = $this->request->input('location');
        $event->link = $this->request->input('link');
        $event->started_at = Carbon::parse($this->request->input('started_at'));
        $event->save();

        makeActivity(
            auth()->user()->id,
            auth()->user()->artist_id,
            (new Artist)->getMorphClass(),
            'addEvent',
            $event->id,
            false
        );

        return response()->json($event);
    }

    public function editEvent()
    {
        $this->request->validate([
            'id' => 'required|integer',
            'title' => 'required|string|max:50',
            'location' => 'required|string|max:100',
            'link' => 'nullable|string|max:100',
            'started_at' => 'nullable|date_format:m/d/Y',
        ]);

        $event = Event::where('artist_id', auth()->user()->artist_id)
            ->where('id', $this->request->input('id'))
            ->firstOrFail();

        $event->title = $this->request->input('title');
        $event->location = $this->request->input('location');
        $event->link = $this->request->input('link');
        $event->started_at = Carbon::parse($this->request->input('started_at'));
        $event->save();

        return response()->json($event);
    }

    public function deleteEvent()
    {
        $this->request->validate([
            'id' => 'required|integer',
        ]);

        $event = Event::where('artist_id', auth()->user()->artist_id)
            ->where('id', $this->request->input('id'))
            ->firstOrFail();

        Activity::where('user_id', auth()->user()->id)
            ->where('activityable_id', auth()->user()->artist_id)
            ->where('activityable_type', 'App\Artist')
            ->where('action', 'addEvent')
            ->delete();

        $event->delete();

        return response()->json(array(
            'success' => true
        ));
    }
}