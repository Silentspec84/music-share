<?php
/**
 * Created by PhpStorm.
 * User: lechchut
 * Date: 6/3/19
 * Time: 10:52 AM
 */

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use View;
use DB;
use App\Artist;
use App\Song;
use App\Album;
use App\Playlist;
use App\Station;
use App\User;
use App\Channel;
use MetaTag;

class ChannelController
{
    private $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function index()
    {
        $channel = Channel::where('alt_name', $this->request->route('slug'))->firstOrFail();

        if ($channel->object_type == "artist") {
            $channel->objects = Artist::whereIn('id', explode(',', $channel->object_ids))->paginate(20);
        } elseif ($channel->object_type == "song") {
            $channel->objects = Song::whereIn('id', explode(',', $channel->object_ids))->paginate(20);
        } elseif ($channel->object_type == "album") {
            $channel->objects = Album::whereIn('id', explode(',', $channel->object_ids))->paginate(20);
        } elseif ($channel->object_type == "playlist") {
            $channel->objects = Playlist::whereIn('id', explode(',', $channel->object_ids))->paginate(20);
        } elseif ($channel->object_type == "station") {
            $channel->objects = Station::whereIn('id', explode(',', $channel->object_ids))->paginate(20);
        } elseif ($channel->object_type == "user") {
            $channel->objects = User::whereIn('id', explode(',', $channel->object_ids))->paginate(20);
        }

        if( $this->request->is('api*') )
        {
            return response()->json($channel->objects);
        }

        $view = View::make('channel.index')
            ->with('channel', $channel);

        if($this->request->ajax()) {
            $sections = $view->renderSections();

            if($this->request->input('page') && intval($this->request->input('page')) > 1)
            {
                return $sections['pagination'];
            } else {
                return $sections['content'];
            }
        }

        MetaTag::set('title', $channel->meta_title);
        MetaTag::set('description', $channel->meta_description);

        return $view;
    }
}