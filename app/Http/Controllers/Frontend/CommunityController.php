<?php
/**
 * Created by NiNaCoder.
 * Date: 2019-05-26
 * Time: 15:46
 */

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Activity;
use View;
use App\Slide;
use App\Channel;

class CommunityController
{
    private $request;

    public function __construct(Request $request) {
        $this->request = $request;
    }

    public function index()
    {
        $community = new \stdClass();
        $community->activities = Activity::where('action', '!=', 'addEvent')->latest()->paginate(20);
        $community->channels = Channel::where('allow_community', 1)->orderBy('priority', 'asc')->get();
        $community->slides = Slide::where('allow_community', 1)->orderBy('priority', 'asc')->get();

        if( $this->request->is('api*') )
        {
            return response()->json($community);
        }

        if($this->request->ajax()) {
            $view = View::make('community.index')->with('community', $community);
            $sections = $view->renderSections();
            if($this->request->input('page') && intval($this->request->input('page')) > 1)
            {
                return $sections['pagination'];
            } else {
                return $sections['content'];
            }
        }
        return view('community.index')->with('community', $community);
    }
}