<?php
/**
 * Created by NiNaCoder.
 * Date: 2019-05-24
 * Time: 13:08
 */

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use DB;
use App\Genre;
use App\Mood;
use App\Slide;
use App\Channel;
use View;

class DiscoverController
{
    public function index(Request $request)
    {

        $discover = (Object) array();

        $discover->channels = Channel::where('allow_discover', 1)->orderBy('priority', 'asc')->get();

        $discover->slides = Slide::where('allow_discover', 1)->orderBy('priority', 'asc')->get();

        $discover->genres = Genre::orderBy('priority', 'asc')->get();
        $discover->moods = Mood::orderBy('priority', 'asc')->get();

        if( $request->is('api*') )
        {
            return response()->json($discover);
        }

        $view = View::make('discover.index')->with('discover', $discover);

        if($request->ajax()) {
            $sections = $view->renderSections();
            return $sections['content'];
        }

        getMetatags();

        return $view;
    }
}