<?php
/**
 * Created by NiNaCoder.
 * Date: 2019-05-30
 * Time: 10:08
 */

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use View;
use App\Genre;
use App\Song;
use App\Album;
use App\Playlist;
use App\Artist;
use App\Slide;
use App\Channel;
use MetaTag;

class GenreController
{
    private $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    private function getGenre(){
        $genre = Genre::where('alt_name',  $this->request->route('slug'))->firstOrFail();
        /** set metatags for genre section */
        MetaTag::set('title', $genre->meta_title ? $genre->meta_title : $genre->name);
        MetaTag::set('description', $genre->meta_description ? $genre->meta_description : $genre->description);
        MetaTag::set('keywords', $genre->meta_keywords);
        MetaTag::set('image', $genre->artwork);

        return $genre;
    }

    public function index()
    {
        $genre = $this->getGenre();
        $genre->channels = Channel::where('genre', 'REGEXP', '(^|,)(' . $genre->id . ')(,|$)')->orderBy('priority', 'asc')->get();
        $genre->slides = Slide::where('genre', 'REGEXP', '(^|,)(' . $genre->id . ')(,|$)')->orderBy('priority', 'asc')->get();
        $genre->songs = Song::where('genre', 'REGEXP', '(^|,)(' . $genre->id . ')(,|$)')->paginate(20);

        if( $this->request->is('api*') )
        {
            //$genre->albums = (new Album)->get("albums.genre REGEXP '(^|,)(" . $genre->id . ")(,|$)'");
            //$genre->artists = (new Artist)->get("artists.genre REGEXP '(^|,)(" . $genre->id . ")(,|$)'");
            //$genre->playlists = (new Playlist)->get("playlists.genre REGEXP '(^|,)(" . $genre->id . ")(,|$)'");

            return response()->json($genre);
        }

        $genre->related = Genre::where('id', '!=',  $genre->id);

        $view = View::make('genre.index')
            ->with('genre', $genre);

        if($this->request->ajax()) {
            $sections = $view->renderSections();

            if($this->request->input('page') && intval($this->request->input('page')) > 1)
            {
                return $sections['pagination'];
            } else {
                return $sections['content'];
            }
        }

        return $view;
    }

    public function songs()
    {
        $genre = $this->getGenre();
        $songs = Song::where('genre', 'REGEXP', '(^|,)(' . $genre->id . ')(,|$)');

        if( $this->request->is('api*') )
        {
            return response()->json($songs);
        }
    }

    public function albums()
    {
        $genre = $this->getGenre();
        $albums = Album::where('genre', 'REGEXP', '(^|,)(' . $genre->id . ')(,|$)');

        if( $this->request->is('api*') )
        {
            return response()->json($albums);
        }
    }

    public function artists()
    {
        $genre = $this->getGenre();
        $artists = Artist::where('genre', 'REGEXP', '(^|,)(' . $genre->id . ')(,|$)');

        if( $this->request->is('api*') )
        {
            return response()->json($artists);
        }
    }

    public function playlists()
    {
        $genre = $this->getGenre();
        $playlists = Playlist::where('genre', 'REGEXP', '(^|,)(' . $genre->id . ')(,|$)');

        if( $this->request->is('api*') )
        {
            return response()->json($playlists);
        }
    }
}