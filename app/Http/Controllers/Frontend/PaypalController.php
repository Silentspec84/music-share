<?php
/**
 * Created by NiNaCoder.
 * Date: 2019-08-06
 * Time: 17:06
 */

namespace App\Http\Controllers\Frontend;

use App\Email;
use App\Subscription;
use Illuminate\Http\Request;
use App\Order;
use App\Service;
use DB;
use Auth;
use App\User;
use Carbon\Carbon;


use PayPal\Api\ChargeModel;
use PayPal\Api\Currency;
use PayPal\Api\MerchantPreferences;
use PayPal\Api\PaymentDefinition;
use PayPal\Api\Plan;
use PayPal\Api\Patch;
use PayPal\Api\PatchRequest;
use PayPal\Common\PayPalModel;
use PayPal\Api\Agreement;
use PayPal\Api\AgreementStateDescriptor;
use PayPal\Api\Payer;
use PayPal\Api\PayerInfo;
use PayPal\Api\ShippingAddress;

use AshAllenDesign\LaravelExchangeRates\Classes\ExchangeRate;

class PaypalController
{
    private $request;
    private $apiContext;

    public function __construct(Request $request)
    {
        $this->request = $request;
        $this->apiContext = new \PayPal\Rest\ApiContext(
            new \PayPal\Auth\OAuthTokenCredential(
                env('PAYPAL_APP_CLIENT_ID'),
                env('PAYPAL_APP_SECRET')
            )
        );

        /*
         * $apiContext->setConfig(
		      array(
		        'mode' => 'live'
		      )
		);
         */
    }

    public function subscription()
    {

        if(auth()->user()->subscription) {
            abort(403, 'You are already have a subscription.');
        }

        $service = Service::findOrFail($this->request->route('id'));

        $plan = new Plan();

        $plan->setName($service->title)
            ->setDescription($service->price . $service->plan_period)
            ->setType('infinite');

        $paymentDefinition = new PaymentDefinition();

        if(in_array(config('settings.currency', 'USD'), config('payment.paypal_currency_subscription'))) {
            $amount = in_array(config('settings.currency', 'USD'), config('payment.currency_decimals')) ? $service->price : intval($service->price);
        } else {
            $exchangeRates = new ExchangeRate();
            $amount = number_format($exchangeRates->convert($service->price, config('settings.currency'), 'USD', Carbon::now()), 2);
        }

        switch ($service->plan_period_format) {
            case 'D':
                $frequency = 'Day';
                break;
            case 'W':
                $frequency = 'Week';
                break;
            case 'M':
                $frequency = 'Month';
                break;
            case 'Y':
                $frequency = 'Year';
                break;
            default:
                $frequency = 'Month';
        }

        $paymentDefinition->setName('Regular Payments')
            ->setType('REGULAR')
            ->setFrequency($frequency)
            ->setFrequencyInterval(intval($service->plan_period))
            ->setAmount(new Currency(array('value' => $amount, 'currency' => config('settings.currency', 'USD'))));

        $service->title = $service->title . ' ' . config('settings.currency') . $amount . '/' . $frequency;

        $merchantPreferences = new MerchantPreferences();

        $merchantPreferences->setReturnUrl(route('frontend.paypal.subscription.success', ['id' => $service->id]))
            ->setCancelUrl(route('frontend.paypal.subscription.cancel', ['id' => $service->id]))
            ->setAutoBillAmount("yes")
            ->setInitialFailAmountAction("CONTINUE")
            ->setMaxFailAttempts("0");

        $plan->setPaymentDefinitions(array($paymentDefinition));
        $plan->setMerchantPreferences($merchantPreferences);


        try {

            $createdPlan = $plan->create($this->apiContext);

            try {

                $patch = new Patch();
                $value = new PayPalModel('{"state":"ACTIVE"}');
                $patch->setOp('replace')
                    ->setPath('/')
                    ->setValue($value);

                $patchRequest = new PatchRequest();
                $patchRequest->addPatch($patch);

                $createdPlan->update($patchRequest, $this->apiContext);
                $createdPlan = Plan::get($createdPlan->getId(), $this->apiContext);
                $agreement = new Agreement();

                if($service->trial) {
                    switch ($service->trial_period_format) {
                        case 'D':
                            $trial_end = Carbon::now()->addDay($service->trial_period)->format('Y-m-d\TH:i:s\Z');
                            break;
                        case 'W':
                            $trial_end = Carbon::now()->addWeek($service->trial_period)->format('Y-m-d\TH:i:s\Z');
                            break;
                        case 'M':
                            $trial_end = Carbon::now()->addMonth($service->trial_period)->format('Y-m-d\TH:i:s\Z');
                            break;
                        case 'Y':
                            $trial_end = Carbon::now()->addYear($service->trial_period)->format('Y-m-d\TH:i:s\Z');
                            break;
                        default:
                            $trial_end = date("Y-m-d\TH:i:s\Z", strtotime('+2 minute'));
                    }
                } else {
                    $trial_end = date("Y-m-d\TH:i:s\Z", strtotime('+2 minute'));;
                }

                $agreement->setName($service->title . ' ' . config('settings.currency') . $amount . '/' . $frequency)
                    ->setDescription($service->title)
                    ->setStartDate($trial_end);

                $plan = new Plan();
                $plan->setId($createdPlan->getId());
                $agreement->setPlan($plan);
                $payer = new Payer();
                $payer->setPaymentMethod('paypal');
                $agreement->setPayer($payer);

                try {
                    $agreement = $agreement->create($this->apiContext);
                    $approvalUrl = $agreement->getApprovalLink();
                } catch (Exception $ex) {
                    echo "Failed to get activate";
                    var_dump($ex);
                    exit();
                }

                header("Location:" . $approvalUrl);
                exit();

            } catch (Exception $ex) {
                echo "Failed to get activate";
                var_dump($ex);
                exit();
            }

        } catch (Exception $ex) {
            echo "Failed to get activate";
            var_dump($ex);
            exit();
        }

    }

    public function success() {
        if(auth()->user()->subscription) {
            abort(403, 'You are already have a subscription.');
        }

        $this->request->validate([
            'token' => 'required|string',
        ]);

        $service = Service::findOrFail($this->request->route('id'));

        $apiContext = new \PayPal\Rest\ApiContext(
            new \PayPal\Auth\OAuthTokenCredential(
                env('PAYPAL_APP_CLIENT_ID'),
                env('PAYPAL_APP_SECRET')
            )
        );

        $token = $this->request->input('token');
        $agreement = new \PayPal\Api\Agreement();

        try {
            $agreement->execute($token, $apiContext);
            $subscription = new Subscription();

            $subscription->gate = 'paypal';
            $subscription->user_id = auth()->user()->id;
            $subscription->service_id = $service->id;
            $subscription->payment_status = 1;
            $subscription->transaction_id = $this->request->input('token');
            $agreement = Agreement::get($agreement->getId(), $apiContext);
            $subscription->token = $agreement->getId();
            $plan = $agreement->getPlan();
            $subscription->trial_end = Carbon::parse($agreement->getAgreementDetails()->next_billing_date);
            $subscription->next_billing_date = Carbon::parse($agreement->getAgreementDetails()->next_billing_date);
            $subscription->cycles = $agreement->getAgreementDetails()->cycles_completed;
            $subscription->amount = $plan->getPaymentDefinitions()[0]->amount->value;
            $subscription->currency = $plan->getPaymentDefinitions()[0]->amount->currency;


            if(! $service->trial) {
                $subscription->last_payment_date = Carbon::now();
            }

            $subscription->save();

            (new Email)->subscriptionReceipt(auth()->user(), $subscription);

            echo '<script type="text/javascript">
            var opener = window.opener;
            if(opener) {
                opener.Payment.success()
            }
            window.close();
            </script>';
        } catch (Exception $ex) {
            echo "Failed to get activate";
            var_dump($ex);
            exit();
        }
    }
}