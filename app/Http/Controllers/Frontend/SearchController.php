<?php
/**
 * Created by NiNaCoder.
 * Date: 2019-06-02
 * Time: 16:38
 */

namespace App\Http\Controllers\Frontend;

use App\Playlist;
use App\Station;
use Illuminate\Http\Request;
use View;
use App\Artist;
use App\Song;
use App\Album;
use App\User;
use App\City;
use App\Event;
use DB;

class SearchController
{
    private $request;
    private $term;
    private $limit;

    public function __construct(Request $request)
    {
        $this->request = $request;
        $this->term = $this->request->is('api*') ? $this->request->input('q') : $this->request->route('slug');
        $this->request->is('api*') ? (isset($_GET['limit']) ? $this->limit = intval($_GET['limit']) :  $this->limit = 20) : $this->limit = 3;
    }

    public function song()
    {
        $result = (Object) array();
        $result->songs = Song::where('title', 'like', '%' . $this->term . '%')->paginate(20);
        $result->artists = Artist::where('name', 'like', '%' . $this->term . '%')->paginate($this->limit);
        $result->albums = Album::where('title', 'like', '%' . $this->term . '%')->paginate($this->limit);
        $result->playlists = Playlist::where('title', 'like', '%' . $this->term . '%')->paginate($this->limit);
        $result->users = User::where('name', 'like', '%' . $this->term . '%')->paginate($this->limit);
        $result->events = Event::where('title', 'like', '%' . $this->term . '%')->paginate($this->limit);

        $total = Song::where('title', 'like', '%' . $this->term . '%')->count();

        if( $this->request->is('api*') )
        {
            return response()->json($result->songs);
        }

        $view = View::make('search.song')
            ->with('result', $result)
            ->with('term', $this->term)
            ->with('total', $total);

        if($this->request->ajax()) {
            $sections = $view->renderSections();

            if($this->request->input('page') && intval($this->request->input('page')) > 1)
            {
                return $sections['pagination'];
            } else {
                return $sections['content'];
            }
        }

        $item = new \stdClass();
        $item->term = $this->term;
        getMetatags($item);

        return $view;
    }

    public function artist()
    {
        $result = (Object) array();
        $result->artists = Artist::where('name', 'like', '%' . $this->term . '%')->paginate(20);

        $total = Artist::where('name', 'like', '%' . $this->term . '%')->count();

        if( $this->request->is('api*') )
        {
            return response()->json($result->artists);
        }

        $result->albums = Album::where('title', 'like', '%' . $this->term . '%')->paginate($this->limit);
        $result->playlists = Playlist::where('title', 'like', '%' . $this->term . '%')->paginate($this->limit);
        $result->users = User::where('name', 'like', '%' . $this->term . '%')->paginate($this->limit);
        $result->events = Event::where('title', 'like', '%' . $this->term . '%')->paginate($this->limit);


        $view = View::make('search.artist')
            ->with('result', $result)
            ->with('term', $this->term)
            ->with('total', $total);

        if($this->request->ajax()) {
            $sections = $view->renderSections();

            if($this->request->input('page') && intval($this->request->input('page')) > 1)
            {
                return $sections['pagination'];
            } else {
                return $sections['content'];
            }
        }

        $item = new \stdClass();
        $item->term = $this->term;
        getMetatags($item);

        return $view;
    }

    public function album()
    {
        $result = (Object) array();
        $result->albums = Album::where('title', 'like', '%' . $this->term . '%')->paginate(20);
        $result->playlists = Playlist::where('title', 'like', '%' . $this->term . '%')->paginate($this->limit);
        $result->artists = Artist::where('name', 'like', '%' . $this->term . '%')->paginate($this->limit);
        $result->users = User::where('name', 'like', '%' . $this->term . '%')->paginate($this->limit);
        $result->events = Event::where('title', 'like', '%' . $this->term . '%')->paginate($this->limit);

        $total = Album::where('title', 'like', '%' . $this->term . '%')->count();

        if( $this->request->is('api*') )
        {
            return response()->json($result->albums);
        }

        $view = View::make('search.album')
            ->with('result', $result)
            ->with('term', $this->term)
            ->with('total', $total);

        if($this->request->ajax()) {
            $sections = $view->renderSections();

            if($this->request->input('page') && intval($this->request->input('page')) > 1)
            {
                return $sections['pagination'];
            } else {
                return $sections['content'];
            }
        }

        $item = new \stdClass();
        $item->term = $this->term;
        getMetatags($item);

        return $view;
    }

    public function playlist()
    {
        $result = (Object) array();

        $result->playlists = Playlist::where('title', 'like', '%' . $this->term . '%')->paginate(20);
        $result->artists = Artist::where('name', 'like', '%' . $this->term . '%')->paginate($this->limit);
        $result->albums = Album::where('title', 'like', '%' . $this->term . '%')->paginate($this->limit);
        $result->playlists = Playlist::where('title', 'like', '%' . $this->term . '%')->paginate($this->limit);
        $result->users = User::where('name', 'like', '%' . $this->term . '%')->paginate($this->limit);
        $result->events = Event::where('title', 'like', '%' . $this->term . '%')->paginate($this->limit);

        $total = Playlist::where('title', 'like', '%' . $this->term . '%')->count();

        if( $this->request->is('api*') )
        {
            return response()->json($result->playlists);
        }

        $view = View::make('search.playlist')
            ->with('result', $result)
            ->with('term', $this->term)
            ->with('total', $total);

        if($this->request->ajax()) {
            $sections = $view->renderSections();

            if($this->request->input('page') && intval($this->request->input('page')) > 1)
            {
                return $sections['pagination'];
            } else {
                return $sections['content'];
            }
        }

        $item = new \stdClass();
        $item->term = $this->term;
        getMetatags($item);

        return $view;
    }

    public function user()
    {
        $result = (Object) array();
        $result->users = User::where('name', 'like', '%' . $this->term . '%')->paginate(20);
        $result->artists = Artist::where('name', 'like', '%' . $this->term . '%')->paginate($this->limit);
        $result->albums = Album::where('title', 'like', '%' . $this->term . '%')->paginate($this->limit);
        $result->playlists = Playlist::where('title', 'like', '%' . $this->term . '%')->paginate($this->limit);
        $result->events = Event::where('title', 'like', '%' . $this->term . '%')->paginate($this->limit);

        if( $this->request->is('api*') )
        {
            return response()->json($result->users);
        }

        $total = User::where('name', 'like', '%' . $this->term . '%')->count();

        $view = View::make('search.user')
            ->with('result', $result)
            ->with('term', $this->term)
            ->with('total', $total);

        if($this->request->ajax()) {
            $sections = $view->renderSections();

            if($this->request->input('page') && intval($this->request->input('page')) > 1)
            {
                return $sections['pagination'];
            } else {
                return $sections['content'];
            }
        }

        $item = new \stdClass();
        $item->term = $this->term;
        getMetatags($item);

        return $view;
    }

    public function event()
    {
        $result = (Object) array();
        $result->events = Event::where('title', 'like', '%' . $this->term . '%')->paginate(20);
        $result->users = User::where('name', 'like', '%' . $this->term . '%')->paginate($this->limit);
        $result->artists = Artist::where('name', 'like', '%' . $this->term . '%')->paginate($this->limit);
        $result->albums = Album::where('title', 'like', '%' . $this->term . '%')->paginate($this->limit);
        $result->playlists = Playlist::where('title', 'like', '%' . $this->term . '%')->paginate($this->limit);

        if( $this->request->is('api*') )
        {
            return response()->json($result);
        }

        $total = Event::where('title', 'like', '%' . $this->term . '%')->count();

        $view = View::make('search.event')
            ->with('result', $result)
            ->with('term', $this->term)
            ->with('total', $total);

        if($this->request->ajax()) {
            $sections = $view->renderSections();

            if($this->request->input('page') && intval($this->request->input('page')) > 1)
            {
                return $sections['pagination'];
            } else {
                return $sections['content'];
            }
        }

        $item = new \stdClass();
        $item->term = $this->term;
        getMetatags($item);

        return $view;
    }

    public function station()
    {
        $result = (Object) array();

        $result->stations = Station::where('title', 'like', '%' . $this->term . '%')->paginate(20);
        $result->playlists = Playlist::where('title', 'like', '%' . $this->term . '%')->paginate($this->limit);
        $result->artists = Artist::where('name', 'like', '%' . $this->term . '%')->paginate($this->limit);
        $result->albums = Album::where('title', 'like', '%' . $this->term . '%')->paginate($this->limit);
        $result->playlists = Playlist::where('title', 'like', '%' . $this->term . '%')->paginate($this->limit);
        $result->users = User::where('name', 'like', '%' . $this->term . '%')->paginate($this->limit);
        $result->events = Event::where('title', 'like', '%' . $this->term . '%')->paginate($this->limit);

        $total = Station::where('title', 'like', '%' . $this->term . '%')->count();

        if( $this->request->is('api*') )
        {
            return response()->json($result->stations);
        }

        $view = View::make('search.station')
            ->with('result', $result)
            ->with('term', $this->term)
            ->with('total', $total);

        if($this->request->ajax()) {
            $sections = $view->renderSections();

            if($this->request->input('page') && intval($this->request->input('page')) > 1)
            {
                return $sections['pagination'];
            } else {
                return $sections['content'];
            }
        }

        $item = new \stdClass();
        $item->term = $this->term;
        getMetatags($item);

        return $view;
    }

    public function suggest(){
        $songs = Song::where('title', 'like', '%' . $this->term . '%')->limit($this->limit)->get();
        $artists = Artist::where('name', 'like', '%' . $this->term . '%')->limit($this->limit)->get();
        $albums = Album::where('title', 'like', '%' . $this->term . '%')->limit($this->limit)->get();
        $playlists = Playlist::with('user')->where('title', 'like', '%' . $this->term . '%')->limit($this->limit)->get();
        $stations = Station::where('title', 'like', '%' . $this->term . '%')->limit($this->limit)->get();
        $users = User::where('name', 'like', '%' . $this->term . '%')->limit($this->limit)->get();

        if( $this->request->is('api*') )
        {
            return response()->json([
                'songs' => $songs,
                'artists' => $artists,
                'albums'=> $albums,
                'playlists' => $playlists,
                'stations' => $stations,
                'users' => $users,
            ]);
        }
    }

    public function city()
    {
        $cities = City::where('name', 'like', '%' . $this->term . '%')->paginate(20);

        if( $this->request->is('api*') )
        {
            return response()->json($cities);
        }
    }
}