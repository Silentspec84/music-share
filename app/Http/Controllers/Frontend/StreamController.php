<?php
/**
 * Created by NiNaCoder.
 * Date: 2019-06-18
 * Time: 21:20
 */

namespace App\Http\Controllers\Frontend;

use App\Song;
use App\Artist;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\History;
use Auth;
use File;
use DB;

class StreamController
{
    private $request;
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function mp3(){
        $song = Song::withoutGlobalScopes()->findOrFail($this->request->route('id'));

        if($song->getFirstMedia('audio')->disk == 's3') {
            header("Location: " . $song->getFirstTemporaryUrl(Carbon::now()->addMinutes(intval(config('settings.s3_signed_time', 5))), 'mp3'));
            exit();
        } else {
            header('Content-type: ' . $song->getFirstMedia('audio')->mime_type);
            header('Content-Length: ' . $song->getFirstMedia('audio')->size);
            header('Content-Disposition: attachment; filename="' . $song->getFirstMedia('audio')->file_name);
            header('Cache-Control: no-cache');
            header('Accept-Ranges: bytes');
            readfile($song->getFirstMedia('audio')->getPath());

            //return response()->download($song->getFirstMedia('audio')->getPath(), $song->getFirstMedia('audio')->file_name);
            //header("Location: " . $song->getFirstMedia('audio')->getUrl());
            exit();
        }
    }

    public function hdMp3(){
        $song = Song::withoutGlobalScopes()->findOrFail($this->request->route('id'));

        if($song->getFirstMedia('hd_audio')->disk == 's3') {
            header("Location: " . $song->getFirstTemporaryUrl(Carbon::now()->addMinutes(intval(config('settings.s3_signed_time', 5))), 'mp3'));
            exit();
        } else {
            header('Content-type: ' . $song->getFirstMedia('hd_audio')->mime_type);
            header('Content-Length: ' . $song->getFirstMedia('hd_audio')->size);
            header('Content-Disposition: attachment; filename="' . $song->getFirstMedia('hd_audio')->file_name);
            header('Cache-Control: no-cache');
            header('Accept-Ranges: bytes');
            readfile($song->getFirstMedia('hd_audio')->getPath());
            exit();
        }
    }

    public function hls(){
        $song = Song::withoutGlobalScopes()->findOrFail($this->request->route('id'));

        if($song->getFirstMedia('hls')->disk == 's3') {
            //$content = file_get_contents($song->getFirstTemporaryUrl(Carbon::now()->addMinutes(intval(config('settings.s3_signed_time', 5))), 'm3u8'));
            $content = stream_get_contents($song->getFirstMedia('hd_m3u8')->stream());
            foreach ($song->getMedia('hls') as $track) {
                $content = str_replace($track->file_name, $track->getTemporaryUrl(Carbon::now()->addMinutes(intval(config('settings.s3_signed_time', 5)))), $content);
            }
        } else {
            $content = stream_get_contents($song->getFirstMedia('m3u8')->stream());
            foreach ($song->getMedia('hls') as $track) {
                $content = str_replace($track->file_name, $track->getFullUrl(), $content);
            }
        }

        return response($content)
            ->withHeaders([
                'Content-Type' => 'text/plain',
                'Cache-Control' => 'no-store, no-cache',
                'Content-Disposition' => 'attachment; filename="track.m3u8',
            ]);
    }

    public function hlsHD(){
        $song = Song::withoutGlobalScopes()->findOrFail($this->request->route('id'));

        if($song->getFirstMedia('hd_hls')->disk == 's3') {
            $content = stream_get_contents($song->getFirstMedia('hd_m3u8')->stream());
            foreach ($song->getMedia('hd_hls') as $track) {
                $content = str_replace($track->file_name, $track->getTemporaryUrl(Carbon::now()->addMinutes(intval(config('settings.s3_signed_time', 5)))), $content);
            }
        } else {
            $content = stream_get_contents($song->getFirstMedia('hd_m3u8')->stream());
            foreach ($song->getMedia('hd_hls') as $track) {
                $content = str_replace($track->file_name, $track->getFullUrl(), $content);
            }
        }

        return response($content)
            ->withHeaders([
                'Content-Type' => 'text/plain',
                'Cache-Control' => 'no-store, no-cache',
                'Content-Disposition' => 'attachment; filename="track.m3u8',
            ]);
    }

    public function onTrackPlayed(){
        //Increase song plays time
        $song = Song::findOrFail($this->request->input('id'));
        $song->increment('plays');
        if(isset($song->artists[0])) {
            $artist_id = $song->artists[0]->id;
            //Insert song statistic
            DB::statement("INSERT INTO " . DB::getTablePrefix() . "popular (`song_id`, `artist_id`, `plays`, `created_at`) VALUES (" . intval($song->id). ", '" . $artist_id . "', '1', '" . Carbon::now() . "') ON DUPLICATE KEY UPDATE plays=plays+1");
        }

        if(auth()->check())
        {
            makeActivity(auth()->user()->id, $song->id, (new Song)->getMorphClass(), 'playSong', $song->id);
            History::updateOrCreate(
                [
                    'user_id' => auth()->user()->id,
                    'historyable_id' => $song->id,
                    'historyable_type' => (new Song)->getMorphClass(),
                ],
                [
                    'created_at' => Carbon::now(),
                    'ownerable_type' => (new Artist)->getMorphClass(),
                    'ownerable_id' => isset($song->artists[0]) ? $song->artists[0]->id : null,
                    'interaction_count' => DB::raw('interaction_count + 1')
                ]
            );
            return response()->json(array('success' => true));
        }
    }
}