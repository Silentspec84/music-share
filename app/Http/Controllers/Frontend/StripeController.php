<?php
/**
 * Created by NiNaCoder.
 * Date: 2019-08-06
 * Time: 17:06
 */

namespace App\Http\Controllers\Frontend;

use App\Email;
use Illuminate\Http\Request;
use App\Order;
use App\Service;
use DB;
use Auth;
use Carbon\Carbon;
use App\Subscription;
use Stripe\StripeClient;

class StripeController
{
    private $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function subscription()
    {
        if(auth()->user()->subscription) {
            abort(403, 'You are already have a subscription.');
        }

        $this->request->validate([
            'planId' => 'required|integer',
            'stripeToken' => 'required|string',
        ]);

        $service = Service::findOrFail($this->request->input('planId'));

        $stripe = new StripeClient(config('settings.payment_stripe_test_mode') ? config('settings.payment_stripe_test_key') : env('STRIPE_SECRET_API'));

        $product = $stripe->products->create([
            'name' => $service->title,
        ]);


        $plan = $stripe->plans->create([
            "amount" => in_array(config('settings.currency', 'USD'), config('payment.currency_decimals')) ? ($service->price  * 100) : (intval($service->price) * 100),
            "interval" => "month",
            'product' => $product->id,
            "currency" => config('settings.currency', 'USD')
        ]);

        $customer = $stripe->customers->create([
            "email" => auth()->user()->email,
            "source" => config('settings.payment_stripe_test_mode') ? 'tok_visa' : $this->request->input('stripeToken')
        ]);

        if($service->trial) {
            switch ($service->trial_period_format) {
                case 'D':
                    $trial_end = Carbon::now()->addDay($service->trial_period);
                    break;
                case 'W':
                    $trial_end = Carbon::now()->addWeek($service->trial_period);
                    break;
                case 'M':
                    $trial_end = Carbon::now()->addMonth($service->trial_period);
                    break;
                case 'Y':
                    $trial_end = Carbon::now()->addYear($service->trial_period);
                    break;
                default:
                    $trial_end = 'now';
            }
        } else {
            $trial_end = 'now';
        }

        $stripe_subscription = $stripe->subscriptions->create([
            'customer' => $customer->id,
            'items' => [
                ['plan' => $plan->id],
            ],
            'trial_end' => ($trial_end == 'now' ? $trial_end : $trial_end->timestamp),
        ]);

        if($stripe_subscription->id) {
            $subscription = new Subscription();
            $subscription->gate = 'stripe';
            $subscription->user_id = auth()->user()->id;
            $subscription->service_id = $service->id;
            $subscription->payment_status = 1;
            $subscription->transaction_id = $stripe_subscription->id;
            $subscription->token = $stripe_subscription->id;
            $subscription->next_billing_date = Carbon::parse($stripe_subscription->current_period_end);
            $subscription->trial_end = ($trial_end == 'now' ? Carbon::now() : $trial_end);
            $subscription->amount = $service->price;
            $subscription->currency = config('settings.currency', 'USD');
            if($stripe_subscription->status == 'active') {
                $subscription->cycles = $stripe_subscription->plan->interval_count;
                $subscription->last_payment_date = Carbon::now();
            }

            $subscription->save();

            (new Email)->subscriptionReceipt(auth()->user(), $subscription);

            return response()->json($subscription);

        } else {
            abort(500, 'Payment failed!');
        }
    }
}