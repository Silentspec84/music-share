<?php
/**
 * Created by NiNaCoder.
 * Date: 2019-07-04
 * Time: 22:09
 */

namespace App\Http\Controllers\Frontend;

use App\Genre;
use App\Mood;
use View;
use Illuminate\Http\Request;
use Auth;
use App\Upload;
use App\Role;

class UploadController
{
    private $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function index()
    {
        if(! Role::getValue('artist_allow_upload') || ! auth()->user()->artist_id) {
            abortNoPermission();
        }

        $allowGenres = Genre::findMany(Role::getValue('artist_allow_genre'));
        $allowMoods = Mood::findMany(Role::getValue('artist_allow_mood'));

        $view = View::make('upload.index')
            ->with('allowGenres', $allowGenres)
            ->with('allowMoods', $allowMoods);

        if($this->request->ajax()) {
            $sections = $view->renderSections();
            return $sections['content'];
        }

        return $view;
    }

    public function upload()
    {
        /** Check if user have permission to upload */

        if(! Role::getValue('artist_allow_upload') || ! auth()->user()->artist_id) {
            abortNoPermission();
        }

        if(auth()->user()->artist_id) {
            $res = (new Upload)->handle($this->request, $artistIds = auth()->user()->artist_id);
        } else {
            $res = (new Upload)->handle($this->request);
        }

        return response()->json($res);
    }
}