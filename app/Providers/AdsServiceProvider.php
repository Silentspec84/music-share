<?php


namespace App\Providers;

use Carbon\Carbon;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;
use Cache;
use App\Banner;
use App\Role;

class AdsServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        if(Cache::has('banners')) {
            $banners = Cache::get('banners');
        } else {
            $banners = Banner::all();
            Cache::forever('banners', $banners);
        }

        foreach ($banners as $banner) {
            if((! $banner->started_at || $banner->started_at < Carbon::now()) || ($banner->ended_at || $banner->ended_at > Carbon::now())) {
                if(Role::getValue('ad_support') && $banner->approved){
                    View::share('banner_' . $banner->banner_tag, $banner->code);
                } else {
                    View::share('banner_' . $banner->banner_tag, '');
                }
            }
        }
    }
}