<?php
/**
 * Created by NiNaCoder.
 * Date: 2019-07-05
 * Time: 18:01
 */

namespace App;

use Auth;
use Illuminate\Support\Str;
use Response;
use DB;
use Config;
use Image;
use Storage;
use Validator;
use FFMpeg;
use File;

class Upload
{
    public function handle($request, $artistIds = null, $album_id = null, $isAdminPanel = false)
    {
        /** validate file format */
        $request->validate([
            'file' => 'required|mimetypes:audio/mpeg,audio/mp3,application/octet-stream|max:' . config('settings.max_audio_file_size', 10240),
        ]);

        //application/octet-stream,audio/ogg,audio/x-wav,audio/mpeg,audio/flac,audio/x-hx-aac-adts,audio/x-m4a,video/mp4

        if ($request->file('file')->isValid()) {
            $file_name_with_extension = $request->file('file')->getClientOriginalName();
            $file_name_with_extension = Str::random(10) . '_' . $file_name_with_extension;

            $getID3 = new \getID3;
            $mp3Info = $getID3->analyze($request->file('file')->getPathName());
            /** validate file information */
            $data = [
                'fileType' => $mp3Info['audio']['dataformat'],
                'bitRate' => intval($mp3Info['audio']['bitrate'] / 1000),
                'playtimeSeconds' => intval($mp3Info['playtime_seconds'])
            ];

            Validator::make($data, [
                'fileType' => ['required', 'regex:(mp3)'],
                'bitRate' => ['required', 'numeric', 'min:' . config('settings.min_audio_bitrate', 64), 'max:' . config('settings.max_audio_bitrate', 320)],
                'playtimeSeconds' => ['required', 'numeric', 'min:' . config('settings.min_audio_duration', 60), 'max:' . config('settings.max_audio_duration', 300)],
            ])->validate();

            $trackInfo = array();

            if (isset($mp3Info['tags']['id3v2']['title'][0]))
            {
                $trackInfo = $mp3Info['tags']['id3v2'];
            } elseif (isset($mp3Info['tags']['id3v1']))
            {
                $trackInfo = $mp3Info['tags']['id3v1'];
            }

            $song = new Song();

            //get track name from ID3
            isset($trackInfo['title'][0]) ? $song['title'] = $trackInfo['title'][0] : $song['title'] = pathinfo($file_name_with_extension, PATHINFO_FILENAME);
            $song->duration = intval($mp3Info['playtime_seconds']);

            if($artistIds)
            {
                $song->artistIds = $artistIds;
            } else {
                if($isAdminPanel)
                {
                    if (isset($trackInfo['artist'][0])) {
                        $artistName = $trackInfo['artist'][0];
                        $row = Artist::where('name', '=', $artistName)->first();
                        if (isset($row->id)) {
                            $song->artistIds = $row->id;
                            $song->genre = $row->genre;
                            $song->mood = $row->mood;
                        } else {
                            $artist = new Artist();
                            $artist->name = $artistName ? $artistName : 'Various Artists';
                            $artist->save();
                            $song->artistIds = $artist->id;
                        }
                    } else {
                        $artist = new Artist();
                        $artist->name = 'Various Artists';
                        $artist->save();
                        $song->artistIds = $artist->id;
                    }
                }
            }

            /**
             * If song doesn't contain album info then check for artwork only
             */
            if (isset($mp3Info['comments']['picture']['0']['data'])) {
                $song->addMediaFromBase64(base64_encode(Image::make($mp3Info['comments']['picture']['0']['data'])->fit(intval(config('settings.image_artwork_max', 500)),  intval(config('settings.image_artwork_max', 500)))->encode('jpg', config('settings.image_jpeg_quality', 90))->encoded))
                    ->usingFileName(Str::random(10) . '.jpg')
                    ->toMediaCollection('artwork', config('settings.storage_artwork_location', 'public'));
            }

            /**
             * Detect FFmpeg installation and Version
             */

            if(config('settings.ffmpeg') && @shell_exec(env('FFMPEG_PATH') . ' -version')) {
                if(! config('settings.audio_stream_hls') || config('settings.audio_stream_hls') && config('settings.audio_mp3_backup')) {
                    $defaultBitrateMp3 = Str::random(10) . '.mp3';
                    $hdBitrateMp3 = Str::random(10) . '.hd.mp3';

                    $ffmpeg = FFMpeg\FFMpeg::create([
                        'ffmpeg.binaries' => env('FFMPEG_PATH', '/usr/bin/ffmpeg'),
                        'ffprobe.binaries' => env('FFPROBE_PATH', '/usr/bin/ffprobe'),
                        'timeout' => 3600,
                        'ffmpeg.threads' => 12,
                    ]);

                    if (intval($mp3Info['audio']['bitrate'] / 1000) == config('settings.audio_default_bitrate', 128)) {
                        if(config('settings.audio_stream_hls')) {
                            File::copy($request->file('file')->getPathName(), Storage::disk('public')->path($defaultBitrateMp3));
                            $song->addMedia(Storage::disk('public')->path($defaultBitrateMp3))
                                ->usingFileName($request->file('file')->getClientOriginalName(), PATHINFO_FILENAME)
                                ->withCustomProperties(['bitrate' => config('settings.audio_default_bitrate', 128)])
                                ->toMediaCollection('audio', config('settings.storage_audio_location', 'public'));
                        } else {
                            $song->addMedia($request->file('file')->getPathName())
                                ->usingFileName($request->file('file')->getClientOriginalName(), PATHINFO_FILENAME)
                                ->withCustomProperties(['bitrate' => config('settings.audio_default_bitrate', 128)])
                                ->toMediaCollection('audio', config('settings.storage_audio_location', 'public'));
                        }
                    } else {
                        $audio = $ffmpeg->open($request->file('file')->getPathName());
                        $audio->save((new FFMpeg\Format\Audio\Mp3())->setAudioKiloBitrate(config('settings.audio_default_bitrate', 128)), Storage::disk('public')->path($defaultBitrateMp3));
                        $song->addMedia(Storage::disk('public')->path($defaultBitrateMp3))
                            ->usingFileName($request->file('file')->getClientOriginalName(), PATHINFO_FILENAME)
                            ->withCustomProperties(['bitrate' => config('settings.audio_default_bitrate', 128)])
                            ->toMediaCollection('audio', config('settings.storage_audio_location', 'public'));
                    }

                    if (config('settings.audio_hd', false) && intval($mp3Info['audio']['bitrate'] / 1000) >= config('settings.audio_hd_bitrate', 320)) {
                        if (intval($mp3Info['audio']['bitrate'] / 1000) == config('settings.audio_hd_bitrate', 320)) {
                            if(config('settings.audio_stream_hls')) {
                                File::copy($request->file('file')->getPathName(), Storage::disk('public')->path($hdBitrateMp3));
                                $song->addMedia(Storage::disk('public')->path($hdBitrateMp3))
                                    ->usingFileName($request->file('file')->getClientOriginalName(), PATHINFO_FILENAME)
                                    ->withCustomProperties(['bitrate' => config('settings.audio_hd_bitrate', 320)])
                                    ->toMediaCollection('audio', config('settings.storage_audio_location', 'public'));
                            } else {
                                $song->addMedia($request->file('file')->getPathName())
                                    ->usingFileName($request->file('file')->getClientOriginalName(), PATHINFO_FILENAME)
                                    ->withCustomProperties(['bitrate' => config('settings.audio_hd_bitrate', 320)])
                                    ->toMediaCollection('audio', config('settings.storage_audio_location', 'public'));
                            }
                        } else {
                            $audio = $ffmpeg->open($request->file('file')->getPathName());
                            $audio = $audio->save((new FFMpeg\Format\Audio\Mp3())->setAudioKiloBitrate(config('settings.audio_hd_bitrate', 320)), Storage::disk('public')->path($hdBitrateMp3));
                            $song->addMedia(Storage::disk('public')->path($hdBitrateMp3))
                                ->usingFileName($request->file('file')->getClientOriginalName(), PATHINFO_FILENAME)
                                ->withCustomProperties(['bitrate' => config('settings.audio_hd_bitrate', 320)])
                                ->toMediaCollection('hd_audio', config('settings.storage_audio_location', 'public'));
                        }
                        $song->hd = 1;
                    }
                    $song->mp3 = 1;
                }

                if(config('settings.audio_stream_hls')) {
                    //Create normal hls file
                    $tempFolder = Str::random(32);
                    Storage::disk('public')->makeDirectory($tempFolder);
                    $tempFile = Str::random(32);
                    exec(env('FFMPEG_PATH', '/usr/bin/ffmpeg') . ' -i ' . $request->file('file')->getPathName() . ' -c:a aac -b:a ' . intval(config('settings.audio_default_bitrate', 128)) . 'k -vn -hls_list_size 0 -hls_time 20 ' . Storage::disk('public')->path($tempFolder . '/' . $tempFile. '.m3u8'), $status, $var);


                    if($var == 0){
                        foreach (File::allFiles(Storage::disk('public')->path($tempFolder)) as $file) {
                            if(ends_with($file, ['.ts'])){
                                $song->addMediaFromDisk($tempFolder . '/' . trim(basename($file).PHP_EOL), 'public')->toMediaCollection('hls', config('settings.storage_audio_location', 'public'));
                            }
                        }
                        $song->hls = 1;
                        $song->addMedia(Storage::disk('public')->path($tempFolder . '/' . $tempFile. '.m3u8'))
                            ->withCustomProperties(['bitrate' => config('settings.audio_default_bitrate', 128)])
                            ->toMediaCollection('m3u8', config('settings.storage_audio_location', 'public'));
                        //Delete the temp file
                        $song->save();
                        Storage::disk('public')->deleteDirectory($tempFolder, true);
                        sleep(1);
                        Storage::disk('public')->deleteDirectory($tempFolder);
                    } else {
                        abort(500, 'FFMPEG HLS failed!');
                    }

                    //Create HD hls file
                    if(config('settings.audio_hd', false) && intval($mp3Info['audio']['bitrate'] / 1000) >= config('settings.audio_hd_bitrate', 320)) {
                        $tempFolder = Str::random(32);
                        Storage::disk('public')->makeDirectory($tempFolder);
                        exec(env('FFMPEG_PATH', '/usr/bin/ffmpeg') . ' -i ' . $request->file('file')->getPathName() . ' -c:a aac -b:a ' . intval(config('settings.audio_hd_bitrate', 320)) . 'k -vn -hls_list_size 0 -hls_time 20 ' . Storage::disk('public')->path($tempFolder . '/' . $tempFile. '.m3u8'), $status, $var);

                        if($var == 0){
                            foreach (File::allFiles(Storage::disk('public')->path($tempFolder)) as $file) {
                                if(ends_with($file, ['.ts'])){
                                    $song->addMediaFromDisk($tempFolder . '/' . trim(basename($file).PHP_EOL), 'public')->toMediaCollection('hd_hls', config('settings.storage_audio_location', 'public'));
                                }
                            }
                            $song->hls = 1;
                            $song->addMedia(Storage::disk('public')
                                ->path($tempFolder . '/' . $tempFile. '.m3u8'))
                                ->withCustomProperties(['bitrate' => config('settings.audio_hd_bitrate', 320)])
                                ->toMediaCollection('hd_m3u8', config('settings.storage_audio_location', 'public'));
                            $song->save();
                            //Delete the temp file
                            Storage::disk('public')->deleteDirectory($tempFolder, true);
                            sleep(1);
                            Storage::disk('public')->deleteDirectory($tempFolder);
                        } else {
                            abort(500, 'FFMPEG HLS HD failed!');
                        }
                        $song->hd = 1;
                        $song->save();
                        Storage::disk('public')->deleteDirectory($tempFolder, true);
                        sleep(1);
                        Storage::disk('public')->deleteDirectory($tempFolder);
                    }
                }
            } else {
                $song->addMedia($request->file('file')->getPathName())
                    ->usingFileName($request->file('file')->getClientOriginalName(), PATHINFO_FILENAME)
                    ->withCustomProperties(['bitrate' => intval($mp3Info['audio']['bitrate'] / 1000)])
                    ->toMediaCollection('audio', config('settings.storage_audio_location', 'public'));
                $song->mp3 = 1;
            }

            $song->user_id = auth()->user()->id;

            if($isAdminPanel)
            {
                $song->approved = 1;
            }

            $song->save();

            if($album_id) {
                DB::table('album_songs')->insert([
                    'song_id' => $song->id,
                    'album_id' => $album_id,
                ]);
            } elseif($isAdminPanel) {
                if (isset($trackInfo['album'][0])) {
                    $row = Album::where('title', '=', $trackInfo['album'][0])->first();

                    if (isset($row->id)) {
                        DB::table('album_songs')->insert(
                            [ 'song_id' => $song->id, 'album_id' => $row->id ]
                        );
                    } else {
                        $album = new Album();
                        $album->title = $trackInfo['album'][0];
                        $album->artistIds = $song['artistIds'];
                        $album->approved = 1;

                        if (isset($mp3Info['comments']['picture']['0']['data'])) {
                            $album->addMediaFromBase64(base64_encode(Image::make($mp3Info['comments']['picture']['0']['data'])->fit(intval(config('settings.image_artwork_max', 500)),  intval(config('settings.image_artwork_max', 500)))->encode('jpg', config('settings.image_jpeg_quality', 90))->encoded))
                                ->usingFileName(Str::random(10) . '.jpg')
                                ->toMediaCollection('artwork', config('settings.storage_artwork_location', 'public'));
                        }

                        $album->save();
                        DB::table('album_songs')->insert(
                            [ 'song_id' => $song->id, 'album_id' => $album->id ]
                        );
                    }
                }
            }

            return $song;
        }
    }
}