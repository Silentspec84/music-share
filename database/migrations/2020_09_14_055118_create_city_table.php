<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCityTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('city', function (Blueprint $table) {
            $table->integer('id', true);
            $table->char('name', 35)->default('');
            $table->char('country_code', 3)->default('')->index('CountryCode');
            $table->char('district', 20)->default('');
            $table->boolean('fixed')->default(0)->index('fixed');
            $table->boolean('visibility')->default(1)->index('visibility');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('city');
    }
}
