<?php

use Illuminate\Database\Seeder;

class BannersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('banners')->delete();
        
        \DB::table('banners')->insert(array (
            0 => 
            array (
                'id' => 2,
                'banner_tag' => 'header',
                'description' => 'Top banner',
                'code' => '<div style="text-align:center;"><a href="http://ninacoder.info" target="_blank"><img src="https://user-images.githubusercontent.com/63708869/93670112-d5747500-fac2-11ea-8bdf-56427ed7d2a2.jpg" style="border: none;" alt="" /></a></div>',
                'approved' => 1,
                'short_place' => 0,
                'bstick' => 0,
                'main' => 0,
                'category' => '',
                'group_level' => 'all',
                'started_at' => '2020-09-08 13:48:00',
                'ended_at' => '2020-09-30 13:46:00',
                'fpage' => 0,
                'innews' => 0,
                'device_level' => '',
                'allow_views' => 0,
                'max_views' => 0,
                'allow_counts' => 0,
                'max_counts' => 0,
                'views' => 0,
                'clicks' => 0,
                'rubric' => 0,
                'created_at' => '2020-09-07 13:48:00',
                'updated_at' => '2020-09-19 14:55:54',
            ),
        ));
        
        
    }
}