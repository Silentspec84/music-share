(function (factory) {
    if (typeof define === 'function' && define.amd) {
        define(['jquery'], factory);
    } else if (typeof exports === 'object') {
        factory(require('jquery'));
    } else {
        factory(jQuery);
    }
})(function ($) {
    "use strict";

    window.Artist = {
        chartReady: false,
        select2Ready: false,
        form: $("#upload-song-form"),
        editSongForm: $("#edit-song-form"),
        file: $("#uploadFile"),
        lightbox: $(".lightbox-uploadSong"),
        editSongSaveBtn: $("#edit-song-save-btn"),
        genreSelection: $("#genre-selection"),
        moodSelection: $("#mood-selection"),
        albumSongsSelection: $("#album-songs-selection"),
        albumGenreSelection: $("#album-genre-selection"),
        albumMoodSelection: $("#album-mood-selection"),
        createAlbumForm: $("#create-album-form"),
        editAlbumForm: $("#edit-album-form"),
        init: function () {

        },
        upload: {
            chart: {
                config: {
                    // The type of chart we want to create
                    type: 'line',

                    // The data for our dataset
                    data: {
                        labels: ['time'],
                        datasets: [{
                            label: 'Speed (Kbps)',
                            backgroundColor: '#92bf59',
                            borderColor: '#729943',
                            data: [0]
                        }]
                    },

                    // Configuration options go here
                    options: {
                        maintainAspectRatio: false,
                        scales: {
                            xAxes: [{
                                stacked: false,
                                ticks: {
                                    beginAtZero: true,
                                    mirror: false,
                                    suggestedMin: 0,
                                    suggestedMax: 100
                                }
                            }],
                            yAxes: [{
                                stacked: true
                            }]
                        },
                    }
                },
                update: function (time, speed) {
                    //if(Artist.upload.chart.config.data.datasets.length > 30) Artist.upload.chart.config.data.datasets.splice(0, 2)
                    if (Artist.upload.chart.config.data.datasets.length > 0) {
                        if (Artist.upload.chart.config.data.labels.length > 10) Artist.upload.chart.config.data.labels.splice(0, 1);
                        Artist.upload.chart.config.data.labels.push(time);
                        Artist.upload.chart.config.data.datasets.forEach(function (dataset) {
                            if (dataset.data.length > 10) dataset.data.splice(0, 1);
                            dataset.data.push(speed);
                        });
                        if ($('#uploadSpeedChart').length) {
                            window.chart.update();
                        }
                    }
                }
            },
            reinventForm: function () {
                $('#fileupload').fileupload({
                    url: route.route('frontend.upload'),
                    maxNumberOfFiles: 1,
                    limitMultiFileUploads: 10,
                    limitConcurrentUploads: 1
                    //maxFileSize: 40000
                });
                if ($('#uploadSpeedChart').length) {
                    var ctx = document.getElementById('uploadSpeedChart').getContext('2d');
                    window.chart = new Chart(ctx, Artist.upload.chart.config);
                }
            },
            edit: function (response, $form) {
                __DEV__ && console.log(response);
                $form.parent().fadeOut(500);
                if (response.approved) {
                    Toast.show('success', 'Your song "' + response.title + '" has been published!');
                } else {
                    Toast.show('success', 'Your song has been submitted! You will receive an email once it has been approved.');
                }
            }
        },
        claim: {
            el: $('#claimArtistLightbox'),
            continueButton: $(".lightbox-claimArtist .continue"),
            requestButton: $(".lightbox-claimArtist .submit"),
            finishedButton: $(".lightbox-claimArtist .finished"),
            error: $(".lightbox-claimArtist .error"),
            emailInput: $("#artist-claiming-email"),
            fullNameInput: $("#artist-claiming-fname"),
            phoneInput: $("#artist-claiming-phone"),
            phoneExtInput: $("#artist-claiming-phone-ext"),
            affiliationInput: $("#artist-claiming-affiliation"),
            messageInput: $("#artist-claiming-message"),
            termCheck: $("#accept-terms"),
            facebookAccessToken: null,
            init: function (el) {
                $.engineLightBox.hide();
                if (!User.isLogged()) {
                    User.SignIn.show();
                    return false;
                }
                $.engineLightBox.show("lightbox-claimArtist");
                Artist.claim.emailInput.val(User.userInfo.email);
                Artist.claim.fullNameInput.val(User.userInfo.name);

                if (el.data('id')) {
                    var container = $('#window-claim-artist-selector');
                    container.removeClass('hide');
                    var selected = container.find('.window-selector-selected');
                    var artist = window['artist_data_' + el.data('id')];
                    selected.find('.img-container img').attr('src', artist.artwork_url);
                    selected.find('.metadata .title').html(artist.name);
                    container.find('[name="artist_id"]').val(artist.id)
                    container.find('[name="artist_name"]').val(artist.name);
                }
            },
            show: {
                account: function () {
                    $('.lightbox-stage').removeClass('active');
                    $('.lightbox-stage[rel="account"]').addClass('active');
                    $('.claiming-stage').addClass('hide');
                    $('#artist-claiming-stage-account').removeClass('hide');
                    Artist.claim.requestButton.addClass('hide');
                    Artist.claim.el.find('.back').addClass('hide').unbind('click');
                    Artist.claim.continueButton.removeClass('hide');
                },
                information: function () {
                    $('.lightbox-stage').removeClass('active');
                    $('.lightbox-stage[rel="info"]').addClass('active');
                    $('.claiming-stage').addClass('hide');
                    $('#artist-claiming-stage-info').removeClass('hide');
                    Artist.claim.continueButton.addClass('hide');
                    Artist.claim.requestButton.removeClass('hide');
                    Artist.claim.error.addClass('hide');
                    Artist.claim.el.find('.back').removeClass('hide').one('click', Artist.claim.show.account);
                },
                finished: function () {
                    $('.lightbox-stage').removeClass('active');
                    $('.lightbox-stage[rel="done"]').addClass('active');
                    $('.claiming-stage').addClass('hide');
                    $('#artist-claiming-stage-message').removeClass('hide');
                    Artist.claim.continueButton.addClass('hide');
                    Artist.claim.requestButton.addClass('hide');
                    Artist.claim.finishedButton.removeClass('hide');
                    Artist.claim.error.addClass('hide');
                    Artist.claim.el.find('.back').addClass('hide');
                }
            },
            account: function () {
                $.ajax({
                    url: "/auth/user/artistClaim",
                    data: {
                        stage: 'account',
                        email: Artist.claim.emailInput.val(),
                        fullName: Artist.claim.fullNameInput.val()
                    },
                    type: "post",
                    success: function (response) {
                        Artist.claim.show.information();
                        if (response.length) {
                            $.each(response, function (i, item) {
                                if (item.service === 'facebook') {
                                    Artist.claim.el.find('.facebook-icon-container > img').attr('src', item.provider_artwork).removeClass('hide');
                                    Artist.claim.el.find('.facebook-icon-container > .icon').addClass('hide');
                                    Artist.claim.el.find('.facebook .icon-message').html('Connected as ' + item.provider_name);
                                    Artist.claim.el.find('.facebook .btn').addClass('hide');
                                }
                            });
                        }
                        /*
                        if (response.success === true) {
                            Artist.claim.show.information();
                            FB.getLoginStatus(function (response) {
                                if(response.authResponse) {
                                    Artist.claim.facebookAccessToken = response.authResponse.accessToken;
                                    FB.api('/me', function (response) {
                                        __DEV__ && console.log(response);
                                        Artist.claim.renderFacebookConnect(response);
                                    });
                                } else {
                                    Artist.claim.el.find('.facebook .btn').bind('click', function(){
                                        FB.login(function(response) {
                                            FB.getLoginStatus(function (response) {
                                                if(response.authResponse){
                                                    Artist.claim.facebookAccessToken = response.authResponse.accessToken;
                                                    FB.api('/me', function (response) {
                                                        __DEV__ && console.log(response);
                                                        Artist.claim.renderFacebookConnect(response);
                                                    });
                                                }
                                            });
                                        });
                                    });
                                }
                            });
                        }
                        */


                    },
                    error: function (jqXHR) {
                        var serverMsg = JSON.parse(jqXHR.responseText);
                        Artist.claim.error.removeClass('hide').html(serverMsg.errors[Object.keys(serverMsg.errors)[0]][0]);
                    }
                });
            },
            search: function (response) {
                var container = $('#window-claim-artist-selector');
                container.removeClass('hide');
                var el = $('.window-selector-selected');
                if (response.data.length) {
                    container.removeClass('hide');
                    var artists = response.data;
                    el.find('.img-container img').attr('src', artists[0].artwork_url);
                    el.find('.metadata .title').html(artists[0].name);
                    container.find('[name="artist_id"]').val(artists[0].id)
                    container.find('[name="artist_name"]').val(artists[0].name);

                    if (artists.length === 1) {
                        container.find('.others').addClass('hide');
                    } else {
                        container.find('.others').removeClass('hide');
                    }

                    for (var i = 0; i < artists.length; i++) {
                        if (i !== 0) {
                            var row = el.find('.module').clone();
                            row.find('.img-container img').attr('src', artists[i].artwork_url);
                            row.find('.metadata .title').html(artists[i].name);
                            row.attr('data-id', artists[i].id)
                            $('.window-selector-others').prepend(row);
                            row.bind('click', function () {
                                el.find('.img-container img').attr('src', $(this).find('.img-container img').attr('src'));
                                el.find('.metadata .title').html($(this).find('.metadata .title').html());
                                container.find('[name="artist_id"]').val($(this).data('id'));
                                container.find('[name="artist_name"]').val($(this).find('.metadata .title').html());
                            });
                        }
                    }
                } else {
                    el.find('.img-container img').attr('src', route.route('frontend.homepage') + 'common/default/artist.png');
                    el.find('.metadata .title').html($('#artist-claim-search-form').find('input').val());
                    container.find('.others').addClass('hide');
                    container.find('[name="artist_id"]').val(0)
                    container.find('[name="artist_name"]').val($('#artist-claim-search-form').find('input').val());
                }
            },
            information: function () {
                $.engineLightBox.checkError(Artist.claim.phoneInput, 'artist-claiming-phone');
                $.engineLightBox.checkError(Artist.claim.affiliationInput, 'artist-claiming-affiliation');
                var container = $('#window-claim-artist-selector');

                if (!Artist.claim.termCheck.is(":checked")) {
                    Artist.claim.error.removeClass('hide').html(Language.text.AGREE_ARTIST_TERMS_FAILED);
                    return false;
                }
                $.ajax({
                    url: route.route('frontend.auth.user.artistClaim'),
                    data: {
                        stage: 'info',
                        email: Artist.claim.emailInput.val(),
                        fullName: Artist.claim.fullNameInput.val(),
                        phone: Artist.claim.phoneInput.val(),
                        ext: Artist.claim.phoneExtInput.val(),
                        affiliation: Artist.claim.affiliationInput.val(),
                        message: Artist.claim.messageInput.val(),
                        artist_id: container.find('[name="artist_id"]').val(),
                        artist_name: container.find('[name="artist_name"]').val(),
                        facebookAccessToken: Artist.claim.facebookAccessToken
                    },
                    type: "post",
                    beforeSend: function () {
                        Artist.claim.requestButton.attr('disabled', 'disabled').unbind('click');
                    },
                    success: function (response) {
                        if (response.success === true) {
                            Artist.claim.show.finished();
                        }
                    },
                    error: function (jqXHR) {
                        var serverMsg = JSON.parse(jqXHR.responseText);
                        Artist.claim.error.removeClass('hide').html(serverMsg.errors[Object.keys(serverMsg.errors)[0]][0]);
                        Artist.claim.requestButton.removeAttr('disabled').one('click', Artist.claim.information);
                    }
                });

            }
        },
        chart: {
            loadData: function () {
                var a = window.location.href.toString().split(window.location.host)[1];
                var b = a.split("/")[2];
                a = a.split("/")[1];
                if (a !== "artist-management" && a !== "song") return false;
                if (!$(".artist-management-chart").length) return false;
                __DEV__ && console.log("Loading stats chart");
                var url;
                if (a === "song") url = "/artist-management/chart/song/" + b;
                else url = route.route('frontend.auth.user.artist.manager.chart.overview');
                $.ajax({
                    url: url,
                    type: "post",
                    success: function (response) {
                        Artist.chart.buildChart(response.data);
                    }
                });
            },
            buildChart: function (data) {
                window.chartColors = {
                    red: 'rgb(255, 99, 132)',
                    orange: 'rgb(255, 159, 64)',
                    yellow: 'rgb(255, 205, 86)',
                    green: 'rgb(75, 192, 192)',
                    blue: 'rgb(54, 162, 235)',
                    purple: 'rgb(153, 102, 255)',
                    grey: 'rgb(201, 203, 207)'
                };


                $("#artist-management-chart").empty();
                Chart.defaults.global.pointHitDetectionRadius = 1;

                var customTooltips = function (tooltip) {
                    // Tooltip Element
                    var tooltipEl = document.getElementById('chartjs-tooltip');

                    if (!tooltipEl) {
                        tooltipEl = document.createElement('div');
                        tooltipEl.id = 'chartjs-tooltip';
                        tooltipEl.innerHTML = '<table></table>';
                        this._chart.canvas.parentNode.appendChild(tooltipEl);
                    }

                    // Hide if no tooltip
                    if (tooltip.opacity === 0) {
                        tooltipEl.style.opacity = 0;
                        return;
                    }

                    // Set caret Position
                    tooltipEl.classList.remove('above', 'below', 'no-transform');
                    if (tooltip.yAlign) {
                        tooltipEl.classList.add(tooltip.yAlign);
                    } else {
                        tooltipEl.classList.add('no-transform');
                    }

                    function getBody(bodyItem) {
                        return bodyItem.lines;
                    }

                    // Set Text
                    if (tooltip.body) {
                        var titleLines = tooltip.title || [];
                        var bodyLines = tooltip.body.map(getBody);

                        var innerHtml = '<thead>';

                        titleLines.forEach(function (title) {
                            innerHtml += '<tr><th>' + title + '</th></tr>';
                        });
                        innerHtml += '</thead><tbody>';

                        bodyLines.forEach(function (body, i) {
                            var colors = tooltip.labelColors[i];
                            var style = 'background:' + colors.backgroundColor;
                            style += '; border-color:' + colors.borderColor;
                            style += '; border-width: 2px';
                            var span = '<span class="chartjs-tooltip-key" style="' + style + '"></span>';
                            innerHtml += '<tr><td>' + span + body + '</td></tr>';
                        });
                        innerHtml += '</tbody>';

                        var tableRoot = tooltipEl.querySelector('table');
                        tableRoot.innerHTML = innerHtml;
                    }

                    var positionY = this._chart.canvas.offsetTop;
                    var positionX = this._chart.canvas.offsetLeft;

                    // Display, position, and set styles for font
                    tooltipEl.style.opacity = 1;
                    tooltipEl.style.left = positionX + tooltip.caretX + 'px';
                    tooltipEl.style.top = positionY + tooltip.caretY + 'px';
                    tooltipEl.style.fontFamily = tooltip._bodyFontFamily;
                    tooltipEl.style.fontSize = tooltip.bodyFontSize + 'px';
                    tooltipEl.style.fontStyle = tooltip._bodyFontStyle;
                    tooltipEl.style.padding = tooltip.yPadding + 'px ' + tooltip.xPadding + 'px';
                };
                var color = Chart.helpers.color;
                var lineChartData = {
                    labels: data.period,
                    datasets: [{
                        label: 'Play',
                        fill: false,
                        data: data.playSong,
                        pointBackgroundColor: window.chartColors.green,
                        borderColor: window.chartColors.green,
                    }, {
                        label: 'Favorite',
                        borderColor: window.chartColors.red,
                        pointBackgroundColor: window.chartColors.red,
                        fill: false,
                        data: data.favoriteSong
                    }, {
                        label: 'Collection',
                        borderColor: window.chartColors.orange,
                        pointBackgroundColor: window.chartColors.orange,
                        fill: true,
                        data: data.collectSong,
                        backgroundColor: window.chartColors.orange.replace(/rgb/i, "rgba").replace(/\)/i, ',0.4)'),
                    }]
                };
                var chartEl = document.getElementById('artistManagerChart');
                window.myLine = new Chart(chartEl, {
                    type: 'line',
                    data: lineChartData,
                    options: {
                        title: {
                            display: false,
                        },
                        tooltips: {
                            enabled: false,
                            mode: 'index',
                            position: 'nearest',
                            custom: customTooltips
                        },
                        responsive: true,
                        legend: {
                            position: 'bottom',
                            fill: true
                        },
                        hover: {
                            mode: 'index'
                        },
                        maintainAspectRatio: false,
                        layout: {
                            padding: {
                                left: 10,
                                right: 25,
                                top: 25,
                                bottom: 0
                            }
                        },
                        scales: {
                            xAxes: [{
                                gridLines: {
                                    display: false,
                                    drawBorder: false,
                                    borderDash: [0],
                                },
                            }],
                            yAxes: [{
                                gridLines: {
                                    color: "rgb(234, 236, 244)",
                                    zeroLineColor: "rgb(234, 236, 244)",
                                    drawBorder: false,
                                    borderDash: [0],
                                    zeroLineBorderDash: [2]
                                }
                            }],
                        },
                    }
                });
            }
        },
        loadGenres: function (el, object_type, id) {
            el.find("option").remove();
            $.ajax({
                url: route.route('frontend.homepage') + "artist-management/genres",
                type: "post",
                data: {
                    object_type: object_type,
                    id: id
                },
                success: function (response) {
                    if (response.length) {
                        $.each(response, function (i, item) {
                            el.append($('<option>', {
                                value: item.id,
                                text: item.name,
                                selected: item.selected ? "selected" : ""
                            }));
                        });
                    }
                }
            });
        },
        loadMoods: function (el, object_type, id) {
            el.find("option").remove();
            $.ajax({
                url: route.route('frontend.homepage') + "artist-management/moods",
                type: "post",
                data: {
                    object_type: object_type,
                    id: id
                },
                success: function (response) {
                    if (response.length) {
                        $.each(response, function (i, item) {
                            el.append($('<option>', {
                                value: item.id,
                                text: item.name,
                                selected: item.selected ? "selected" : ""
                            }));
                        });
                    }
                }
            });
        },
        editSong: function (song) {
            __DEV__ && console.log('Edit song', song);

            $.engineLightBox.show("lightbox-edit-song");
            Artist.editSongForm.find(".title").html(song.title);
            Artist.editSongForm.find("input[name='title']").val(song.title);
            Artist.editSongForm.find("input[name='id']").val(song.id);
            Artist.editSongForm.find(".edit-artwork").attr("data-type", "song").attr("data-id", song.id);
            Artist.editSongForm.find(".img-container img").attr("src", song.artwork_url);
            Artist.editSongForm.find(".img-container img").attr("rel", "artwork-song-" + song.id);
            $.engineUtils.makeSelectOption(Artist.editSongForm.find('select[name=genre\\[\\]]'), User.userInfo.allow_genres);
            $.engineUtils.makeSelectOption(Artist.editSongForm.find('select[name=mood\\[\\]]'), User.userInfo.allow_moods);
            if (song.genre) {
                song.genre.split(',').forEach(function (i) {
                    Artist.editSongForm.find('select[name=genre\\[\\]] option[value="' + i + '"]').attr('selected', 'selected')
                })
            }
            if (song.mood) {
                song.mood.split(',').forEach(function (i) {
                    Artist.editSongForm.find('select[name=mood\\[\\]] option[value="' + i + '"]').attr('selected', 'selected')
                })
            }
            setTimeout(function () {
                Artist.editSongForm.find('.select2').select2({
                    width: '100%',
                    placeholder: "Please select",
                    maximumSelectionLength: 4
                });
            }, 100);
            if (song.visibility) {
                Artist.editSongForm.find('input[name="visibility"]').attr('checked', 'checked');
            } else {
                Artist.editSongForm.find('input[name="visibility"]').removeAttr('checked');
            }

            Artist.editSongSaveBtn.one('click', function () {
                Artist.editSongForm.submit();
            });
            Artist.editSongForm.find('.datepicker').datepicker();
            Artist.editSongForm.find('.edit-artwork-input').change(function () {
                var input = this;
                var url = $(this).val();
                var ext = url.substring(url.lastIndexOf('.') + 1).toLowerCase();
                if (input.files && input.files[0] && (ext === "gif" || ext === "png" || ext === "jpeg" || ext === "jpg")) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        Artist.editSongForm.find('img').attr('src', e.target.result);
                    };
                    reader.readAsDataURL(input.files[0]);
                }
            });
        },
        createAlbum: function () {
            $.engineLightBox.show("lightbox-create-album");
            Artist.loadGenres($('.lightbox-create-album select[name="genre[]"]'), "album", null);
            Artist.loadMoods($('.lightbox-create-album select[name="mood[]"]'), "album", null);
            $('#create-album-form').find('.datepicker').datepicker();
            $('#create-album-form').ajaxForm({
                beforeSubmit: function (data, $form, options) {
                    var error = 0;
                    Object.keys(data).forEach(function eachKey(key) {
                        if (data[key].required && !data[key].value) {
                            $form.find("[name='" + data[key].name + "']").closest(".control").addClass("field-error");
                            error++;
                        } else if (data[key].required && data[key].value) {
                            $form.find("[name='" + data[key].name + "']").closest(".control").removeClass("field-error");
                        }
                    });
                    if (error) return false;
                    $form.find("[type='submit']").attr("disabled", "disabled");
                },
                success: function (response, textStatus, xhr, $form) {
                    $form.find("[type='submit']").removeAttr("disabled");
                    $form.trigger("reset");
                    $.engineLightBox.hide();
                    if (response.approved) {
                        Toast.show("success", null, Language.text.POPUP_CREATED_ALBUM.replace(':albumLink', Favorite.objectLink(response.title, response.permalink_url)));
                    } else {
                        Toast.show("success", Language.text.POPUP_CREATED_ALBUM_NOT_APPROVED);
                    }
                    $(window).trigger({
                        type: "engineReloadCurrentPage"
                    });
                },
                error: function (e, textStatus, xhr, $form) {
                    var errors = e.responseJSON.errors;
                    $.each(errors, function (key, value) {
                        Toast.show("error", value[0], null);
                    });
                    $form.find('.error').removeClass('hide').html(e.responseJSON.errors[Object.keys(e.responseJSON.errors)[0]][0]);
                    $form.find("[type='submit']").removeAttr("disabled");
                }
            });
        },
        onAlbumArtworkChange: function () {
            Artist.createAlbumForm.find('.input-album-artwork').change(function () {
                var input = this;
                var url = $(this).val();
                var ext = url.substring(url.lastIndexOf('.') + 1).toLowerCase();
                if (input.files && input.files[0] && (ext === "gif" || ext === "png" || ext === "jpeg" || ext === "jpg")) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        Artist.createAlbumForm.find('img').attr('src', e.target.result);
                    };
                    reader.readAsDataURL(input.files[0]);
                }
            });
        },
        editAlbum: function (el) {
            var album = window['album_data_' + el.data('id')];
            Artist.albumSongsSelection.find("option").remove();
            $.engineLightBox.show("lightbox-edit-album");
            Artist.editAlbumForm.find(".title").html(album.title);
            Artist.editAlbumForm.find("input[name='title']").val(album.title);
            Artist.editAlbumForm.find("input[name='id']").val(album.id);
            Artist.editAlbumForm.find(".edit-artwork").attr("data-type", "album").attr("data-id", album.id);
            Artist.editAlbumForm.find(".img-container img").attr("src", album.artwork_url);
            Artist.editAlbumForm.find(".img-container img").attr("rel", "artwork-album-" + album.id)
            $.engineUtils.makeSelectOption(Artist.editAlbumForm.find('select[name=genre\\[\\]]'), User.userInfo.allow_genres);
            $.engineUtils.makeSelectOption(Artist.editAlbumForm.find('select[name=mood\\[\\]]'), User.userInfo.allow_moods);
            if (album.genre) {
                album.genre.split(',').forEach(function (i) {
                    Artist.editAlbumForm.find('select[name=genre\\[\\]] option[value="' + i + '"]').attr('selected', 'selected')
                })
            }
            if (album.mood) {
                album.mood.split(',').forEach(function (i) {
                    Artist.editAlbumForm.find('select[name=mood\\[\\]] option[value="' + i + '"]').attr('selected', 'selected')
                })
            }
            if (album.type) {
                Artist.editAlbumForm.find('select[name=type] option[value="' + album.type + '"]').attr('selected', 'selected')
            }
            setTimeout(function () {
                Artist.editAlbumForm.find('.select2').select2({
                    width: '100%',
                    placeholder: "Please select",
                    maximumSelectionLength: 4
                });
            }, 100);

            if (album.visibility) {
                Artist.editAlbumForm.find('input[name="visibility"]').attr('checked', 'checked');
            } else {
                Artist.editAlbumForm.find('input[name="visibility"]').removeAttr('checked');
            }
            Artist.editAlbumForm.find('.datepicker').datepicker();
            Artist.editAlbumForm.find('.edit-artwork-input').change(function () {
                var input = this;
                var url = $(this).val();
                var ext = url.substring(url.lastIndexOf('.') + 1).toLowerCase();
                if (input.files && input.files[0] && (ext === "gif" || ext === "png" || ext === "jpeg" || ext === "jpg")) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        Artist.editAlbumForm.find('img').attr('src', e.target.result);
                    };
                    reader.readAsDataURL(input.files[0]);
                }
            });

        },
        deleteSong: function (song) {
            bootbox.confirm({
                title: Language.text.DELETED_SONG_MSG,
                message: Language.text.WARNING_DELETE_SONG,
                centerVertical: true,
                buttons: {
                    cancel: {
                        label: Language.text.CANCEL,
                    },
                    confirm: {
                        label: Language.text.CONFIRM_DELETE,
                    }
                },
                callback: function (result) {
                    if (result) {
                        $.ajax({
                            url: route.route('frontend.auth.user.artist.manager.song.delete'),
                            data: {
                                'id': song.id
                            },
                            type: 'post',
                            dataType: 'json',
                            success: function () {
                                $.engineUtils.cleanStorage();
                                $('.module[data-type="song"][data-id="' + song.id + '"]').fadeOut();
                                Toast.show("success", Language.text.POPUP_DELETED_SONG);
                            },
                            error: function () {
                                Toast.show("error", Language.text.POPUP_DELETE_SONG_DENIED);
                            }
                        });
                    }
                }
            });
        },
        deleteAlbum: function (el) {
            var album_id = el.data('id');
            bootbox.confirm({
                title: Language.text.DELETED_ALBUM_MSG,
                message: Language.text.WARNING_DELETE_ALBUM,
                centerVertical: true,
                confirm: {
                    label: Language.text.CONFIRM_DELETE,
                },
                buttons: {
                    cancel: {
                        label: Language.text.CANCEL,
                    },
                    confirm: {
                        label: Language.text.CONFIRM_DELETE,
                    }
                },
                callback: function (result) {
                    if (result) {
                        $.ajax({
                            url: route.route('frontend.auth.user.artist.manager.albums.delete'),
                            data: {
                                'id': album_id
                            },
                            type: 'post',
                            dataType: 'json',
                            success: function () {
                                $.engineUtils.cleanStorage();
                                $(window).trigger({
                                    type: 'engineNeedHistoryChange',
                                    href: route.route('frontend.auth.user.artist.manager.albums')
                                });
                                Toast.show("success", Language.text.POPUP_DELETED_ALBUM);
                            },
                            error: function () {
                                Toast.show("error", Language.text.POPUP_DELETE_ALBUM_DENIED);
                            }
                        });
                    }
                }
            });
        },
        sortAlbumSongs: function () {
            if (!$(".album-song-sortable").length) return false;
            __DEV__ && console.log("start sort able");
            $(".album-song-sortable").sortable({
                handle: ".drag-handle",
                scroll: false,
                cancel: "span",
                placeholder: "sortable-playlist-song-placeholder",
                cursorAt: {
                    top: 0,
                    left: 0
                },
                helper: function (e, item) {
                    var song = $.engineUtils.getSongData(item);
                    var helper = $("<div />");
                    helper.addClass('sortable-playlist-song-helper')
                    helper.append(song.title);
                    return helper;
                },
                start: function (e, ui) {
                    $('body').addClass("lock-childs");
                    ui.item.show();
                },
                update: function (e, ui) {
                    var data = {};
                    data.album_id = $(this).data("id");
                    data.nextOrder = $(this).sortable('toArray', {
                        attribute: 'data-id',
                    });
                    data.nextOrder = data.nextOrder.filter(Boolean);
                    data.nextOrder = JSON.stringify(data.nextOrder)
                    $.ajax({
                        type: 'post',
                        url: route.route('frontend.auth.user.artist.manager.albums.sort'),
                        data: data,
                        success: function () {
                            Toast.show('success', Language.text.POPUP_PLAYLIST_SAVE_DESCRIPTION)
                            $(window).trigger({
                                type: "engineSiteContentChange"
                            });
                        }
                    });
                },
                stop: function (e, ui) {
                    setTimeout(function () {
                        $('body').removeClass("lock-childs");
                    }, 500)
                }
            });
        },
        song: {
            updated: function (response, $form) {
                $(window).trigger({
                    type: "engineReloadCurrentPage"
                });
                $.engineLightBox.hide();
            }
        },
        album: {
            updated: function (response, $form) {
                $(window).trigger({
                    type: "engineReloadCurrentPage"
                });
                $.engineLightBox.hide();
            }
        },
        event: {
            create: function () {
                $.engineLightBox.show("lightbox-create-event");
            },
            edit: function (e) {
                var id = e.data('id');
                var event = window['event_data_' + id];
                var form = $('#edit-event-form');
                form.find('[name="title"]').attr('value', event.title);
                form.find('[name="location"]').attr('value', event.location);
                form.find('[name="link"]').attr('value', event.link);
                form.find('[name="started_at"]').attr('value', event.started_at);
                form.find('[name="id"]').val(event.id);
                $.engineLightBox.show("lightbox-edit-event");
            },
            delete: function (e) {
                var id = e.data('id');
                bootbox.confirm({
                    title: Language.text.POPUP_DELETE_EVENT_TITLE,
                    message: Language.text.POPUP_DELETE_EVENT_MESSAGE,
                    centerVertical: true,
                    callback: function (result) {
                        if (result) {
                            $.ajax({
                                url: route.route('frontend.auth.user.artist.manager.event.delete'),
                                type: "post",
                                data: {
                                    id: id
                                },
                                success: function (response) {
                                    $(window).trigger({
                                        type: "engineReloadCurrentPage"
                                    });
                                }
                            });
                        }
                    }
                });
            }
        },
        profile: {
            imgSelect: function () {
                var input = this;
                var url = $(this).val();
                var ext = url.substring(url.lastIndexOf('.') + 1).toLowerCase();
                if (input.files && input.files[0] && (ext === "gif" || ext === "png" || ext === "jpeg" || ext === "jpg")) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        $('#artist-profile-form').find('img.artist-picture-preview').attr('src', e.target.result);
                    };
                    reader.readAsDataURL(input.files[0]);
                } else {
                    Toast.show('error', Language.text.SETTINGS_PICTURE_FAILED, null);
                }
            },
            save: function (response, $form) {
                $("img.profile-img, img.artist-picture-preview").attr("src", response.artwork_url);
                $form.find("[type='submit']").removeAttr("disabled").addClass("btn-success");
            }
        }
    };
    $(document).on("change", "#upload-artist-pic", Artist.profile.imgSelect);
    $(document).ready(Artist.onAlbumArtworkChange);
    $(document).ready(function () {
        Artist.claim.continueButton.bind('click', Artist.claim.account);
        Artist.claim.finishedButton.bind('click', function () {
            $.engineLightBox.hide();
        });
        Artist.claim.requestButton.on('click', Artist.claim.information);
    });
    $(window).on("enginePageHasBeenLoaded", function () {
        if ($('#fileupload').length) {
            Artist.upload.reinventForm();
        }
        Artist.sortAlbumSongs();
    });
    $(window).on("enginePageHasBeenLoaded", function () {
        var a = window.location.href.toString().split(window.location.host)[1];
        if (a === '/artist-management') Artist.chart.loadData()
    });
    $(document).on('change', '.custom-file-input', function (event) {
        $(this).next('.custom-file-label').html(event.target.files[0].name);
    });
});