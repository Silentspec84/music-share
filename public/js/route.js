(function () {

    var laroute = (function () {

        var routes = {

            absolute: true,
            rootUrl: 'http://music-share.test',
            routes : [
                {"host":null,"methods":["GET","HEAD"],"uri":"_ignition\/health-check","name":"ignition.healthCheck","action":"Facade\Ignition\Http\Controllers\HealthCheckController"},
                {"host":null,"methods":["POST"],"uri":"_ignition\/execute-solution","name":"ignition.executeSolution","action":"Facade\Ignition\Http\Controllers\ExecuteSolutionController"},
                {"host":null,"methods":["POST"],"uri":"_ignition\/share-report","name":"ignition.shareReport","action":"Facade\Ignition\Http\Controllers\ShareReportController"},
                {"host":null,"methods":["GET","HEAD"],"uri":"_ignition\/scripts\/{script}","name":"ignition.scripts","action":"Facade\Ignition\Http\Controllers\ScriptController"},
                {"host":null,"methods":["GET","HEAD"],"uri":"_ignition\/styles\/{style}","name":"ignition.styles","action":"Facade\Ignition\Http\Controllers\StyleController"},
                {"host":null,"methods":["GET","HEAD"],"uri":"oauth\/authorize","name":"passport.authorizations.authorize","action":"\Laravel\Passport\Http\Controllers\AuthorizationController@authorize"},
                {"host":null,"methods":["POST"],"uri":"oauth\/authorize","name":"passport.authorizations.approve","action":"\Laravel\Passport\Http\Controllers\ApproveAuthorizationController@approve"},
                {"host":null,"methods":["DELETE"],"uri":"oauth\/authorize","name":"passport.authorizations.deny","action":"\Laravel\Passport\Http\Controllers\DenyAuthorizationController@deny"},
                {"host":null,"methods":["POST"],"uri":"oauth\/token","name":"passport.token","action":"\Laravel\Passport\Http\Controllers\AccessTokenController@issueToken"},
                {"host":null,"methods":["GET","HEAD"],"uri":"oauth\/tokens","name":"passport.tokens.index","action":"\Laravel\Passport\Http\Controllers\AuthorizedAccessTokenController@forUser"},
                {"host":null,"methods":["DELETE"],"uri":"oauth\/tokens\/{token_id}","name":"passport.tokens.destroy","action":"\Laravel\Passport\Http\Controllers\AuthorizedAccessTokenController@destroy"},
                {"host":null,"methods":["POST"],"uri":"oauth\/token\/refresh","name":"passport.token.refresh","action":"\Laravel\Passport\Http\Controllers\TransientTokenController@refresh"},
                {"host":null,"methods":["GET","HEAD"],"uri":"oauth\/clients","name":"passport.clients.index","action":"\Laravel\Passport\Http\Controllers\ClientController@forUser"},
                {"host":null,"methods":["POST"],"uri":"oauth\/clients","name":"passport.clients.store","action":"\Laravel\Passport\Http\Controllers\ClientController@store"},
                {"host":null,"methods":["PUT"],"uri":"oauth\/clients\/{client_id}","name":"passport.clients.update","action":"\Laravel\Passport\Http\Controllers\ClientController@update"},
                {"host":null,"methods":["DELETE"],"uri":"oauth\/clients\/{client_id}","name":"passport.clients.destroy","action":"\Laravel\Passport\Http\Controllers\ClientController@destroy"},
                {"host":null,"methods":["GET","HEAD"],"uri":"oauth\/scopes","name":"passport.scopes.index","action":"\Laravel\Passport\Http\Controllers\ScopeController@all"},
                {"host":null,"methods":["GET","HEAD"],"uri":"oauth\/personal-access-tokens","name":"passport.personal.tokens.index","action":"\Laravel\Passport\Http\Controllers\PersonalAccessTokenController@forUser"},
                {"host":null,"methods":["POST"],"uri":"oauth\/personal-access-tokens","name":"passport.personal.tokens.store","action":"\Laravel\Passport\Http\Controllers\PersonalAccessTokenController@store"},
                {"host":null,"methods":["DELETE"],"uri":"oauth\/personal-access-tokens\/{token_id}","name":"passport.personal.tokens.destroy","action":"\Laravel\Passport\Http\Controllers\PersonalAccessTokenController@destroy"},
                {"host":null,"methods":["GET","POST","HEAD"],"uri":"broadcasting\/auth","name":null,"action":"\Illuminate\Broadcasting\BroadcastController@authenticate"},
                {"host":null,"methods":["POST"],"uri":"api\/auth\/userInfoValidate","name":"api.auth.info.validate","action":"AuthController@userInfoValidate"},
                {"host":null,"methods":["POST"],"uri":"api\/auth\/usernameValidate","name":"api.auth.info.validate.username","action":"AuthController@usernameValidate"},
                {"host":null,"methods":["POST"],"uri":"api\/auth\/signup","name":"api.auth.signup","action":"AuthController@signup"},
                {"host":null,"methods":["POST"],"uri":"api\/auth\/login","name":"api.auth.login","action":"AuthController@login"},
                {"host":null,"methods":["GET","HEAD"],"uri":"api\/connect\/redirect\/{service}","name":"api.auth.login.socialite.redirect","action":"AuthController@socialiteLogin"},
                {"host":null,"methods":["GET","HEAD"],"uri":"api\/connect\/callback\/{service}","name":"api.auth.login.socialite.callback","action":"AuthController@socialiteLogin"},
                {"host":null,"methods":["GET","HEAD"],"uri":"api\/settings","name":"api.settings","action":"SettingsController@profile"},
                {"host":null,"methods":["GET","HEAD"],"uri":"api\/settings\/subscription","name":"api.settings.subscription","action":"SettingsController@subscription"},
                {"host":null,"methods":["GET","HEAD"],"uri":"api\/settings\/account","name":"api.settings.account","action":"SettingsController@account"},
                {"host":null,"methods":["GET","HEAD"],"uri":"api\/settings\/password","name":"api.settings.password","action":"SettingsController@password"},
                {"host":null,"methods":["GET","HEAD"],"uri":"api\/settings\/preferences","name":"api.settings.preferences","action":"SettingsController@preferences"},
                {"host":null,"methods":["GET","HEAD"],"uri":"api\/settings\/services","name":"api.settings.services","action":"SettingsController@services"},
                {"host":null,"methods":["POST"],"uri":"api\/auth\/user","name":"api.auth.user","action":"AuthController@user"},
                {"host":null,"methods":["POST"],"uri":"api\/auth\/user\/settings\/profile","name":"api.auth.user.settings.profile","action":"AuthController@settingsProfile"},
                {"host":null,"methods":["POST"],"uri":"api\/auth\/user\/settings\/account","name":"api.auth.user.settings.account","action":"AuthController@settingsAccount"},
                {"host":null,"methods":["POST"],"uri":"api\/auth\/user\/settings\/password","name":"api.auth.user.settings\/password","action":"AuthController@settingsPassword"},
                {"host":null,"methods":["POST"],"uri":"api\/auth\/user\/settings\/preferences","name":"api.auth.user\/settings\/preferences","action":"AuthController@settingsPreferences"},
                {"host":null,"methods":["POST"],"uri":"api\/auth\/user\/notifications","name":"api.auth.user.notifications","action":"AuthController@notifications"},
                {"host":null,"methods":["POST"],"uri":"api\/auth\/user\/notification-count","name":"api.auth.user.notification.count","action":"AuthController@notificationCount"},
                {"host":null,"methods":["POST"],"uri":"api\/auth\/user\/favorite","name":"api.auth.user.favorite","action":"AuthController@favorite"},
                {"host":null,"methods":["POST"],"uri":"api\/auth\/user\/song\/favorite","name":"api.auth.user.song.favorite","action":"AuthController@songFavorite"},
                {"host":null,"methods":["POST"],"uri":"api\/auth\/user\/library","name":"api.auth.user.library","action":"AuthController@library"},
                {"host":null,"methods":["POST"],"uri":"api\/auth\/user\/song\/library","name":"api.auth.user.song.library","action":"AuthController@songLibrary"},
                {"host":null,"methods":["POST"],"uri":"api\/auth\/user\/playlists","name":"api.auth.user.playlists","action":"AuthController@playlists"},
                {"host":null,"methods":["POST"],"uri":"api\/auth\/user\/playlists\/subscribed","name":"api.auth.user.playlists.subscribed","action":"AuthController@subscribed"},
                {"host":null,"methods":["POST"],"uri":"api\/auth\/user\/playlist\/delete","name":"api.auth.user.playlist.delete","action":"AuthController@deletePlaylist"},
                {"host":null,"methods":["POST"],"uri":"api\/auth\/user\/playlist\/edit","name":"api.auth.user.playlist.edit","action":"AuthController@editPlaylist"},
                {"host":null,"methods":["POST"],"uri":"api\/auth\/user\/playlist\/collaboration\/set","name":"api.auth.user.playlist.collaboration.set","action":"AuthController@setPlaylistCollaboration"},
                {"host":null,"methods":["POST"],"uri":"api\/auth\/user\/playlist\/collaboration\/invite","name":"api.auth.user.playlist.collaboration.invite","action":"AuthController@collaborativePlaylist"},
                {"host":null,"methods":["POST"],"uri":"api\/auth\/user\/playlist\/collaboration\/accept","name":"api.auth.user.playlist.collaboration.accept","action":"AuthController@collaborativePlaylist"},
                {"host":null,"methods":["POST"],"uri":"api\/auth\/user\/playlist\/collaboration\/cancel","name":"api.auth.user.playlist.collaboration.cancel","action":"AuthController@collaborativePlaylist"},
                {"host":null,"methods":["POST"],"uri":"api\/auth\/user\/createPlaylist","name":"api.auth.user.create.playlist","action":"AuthController@createPlaylist"},
                {"host":null,"methods":["POST"],"uri":"api\/auth\/user\/addToPlaylist","name":"api.auth.user.playlist.add.item","action":"AuthController@addToPlaylist"},
                {"host":null,"methods":["POST"],"uri":"api\/auth\/user\/removeFromPlaylist","name":"api.auth.user.playlist.remove.item","action":"AuthController@removeFromPlaylist"},
                {"host":null,"methods":["POST"],"uri":"api\/auth\/user\/managePlaylist","name":"api.auth.user.playlist.manage","action":"AuthController@managePlaylist"},
                {"host":null,"methods":["POST"],"uri":"api\/auth\/user\/removeActivity","name":"api.auth.user.removeActivity","action":"AuthController@removeActivity"},
                {"host":null,"methods":["POST"],"uri":"api\/auth\/user\/artistClaim","name":"api.auth.user.artistClaim","action":"AuthController@artistClaim"},
                {"host":null,"methods":["POST"],"uri":"api\/auth\/user\/checkRole","name":"api.auth.user.role","action":"AuthController@checkRole"},
                {"host":null,"methods":["POST"],"uri":"api\/auth\/user\/subscription\/cancel","name":"api.auth.user.cancel.subscription","action":"AuthController@cancelSubscription"},
                {"host":null,"methods":["POST"],"uri":"api\/auth\/user\/get-mention","name":"api.auth.user.get.mention","action":"AuthController@getMention"},
                {"host":null,"methods":["POST"],"uri":"api\/auth\/user\/get-hashtag","name":"api.auth.user.get.hashtag","action":"AuthController@getHashTag"},
                {"host":null,"methods":["POST"],"uri":"api\/auth\/user\/post-feed","name":"api.auth.user.post.feed","action":"AuthController@postFeed"},
                {"host":null,"methods":["POST"],"uri":"api\/auth\/user\/report","name":"api.auth.user.report","action":"AuthController@report"},
                {"host":null,"methods":["GET","HEAD"],"uri":"api\/upload","name":"api.auth.upload","action":"UploadController@index"},
                {"host":null,"methods":["POST"],"uri":"api\/upload","name":"api.auth.upload.post","action":"UploadController@upload"},
                {"host":null,"methods":["GET","HEAD"],"uri":"api\/auth\/logout","name":"api.auth.logout","action":"AuthController@logout"},
                {"host":null,"methods":["GET","HEAD"],"uri":"api\/reset-password\/{token}","name":"api.account.reset.password","action":"AccountController@resetPassword"},
                {"host":null,"methods":["POST"],"uri":"api\/reset-password","name":"api.account.set.new.password","action":"AccountController@setNewPassword"},
                {"host":null,"methods":["GET","HEAD"],"uri":"api\/verify\/{code}","name":"api.account.verify","action":"AccountController@verifyEmail"},
                {"host":null,"methods":["GET","HEAD"],"uri":"api\/album\/{id}","name":"api.album","action":"AlbumController@index"},
                {"host":null,"methods":["GET","HEAD"],"uri":"api\/album\/{id}\/related-albums","name":"api.album.related","action":"AlbumController@related"},
                {"host":null,"methods":["GET","HEAD"],"uri":"api\/artist\/{id}","name":"api.artist","action":"ArtistController@index"},
                {"host":null,"methods":["GET","HEAD"],"uri":"api\/artist\/{id}\/albums","name":"api.artist.albums","action":"ArtistController@albums"},
                {"host":null,"methods":["GET","HEAD"],"uri":"api\/artist\/{id}\/similar-artists","name":"api.artist.similar","action":"ArtistController@similar"},
                {"host":null,"methods":["GET","HEAD"],"uri":"api\/artist\/{id}\/followers","name":"api.artist.followers","action":"ArtistController@followers"},
                {"host":null,"methods":["GET","HEAD"],"uri":"api\/artist\/{id}\/events","name":"api.artist.events","action":"ArtistController@events"},
                {"host":null,"methods":["GET","HEAD"],"uri":"api\/channel\/{id}","name":"api.channel","action":"ChannelController@index"},
                {"host":null,"methods":["GET","HEAD"],"uri":"api\/genre\/{slug}","name":"api.genre","action":"GenreController@index"},
                {"host":null,"methods":["GET","HEAD"],"uri":"api\/genre\/{slug}\/songs","name":"api.genre.songs","action":"GenreController@songs"},
                {"host":null,"methods":["GET","HEAD"],"uri":"api\/genre\/{slug}\/albums","name":"api.genre.albums","action":"GenreController@albums"},
                {"host":null,"methods":["GET","HEAD"],"uri":"api\/genre\/{slug}\/artists","name":"api.genre.artists","action":"GenreController@artists"},
                {"host":null,"methods":["GET","HEAD"],"uri":"api\/genre\/{slug}\/playlists","name":"api.genre.playlists","action":"GenreController@playlists"},
                {"host":null,"methods":["GET","HEAD"],"uri":"api\/homepage","name":"api.homepage","action":"HomeController@index"},
                {"host":null,"methods":["GET","HEAD"],"uri":"api\/community","name":"api.community","action":"CommunityController@index"},
                {"host":null,"methods":["GET","HEAD"],"uri":"api\/discover","name":"api.discover","action":"DiscoverController@index"},
                {"host":null,"methods":["GET","HEAD"],"uri":"api\/trending","name":"api.trending","action":"TrendingController@index"},
                {"host":null,"methods":["GET","HEAD"],"uri":"api\/page\/{id}","name":"api.page","action":"PageController@index"},
                {"host":null,"methods":["GET","HEAD"],"uri":"api\/playlist\/{id}","name":"api.playlist","action":"PlaylistController@index"},
                {"host":null,"methods":["GET","HEAD"],"uri":"api\/playlist\/{id}\/subscribers","name":"api.playlist.subscribers","action":"PlaylistController@subscribers"},
                {"host":null,"methods":["GET","HEAD"],"uri":"api\/playlist\/{id}\/collaborators","name":"api.playlist.collaborators","action":"PlaylistController@collaborators"},
                {"host":null,"methods":["GET","HEAD"],"uri":"api\/radio","name":"api.radio","action":"RadioController@index"},
                {"host":null,"methods":["GET","HEAD"],"uri":"api\/radio\/categories","name":"api.radio.categories","action":"RadioController@categories"},
                {"host":null,"methods":["GET","HEAD"],"uri":"api\/radio\/{id}\/stations","name":"api.radio.station","action":"RadioController@stations"},
                {"host":null,"methods":["GET","HEAD"],"uri":"api\/search\/suggest","name":"api.search.suggest","action":"SearchController@suggest"},
                {"host":null,"methods":["GET","HEAD"],"uri":"api\/search\/song","name":"api.search.song","action":"SearchController@song"},
                {"host":null,"methods":["GET","HEAD"],"uri":"api\/search\/artist","name":"api.search.artist","action":"SearchController@artist"},
                {"host":null,"methods":["GET","HEAD"],"uri":"api\/search\/album","name":"api.search.album","action":"SearchController@album"},
                {"host":null,"methods":["GET","HEAD"],"uri":"api\/search\/playlist","name":"api.search.playlist","action":"SearchController@playlist"},
                {"host":null,"methods":["GET","HEAD"],"uri":"api\/search\/user","name":"api.search.user","action":"SearchController@user"},
                {"host":null,"methods":["GET","HEAD"],"uri":"api\/search\/station","name":"api.search.station","action":"SearchController@station"},
                {"host":null,"methods":["GET","HEAD"],"uri":"api\/song\/{id}","name":"api.song","action":"SongController@index"},
                {"host":null,"methods":["GET","HEAD"],"uri":"api\/songs\/{ids}","name":"api.songs.by.ids","action":"SongController@songFromIds"},
                {"host":null,"methods":["POST"],"uri":"api\/song\/autoplay","name":"api.song.autoplay.get","action":"SongController@autoplay"},
                {"host":null,"methods":["GET","HEAD"],"uri":"api\/stream\/{id}","name":"api.song.stream.mp3","action":"StreamController@mp3"},
                {"host":null,"methods":["GET","HEAD"],"uri":"api\/stream\/hls\/{id}","name":"api.song.stream.hls","action":"StreamController@hls"},
                {"host":null,"methods":["GET","HEAD"],"uri":"api\/stream\/hls\/{id}\/hd","name":"api.song.stream.hls.hd","action":"StreamController@hlsHD"},
                {"host":null,"methods":["POST"],"uri":"api\/stream\/on-track-played\/{id}","name":"api.song.stream.track.played","action":"StreamController@onTrackPlayed"},
                {"host":null,"methods":["GET","HEAD"],"uri":"api\/station\/{id}","name":"api.station","action":"StationController@index"},
                {"host":null,"methods":["GET","HEAD"],"uri":"api\/user\/{id}","name":"api.user","action":"ProfileController@index"},
                {"host":null,"methods":["GET","HEAD"],"uri":"api\/user\/{id}\/recent","name":"api.user.recent","action":"ProfileController@recent"},
                {"host":null,"methods":["GET","HEAD"],"uri":"api\/user\/{id}\/feed","name":"api.user.feed","action":"ProfileController@feed"},
                {"host":null,"methods":["GET","HEAD"],"uri":"api\/user\/{id}\/posts\/{postId}","name":"api.user.posts","action":"ProfileController@posts"},
                {"host":null,"methods":["GET","HEAD"],"uri":"api\/user\/{id}\/collection","name":"api.user.collection","action":"ProfileController@collection"},
                {"host":null,"methods":["GET","HEAD"],"uri":"api\/user\/{id}\/favorites","name":"api.user.favorites","action":"ProfileController@favorites"},
                {"host":null,"methods":["GET","HEAD"],"uri":"api\/user\/{id}\/playlists","name":"api.user.playlists","action":"ProfileController@playlists"},
                {"host":null,"methods":["GET","HEAD"],"uri":"api\/user\/{id}\/playlists\/subscribed","name":"api.user.playlists.subscribed","action":"ProfileController@subscribed"},
                {"host":null,"methods":["GET","HEAD"],"uri":"api\/user\/{id}\/followers","name":"api.user.followers","action":"ProfileController@followers"},
                {"host":null,"methods":["GET","HEAD"],"uri":"api\/user\/{id}\/following","name":"api.user.following","action":"ProfileController@following"},
                {"host":null,"methods":["GET","HEAD"],"uri":"api\/user\/{id}\/notifications","name":"api.user.notifications","action":"ProfileController@notifications"},
                {"host":null,"methods":["GET","HEAD"],"uri":"api\/user\/{id}\/now-playing","name":"api.user.now_playing.post","action":"ProfileController@now_playing"},
                {"host":null,"methods":["POST"],"uri":"reset-password\/send","name":"frontend.account.send.request.reset.password","action":"AccountController@sendResetPassword"},
                {"host":null,"methods":["GET","HEAD"],"uri":"reset-password\/{token}","name":"frontend.account.reset.password","action":"AccountController@resetPassword"},
                {"host":null,"methods":["POST"],"uri":"reset-password\/new-password","name":"frontend.account.set.new.password","action":"AccountController@setNewPassword"},
                {"host":null,"methods":["GET","HEAD"],"uri":"email-verify\/{code}","name":"frontend.account.verify","action":"AccountController@verifyEmail"},
                {"host":null,"methods":["GET","HEAD"],"uri":"album\/{id}\/{slug}","name":"frontend.album","action":"AlbumController@index"},
                {"host":null,"methods":["GET","HEAD"],"uri":"album\/{id}\/{slug}\/related-albums","name":"frontend.album.related","action":"AlbumController@related"},
                {"host":null,"methods":["GET","HEAD"],"uri":"artist\/{id}\/{slug}","name":"frontend.artist","action":"ArtistController@index"},
                {"host":null,"methods":["GET","HEAD"],"uri":"artist\/{id}\/{slug}\/albums","name":"frontend.artist.albums","action":"ArtistController@albums"},
                {"host":null,"methods":["GET","HEAD"],"uri":"artist\/{id}\/{slug}\/similar-artists","name":"frontend.artist.similar","action":"ArtistController@similar"},
                {"host":null,"methods":["GET","HEAD"],"uri":"artist\/{id}\/{slug}\/followers","name":"frontend.artist.followers","action":"ArtistController@followers"},
                {"host":null,"methods":["GET","HEAD"],"uri":"artist\/{id}\/{slug}\/events","name":"frontend.artist.events","action":"ArtistController@events"},
                {"host":null,"methods":["GET","HEAD"],"uri":"artist-management","name":"frontend.auth.user.artist.manager","action":"ArtistManagementController@index"},
                {"host":null,"methods":["GET","HEAD"],"uri":"artist-management\/uploaded","name":"frontend.auth.user.artist.manager.uploaded","action":"ArtistManagementController@uploaded"},
                {"host":null,"methods":["GET","HEAD"],"uri":"artist-management\/albums","name":"frontend.auth.user.artist.manager.albums","action":"ArtistManagementController@albums"},
                {"host":null,"methods":["POST"],"uri":"artist-management\/albums\/create","name":"frontend.auth.user.artist.manager.albums.create","action":"ArtistManagementController@createAlbum"},
                {"host":null,"methods":["POST"],"uri":"artist-management\/albums\/edit","name":"frontend.auth.user.artist.manager.albums.edit","action":"ArtistManagementController@editAlbum"},
                {"host":null,"methods":["POST"],"uri":"artist-management\/albums\/delete","name":"frontend.auth.user.artist.manager.albums.delete","action":"ArtistManagementController@deleteAlbum"},
                {"host":null,"methods":["POST"],"uri":"artist-management\/albums\/sort","name":"frontend.auth.user.artist.manager.albums.sort","action":"ArtistManagementController@sortAlbumSongs"},
                {"host":null,"methods":["GET","HEAD"],"uri":"artist-management\/albums\/{id}","name":"frontend.auth.user.artist.manager.albums.show","action":"ArtistManagementController@showAlbum"},
                {"host":null,"methods":["GET","HEAD"],"uri":"artist-management\/albums\/{id}\/upload","name":"frontend.auth.user.artist.manager.albums.upload","action":"ArtistManagementController@uploadAlbum"},
                {"host":null,"methods":["POST"],"uri":"artist-management\/albums\/{id}\/upload","name":"frontend.auth.user.artist.manager.albums.upload.post","action":"ArtistManagementController@handleUpload"},
                {"host":null,"methods":["GET","HEAD"],"uri":"artist-management\/events","name":"frontend.auth.user.artist.manager.events","action":"ArtistManagementController@events"},
                {"host":null,"methods":["GET","HEAD"],"uri":"artist-management\/profile","name":"frontend.auth.user.artist.manager.profile","action":"ArtistManagementController@profile"},
                {"host":null,"methods":["POST"],"uri":"artist-management\/song\/edit","name":"frontend.auth.user.artist.manager.song.edit.post","action":"ArtistManagementController@editSongPost"},
                {"host":null,"methods":["POST"],"uri":"artist-management\/song\/delete","name":"frontend.auth.user.artist.manager.song.delete","action":"ArtistManagementController@deleteSong"},
                {"host":null,"methods":["POST"],"uri":"artist-management\/event\/create","name":"frontend.auth.user.artist.manager.event.create","action":"ArtistManagementController@createEvent"},
                {"host":null,"methods":["POST"],"uri":"artist-management\/event\/edit","name":"frontend.auth.user.artist.manager.event.edit","action":"ArtistManagementController@editEvent"},
                {"host":null,"methods":["POST"],"uri":"artist-management\/event\/delete","name":"frontend.auth.user.artist.manager.event.delete","action":"ArtistManagementController@deleteEvent"},
                {"host":null,"methods":["POST"],"uri":"artist-management\/profile","name":"frontend.auth.user.artist.manager.profile.save","action":"ArtistManagementController@saveProfile"},
                {"host":null,"methods":["POST"],"uri":"artist-management\/genres","name":"frontend.auth.user.artist.manager.genres","action":"ArtistManagementController@genres"},
                {"host":null,"methods":["POST"],"uri":"artist-management\/moods","name":"frontend.auth.user.artist.manager.moods","action":"ArtistManagementController@moods"},
                {"host":null,"methods":["POST"],"uri":"artist-management\/chart\/overview","name":"frontend.auth.user.artist.manager.chart.overview","action":"ArtistManagementController@artistChart"},
                {"host":null,"methods":["POST"],"uri":"artist-management\/chart\/song\/{id}","name":"frontend.auth.user.artist.manager.chart.song","action":"ArtistManagementController@songChart"},
                {"host":null,"methods":["POST"],"uri":"auth\/userInfoValidate","name":"frontend.auth.info.validate","action":"AuthController@userInfoValidate"},
                {"host":null,"methods":["POST"],"uri":"auth\/usernameValidate","name":"frontend.auth.info.validate.username","action":"AuthController@usernameValidate"},
                {"host":null,"methods":["POST"],"uri":"auth\/signup","name":"frontend.auth.signup","action":"AuthController@signup"},
                {"host":null,"methods":["POST"],"uri":"auth\/login","name":"frontend.auth.login","action":"AuthController@login"},
                {"host":null,"methods":["GET","HEAD"],"uri":"connect\/redirect\/{service}","name":"frontend.auth.login.socialite.redirect","action":"AuthController@socialiteLogin"},
                {"host":null,"methods":["GET","HEAD"],"uri":"connect\/callback\/{service}","name":"frontend.auth.login.socialite.callback","action":"AuthController@socialiteLogin"},
                {"host":null,"methods":["GET","HEAD"],"uri":"settings","name":"frontend.settings","action":"SettingsController@profile"},
                {"host":null,"methods":["GET","HEAD"],"uri":"settings\/subscription","name":"frontend.settings.subscription","action":"SettingsController@subscription"},
                {"host":null,"methods":["GET","HEAD"],"uri":"settings\/account","name":"frontend.settings.account","action":"SettingsController@account"},
                {"host":null,"methods":["GET","HEAD"],"uri":"settings\/password","name":"frontend.settings.password","action":"SettingsController@password"},
                {"host":null,"methods":["GET","HEAD"],"uri":"settings\/preferences","name":"frontend.settings.preferences","action":"SettingsController@preferences"},
                {"host":null,"methods":["GET","HEAD"],"uri":"settings\/services","name":"frontend.settings.services","action":"SettingsController@services"},
                {"host":null,"methods":["POST"],"uri":"auth\/user","name":"frontend.auth.user","action":"AuthController@user"},
                {"host":null,"methods":["POST"],"uri":"auth\/user\/settings\/profile","name":"frontend.auth.user.settings.profile","action":"AuthController@settingsProfile"},
                {"host":null,"methods":["POST"],"uri":"auth\/user\/settings\/account","name":"frontend.auth.user.settings.account","action":"AuthController@settingsAccount"},
                {"host":null,"methods":["POST"],"uri":"auth\/user\/settings\/password","name":"frontend.auth.user.settings\/password","action":"AuthController@settingsPassword"},
                {"host":null,"methods":["POST"],"uri":"auth\/user\/settings\/preferences","name":"frontend.auth.user\/settings\/preferences","action":"AuthController@settingsPreferences"},
                {"host":null,"methods":["POST"],"uri":"auth\/user\/notifications","name":"frontend.auth.user.notifications","action":"AuthController@notifications"},
                {"host":null,"methods":["POST"],"uri":"auth\/user\/notification-count","name":"frontend.auth.user.notification.count","action":"AuthController@notificationCount"},
                {"host":null,"methods":["POST"],"uri":"auth\/user\/favorite","name":"frontend.auth.user.favorite","action":"AuthController@favorite"},
                {"host":null,"methods":["POST"],"uri":"auth\/user\/song\/favorite","name":"frontend.auth.user.song.favorite","action":"AuthController@songFavorite"},
                {"host":null,"methods":["POST"],"uri":"auth\/user\/library","name":"frontend.auth.user.library","action":"AuthController@library"},
                {"host":null,"methods":["POST"],"uri":"auth\/user\/song\/library","name":"frontend.auth.user.song.library","action":"AuthController@songLibrary"},
                {"host":null,"methods":["POST"],"uri":"auth\/user\/playlists","name":"frontend.auth.user.playlists","action":"AuthController@playlists"},
                {"host":null,"methods":["POST"],"uri":"auth\/user\/playlists\/subscribed","name":"frontend.auth.user.playlists.subscribed","action":"AuthController@subscribed"},
                {"host":null,"methods":["POST"],"uri":"auth\/user\/playlist\/delete","name":"frontend.auth.user.playlist.delete","action":"AuthController@deletePlaylist"},
                {"host":null,"methods":["POST"],"uri":"auth\/user\/playlist\/edit","name":"frontend.auth.user.playlist.edit","action":"AuthController@editPlaylist"},
                {"host":null,"methods":["POST"],"uri":"auth\/user\/playlist\/collaboration\/set","name":"frontend.auth.user.playlist.collaboration.set","action":"AuthController@setPlaylistCollaboration"},
                {"host":null,"methods":["POST"],"uri":"auth\/user\/playlist\/collaboration\/invite","name":"frontend.auth.user.playlist.collaboration.invite","action":"AuthController@collaborativePlaylist"},
                {"host":null,"methods":["POST"],"uri":"auth\/user\/playlist\/collaboration\/accept","name":"frontend.auth.user.playlist.collaboration.accept","action":"AuthController@collaborativePlaylist"},
                {"host":null,"methods":["POST"],"uri":"auth\/user\/playlist\/collaboration\/cancel","name":"frontend.auth.user.playlist.collaboration.cancel","action":"AuthController@collaborativePlaylist"},
                {"host":null,"methods":["POST"],"uri":"auth\/user\/createPlaylist","name":"frontend.auth.user.create.playlist","action":"AuthController@createPlaylist"},
                {"host":null,"methods":["POST"],"uri":"auth\/user\/addToPlaylist","name":"frontend.auth.user.playlist.add.item","action":"AuthController@addToPlaylist"},
                {"host":null,"methods":["POST"],"uri":"auth\/user\/removeFromPlaylist","name":"frontend.auth.user.playlist.remove.item","action":"AuthController@removeFromPlaylist"},
                {"host":null,"methods":["POST"],"uri":"auth\/user\/managePlaylist","name":"frontend.auth.user.playlist.manage","action":"AuthController@managePlaylist"},
                {"host":null,"methods":["POST"],"uri":"auth\/user\/removeActivity","name":"frontend.auth.user.removeActivity","action":"AuthController@removeActivity"},
                {"host":null,"methods":["POST"],"uri":"auth\/user\/artistClaim","name":"frontend.auth.user.artistClaim","action":"AuthController@artistClaim"},
                {"host":null,"methods":["POST"],"uri":"auth\/user\/checkRole","name":"frontend.auth.user.role","action":"AuthController@checkRole"},
                {"host":null,"methods":["POST"],"uri":"auth\/user\/subscription\/cancel","name":"frontend.auth.user.cancel.subscription","action":"AuthController@cancelSubscription"},
                {"host":null,"methods":["POST"],"uri":"auth\/user\/get-mention","name":"frontend.auth.user.get.mention","action":"AuthController@getMention"},
                {"host":null,"methods":["POST"],"uri":"auth\/user\/get-hashtag","name":"frontend.auth.user.get.hashtag","action":"AuthController@getHashTag"},
                {"host":null,"methods":["POST"],"uri":"auth\/user\/post-feed","name":"frontend.auth.user.post.feed","action":"AuthController@postFeed"},
                {"host":null,"methods":["POST"],"uri":"auth\/user\/report","name":"frontend.auth.user.report","action":"AuthController@report"},
                {"host":null,"methods":["GET","HEAD"],"uri":"upload","name":"frontend.auth.upload","action":"UploadController@index"},
                {"host":null,"methods":["POST"],"uri":"upload","name":"frontend.auth.upload.post","action":"UploadController@upload"},
                {"host":null,"methods":["GET","HEAD"],"uri":"auth\/logout","name":"frontend.auth.logout","action":"AuthController@logout"},
                {"host":null,"methods":["GET","HEAD"],"uri":"blog","name":"frontend.blog","action":"BlogController@index"},
                {"host":null,"methods":["GET","HEAD"],"uri":"blog\/{category}","name":"frontend.blog.category","action":"BlogController@index"},
                {"host":null,"methods":["GET","HEAD"],"uri":"blog\/tags\/{tag}","name":"frontend.blog.tags","action":"BlogController@index"},
                {"host":null,"methods":["GET","HEAD"],"uri":"blog\/{year}","name":"frontend.blog.browse.by.year","action":"BlogController@index"},
                {"host":null,"methods":["GET","HEAD"],"uri":"blog\/{year}\/{month}","name":"frontend.blog.browse.by.month","action":"BlogController@index"},
                {"host":null,"methods":["GET","HEAD"],"uri":"blog\/{year}\/{month}\/{day}","name":"frontend.blog.browse.by.day","action":"BlogController@index"},
                {"host":null,"methods":["GET","HEAD"],"uri":"post\/{id}\/{slug}","name":"frontend.post","action":"BlogController@show"},
                {"host":null,"methods":["GET","HEAD"],"uri":"download\/post\/{id}\/attachment\/{attachment-id}","name":"frontend.post.download.attachment","action":"BlogController@downloadAttachment"},
                {"host":null,"methods":["GET","HEAD"],"uri":"channel\/{slug}","name":"frontend.channel","action":"ChannelController@index"},
                {"host":null,"methods":["POST"],"uri":"comments\/show","name":"frontend.comments.show","action":"CommentsController@show"},
                {"host":null,"methods":["POST"],"uri":"comments\/get","name":"frontend.comments.get","action":"CommentsController@getComments"},
                {"host":null,"methods":["POST"],"uri":"comments\/replies\/get","name":"frontend.comments.get.replies","action":"CommentsController@getReplies"},
                {"host":null,"methods":["GET","HEAD"],"uri":"comments\/template","name":"frontend.comments.get.comment.template","action":"CommentsController@getCommentTemplate"},
                {"host":null,"methods":["GET","HEAD"],"uri":"comments\/template\/reply","name":"frontend.comments.get.reply.template","action":"CommentsController@getReplyTemplate"},
                {"host":null,"methods":["GET","HEAD"],"uri":"comments\/template\/emoji","name":"frontend.comments.get.emoji.template","action":"CommentsController@getEmojiTemplate"},
                {"host":null,"methods":["POST"],"uri":"comments\/comment\/edit","name":"frontend.comments.edit.comment","action":"CommentsController@editComment"},
                {"host":null,"methods":["POST"],"uri":"comments\/comment\/edit\/save","name":"frontend.comments.save.comment","action":"CommentsController@saveComment"},
                {"host":null,"methods":["POST"],"uri":"comments\/add","name":"frontend.comments.add","action":"CommentsController@add"},
                {"host":null,"methods":["POST"],"uri":"comments\/add\/reply","name":"frontend.comments.reply","action":"CommentsController@reply"},
                {"host":null,"methods":["GET","HEAD"],"uri":"community","name":"frontend.community","action":"CommunityController@index"},
                {"host":null,"methods":["GET","HEAD"],"uri":"discover","name":"frontend.discover","action":"DiscoverController@index"},
                {"host":null,"methods":["POST"],"uri":"visitor\/feedback","name":"frontend.feedback","action":"FeedbackController@index"},
                {"host":null,"methods":["GET","HEAD"],"uri":"discover\/genre\/{slug}","name":"frontend.genre","action":"GenreController@index"},
                {"host":null,"methods":["GET","HEAD"],"uri":"hashtag\/{slug}","name":"frontend.hashtag","action":"HashTagController@index"},
                {"host":null,"methods":["GET","HEAD"],"uri":"hashtag\/{slug}\/latest","name":"frontend.hashtag.latest","action":"HashTagController@index"},
                {"host":null,"methods":["GET","HEAD"],"uri":"\/","name":"frontend.homepage","action":"HomeController@index"},
                {"host":null,"methods":["POST"],"uri":"language\/switch","name":"frontend.language.switch","action":"LanguageController@switchLanguage"},
                {"host":null,"methods":["POST"],"uri":"language\/current","name":"frontend.language.current","action":"LanguageController@currentLanguage"},
                {"host":null,"methods":["GET","HEAD"],"uri":"discover\/mood\/{slug}","name":"frontend.mood","action":"MoodController@index"},
                {"host":null,"methods":["GET","HEAD"],"uri":"queue","name":"frontend.queue","action":"NowPlayingController@index"},
                {"host":null,"methods":["GET","HEAD"],"uri":"page\/{slug}","name":"frontend.page","action":"PageController@index"},
                {"host":null,"methods":["GET","HEAD"],"uri":"subscription\/paypal\/{id}","name":"frontend.paypal.subscription","action":"PaypalController@subscription"},
                {"host":null,"methods":["GET","HEAD"],"uri":"subscription\/paypal\/success\/{id}","name":"frontend.paypal.subscription.success","action":"PaypalController@success"},
                {"host":null,"methods":["GET","HEAD"],"uri":"subscription\/paypal\/cancel\/{id}","name":"frontend.paypal.subscription.cancel","action":"PaypalController@cancel"},
                {"host":null,"methods":["GET","HEAD"],"uri":"playlist\/{id}\/{slug}","name":"frontend.playlist","action":"PlaylistController@index"},
                {"host":null,"methods":["GET","HEAD"],"uri":"playlist\/{id}\/{slug}\/subscribers","name":"frontend.playlist.subscribers","action":"PlaylistController@subscribers"},
                {"host":null,"methods":["GET","HEAD"],"uri":"playlist\/{id}\/{slug}\/collaborators","name":"frontend.playlist.collaborators","action":"PlaylistController@collaborators"},
                {"host":null,"methods":["GET","HEAD"],"uri":"radio","name":"frontend.radio","action":"RadioController@index"},
                {"host":null,"methods":["POST"],"uri":"radio\/data\/city-by-country-code","name":"frontend.radio.get.city","action":"RadioController@cityByCountryCode"},
                {"host":null,"methods":["POST"],"uri":"radio\/data\/language-by-country-code","name":"frontend.radio.get.language","action":"RadioController@languageByCountryCode"},
                {"host":null,"methods":["GET","HEAD"],"uri":"radio\/regions","name":"frontend.radio.browse.regions","action":"RadioController@browse"},
                {"host":null,"methods":["GET","HEAD"],"uri":"radio\/region\/{slug}","name":"frontend.radio.browse.by.region","action":"RadioController@browse"},
                {"host":null,"methods":["GET","HEAD"],"uri":"radio\/languages","name":"frontend.radio.browse.languages","action":"RadioController@browse"},
                {"host":null,"methods":["GET","HEAD"],"uri":"radio\/language\/{id}","name":"frontend.radio.browse.by.language","action":"RadioController@browse"},
                {"host":null,"methods":["GET","HEAD"],"uri":"radio\/countries","name":"frontend.radio.browse.countries","action":"RadioController@browse"},
                {"host":null,"methods":["GET","HEAD"],"uri":"radio\/country\/{code}","name":"frontend.radio.browse.by.country","action":"RadioController@browse"},
                {"host":null,"methods":["GET","HEAD"],"uri":"radio\/city\/{id}","name":"frontend.radio.browse.by.city","action":"RadioController@browse"},
                {"host":null,"methods":["GET","HEAD"],"uri":"radio\/category\/{slug}","name":"frontend.radio.browse.category","action":"RadioController@browse"},
                {"host":null,"methods":["POST"],"uri":"reaction\/react","name":"frontend.reaction.react","action":"ReactionController@react"},
                {"host":null,"methods":["POST"],"uri":"reaction\/revoke","name":"frontend.reaction.react.revoke","action":"ReactionController@revoke"},
                {"host":null,"methods":["GET","HEAD"],"uri":"search\/song\/{slug}","name":"frontend.search.song","action":"SearchController@song"},
                {"host":null,"methods":["GET","HEAD"],"uri":"search\/artist\/{slug}","name":"frontend.search.artist","action":"SearchController@artist"},
                {"host":null,"methods":["GET","HEAD"],"uri":"search\/album\/{slug}","name":"frontend.search.album","action":"SearchController@album"},
                {"host":null,"methods":["GET","HEAD"],"uri":"search\/playlist\/{slug}","name":"frontend.search.playlist","action":"SearchController@playlist"},
                {"host":null,"methods":["GET","HEAD"],"uri":"search\/user\/{slug}","name":"frontend.search.user","action":"SearchController@user"},
                {"host":null,"methods":["GET","HEAD"],"uri":"search\/station\/{slug}","name":"frontend.search.station","action":"SearchController@station"},
                {"host":null,"methods":["GET","HEAD"],"uri":"search\/event\/{slug}","name":"frontend.search.event","action":"SearchController@event"},
                {"host":null,"methods":["GET","HEAD"],"uri":"share\/embed\/{theme}\/{type}\/{id}","name":"frontend.share.embed","action":"ShareController@embed"},
                {"host":null,"methods":["POST"],"uri":"song\/autoplay","name":"frontend.song.autoplay.get","action":"SongController@autoplay"},
                {"host":null,"methods":["GET","HEAD"],"uri":"song\/{id}\/{slug}","name":"frontend.song","action":"SongController@index"},
                {"host":null,"methods":["GET","HEAD"],"uri":"download\/song\/{id}","name":"frontend.song.download","action":"SongController@download"},
                {"host":null,"methods":["GET","HEAD"],"uri":"download\/song\/{id}\/hd","name":"frontend.song.download.hd","action":"SongController@download"},
                {"host":null,"methods":["GET","HEAD"],"uri":"stream\/{id}","name":"frontend.song.stream.mp3","action":"StreamController@mp3"},
                {"host":null,"methods":["GET","HEAD"],"uri":"stream\/{id}\/hd","name":"frontend.song.stream.mp3.hd","action":"StreamController@hdMp3"},
                {"host":null,"methods":["GET","HEAD"],"uri":"stream\/hls\/{id}","name":"frontend.song.stream.hls","action":"StreamController@hls"},
                {"host":null,"methods":["GET","HEAD"],"uri":"stream\/hls\/{id}\/hd","name":"frontend.song.stream.hls.hd","action":"StreamController@hlsHD"},
                {"host":null,"methods":["POST"],"uri":"stream\/on-track-played","name":"frontend.song.stream.track.played","action":"StreamController@onTrackPlayed"},
                {"host":null,"methods":["GET","HEAD"],"uri":"station\/{id}\/{slug}","name":"frontend.station","action":"StationController@index"},
                {"host":null,"methods":["POST"],"uri":"station\/report","name":"frontend.station.report","action":"StationController@report"},
                {"host":null,"methods":["POST"],"uri":"station\/played","name":"frontend.station.played","action":"StationController@report"},
                {"host":null,"methods":["POST"],"uri":"subscription\/stripe","name":"frontend.stripe.subscription","action":"StripeController@subscription"},
                {"host":null,"methods":["GET","HEAD"],"uri":"subscription\/stripe\/success","name":"frontend.stripe.subscription.success","action":"StripeController@success"},
                {"host":null,"methods":["GET","HEAD"],"uri":"subscription\/stripe\/cancel","name":"frontend.stripe.subscription.cancel","action":"StripeController@cancel"},
                {"host":null,"methods":["GET","HEAD"],"uri":"trending","name":"frontend.trending","action":"TrendingController@index"},
                {"host":null,"methods":["GET","HEAD"],"uri":"trending\/week","name":"frontend.trending.week","action":"TrendingController@index"},
                {"host":null,"methods":["GET","HEAD"],"uri":"trending\/month","name":"frontend.trending.month","action":"TrendingController@index"},
                {"host":null,"methods":["GET","HEAD"],"uri":"{username}","name":"frontend.user","action":"ProfileController@index"},
                {"host":null,"methods":["GET","HEAD"],"uri":"{username}\/feed","name":"frontend.user.feed","action":"ProfileController@feed"},
                {"host":null,"methods":["GET","HEAD"],"uri":"{username}\/posts\/{id}","name":"frontend.user.posts","action":"ProfileController@posts"},
                {"host":null,"methods":["GET","HEAD"],"uri":"{username}\/collection","name":"frontend.user.collection","action":"ProfileController@collection"},
                {"host":null,"methods":["GET","HEAD"],"uri":"{username}\/favorites","name":"frontend.user.favorites","action":"ProfileController@favorites"},
                {"host":null,"methods":["GET","HEAD"],"uri":"{username}\/playlists","name":"frontend.user.playlists","action":"ProfileController@playlists"},
                {"host":null,"methods":["GET","HEAD"],"uri":"{username}\/playlists\/subscribed","name":"frontend.user.playlists.subscribed","action":"ProfileController@subscribed"},
                {"host":null,"methods":["GET","HEAD"],"uri":"{username}\/followers","name":"frontend.user.followers","action":"ProfileController@followers"},
                {"host":null,"methods":["GET","HEAD"],"uri":"{username}\/following","name":"frontend.user.following","action":"ProfileController@following"},
                {"host":null,"methods":["GET","HEAD"],"uri":"{username}\/notifications","name":"frontend.user.notifications","action":"ProfileController@notifications"},
                {"host":null,"methods":["GET","HEAD"],"uri":"{username}\/now-playing","name":"frontend.user.now_playing","action":"ProfileController@now_playing"},
                {"host":null,"methods":["POST"],"uri":"{username}\/now-playing","name":"frontend.user.now_playing.post","action":"ProfileController@now_playing"},
                {"host":null,"methods":["GET","HEAD"],"uri":"login","name":"login","action":"App\Http\Controllers\Auth\LoginController@showLoginForm"},
                {"host":null,"methods":["POST"],"uri":"login","name":null,"action":"App\Http\Controllers\Auth\LoginController@login"},
                {"host":null,"methods":["POST"],"uri":"logout","name":"logout","action":"App\Http\Controllers\Auth\LoginController@logout"},
                {"host":null,"methods":["GET","HEAD"],"uri":"register","name":"register","action":"App\Http\Controllers\Auth\RegisterController@showRegistrationForm"},
                {"host":null,"methods":["POST"],"uri":"register","name":null,"action":"App\Http\Controllers\Auth\RegisterController@register"},
                {"host":null,"methods":["GET","HEAD"],"uri":"password\/reset","name":"password.request","action":"App\Http\Controllers\Auth\ForgotPasswordController@showLinkRequestForm"},
                {"host":null,"methods":["POST"],"uri":"password\/email","name":"password.email","action":"App\Http\Controllers\Auth\ForgotPasswordController@sendResetLinkEmail"},
                {"host":null,"methods":["GET","HEAD"],"uri":"password\/reset\/{token}","name":"password.reset","action":"App\Http\Controllers\Auth\ResetPasswordController@showResetForm"},
                {"host":null,"methods":["POST"],"uri":"password\/reset","name":"password.update","action":"App\Http\Controllers\Auth\ResetPasswordController@reset"}],
            prefix: '',

            route : function (name, parameters, route) {
                route = route || this.getByName(name);

                if ( ! route ) {
                    return undefined;
                }

                return this.toRoute(route, parameters);
            },

            url: function (url, parameters) {
                parameters = parameters || [];

                var uri = url + '/' + parameters.join('/');

                return this.getCorrectUrl(uri);
            },

            toRoute : function (route, parameters) {
                var uri = this.replaceNamedParameters(route.uri, parameters);
                var qs  = this.getRouteQueryString(parameters);

                if (this.absolute && this.isOtherHost(route)){
                    return "//" + route.host + "/" + uri + qs;
                }

                return this.getCorrectUrl(uri + qs);
            },

            isOtherHost: function (route){
                return route.host && route.host != window.location.hostname;
            },

            replaceNamedParameters : function (uri, parameters) {
                uri = uri.replace(/\{(.*?)\??\}/g, function(match, key) {
                    if (parameters.hasOwnProperty(key)) {
                        var value = parameters[key];
                        delete parameters[key];
                        return value;
                    } else {
                        return match;
                    }
                });

                // Strip out any optional parameters that were not given
                uri = uri.replace(/\/\{.*?\?\}/g, '');

                return uri;
            },

            getRouteQueryString : function (parameters) {
                var qs = [];
                for (var key in parameters) {
                    if (parameters.hasOwnProperty(key)) {
                        qs.push(key + '=' + parameters[key]);
                    }
                }

                if (qs.length < 1) {
                    return '';
                }

                return '?' + qs.join('&');
            },

            getByName : function (name) {
                for (var key in this.routes) {
                    if (this.routes.hasOwnProperty(key) && this.routes[key].name === name) {
                        return this.routes[key];
                    }
                }
            },

            getByAction : function(action) {
                for (var key in this.routes) {
                    if (this.routes.hasOwnProperty(key) && this.routes[key].action === action) {
                        return this.routes[key];
                    }
                }
            },

            getCorrectUrl: function (uri) {
                var url = this.prefix + '/' + uri.replace(/^\/?/, '');

                if ( ! this.absolute) {
                    return url;
                }

                return this.rootUrl.replace('/\/?$/', '') + url;
            }
        };

        var getLinkAttributes = function(attributes) {
            if ( ! attributes) {
                return '';
            }

            var attrs = [];
            for (var key in attributes) {
                if (attributes.hasOwnProperty(key)) {
                    attrs.push(key + '="' + attributes[key] + '"');
                }
            }

            return attrs.join(' ');
        };

        var getHtmlLink = function (url, title, attributes) {
            title      = title || url;
            attributes = getLinkAttributes(attributes);

            return '<a href="' + url + '" ' + attributes + '>' + title + '</a>';
        };

        return {
            // Generate a url for a given controller action.
            // route.action('HomeController@getIndex', [params = {}])
            action : function (name, parameters) {
                parameters = parameters || {};

                return routes.route(name, parameters, routes.getByAction(name));
            },

            // Generate a url for a given named route.
            // route.route('routeName', [params = {}])
            route : function (route, parameters) {
                parameters = parameters || {};

                return routes.route(route, parameters);
            },

            // Generate a fully qualified URL to the given path.
            // route.route('url', [params = {}])
            url : function (route, parameters) {
                parameters = parameters || {};

                return routes.url(route, parameters);
            },

            rootUrl : function () {
                return routes.rootUrl;
            },
            // Generate a html link to the given url.
            // route.link_to('foo/bar', [title = url], [attributes = {}])
            link_to : function (url, title, attributes) {
                url = this.url(url);

                return getHtmlLink(url, title, attributes);
            },

            // Generate a html link to the given route.
            // route.link_to_route('route.name', [title=url], [parameters = {}], [attributes = {}])
            link_to_route : function (route, title, parameters, attributes) {
                var url = this.route(route, parameters);

                return getHtmlLink(url, title, attributes);
            },

            // Generate a html link to the given controller action.
            // route.link_to_action('HomeController@getIndex', [title=url], [parameters = {}], [attributes = {}])
            link_to_action : function(action, title, parameters, attributes) {
                var url = this.action(action, parameters);

                return getHtmlLink(url, title, attributes);
            }

        };

    }).call(this);

    /**
     * Expose the class either via AMD, CommonJS or the global object
     */
    if (typeof define === 'function' && define.amd) {
        define(function () {
            return laroute;
        });
    }
    else if (typeof module === 'object' && module.exports){
        module.exports = laroute;
    }
    else {
        window.route = laroute;
    }

}).call(this);
