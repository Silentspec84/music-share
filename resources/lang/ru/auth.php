<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Эти учетные данные не соответствуют нашим записям.',
    'throttle' => 'Слишком много попыток входа в систему. Повторите попытку через :seconds секунд.',
    'banned' => "Вы забанены на данном сервере. Причина: :banned_reason. Дата снятия запрета: :banned_time",
    "email_verification_required" => "Пожалуйста, подтвердите свой адрес электронной почты. Мы отправили электронное письмо со ссылкой для подтверждения на ваш адрес электронной почты. Чтобы завершить процесс регистрации, щелкните ссылку подтверждения. Если вы не получили письмо с подтверждением, проверьте папку со спамом.",
    "email_verification" => "Подтверждение Email",
    "email_verification_invalid" => "Извините, недействительная ссылка для подтверждения.",
    "email_verification_verified" => "Вы уже подтвердили этот адрес электронной почты. Повторная проверка не требуется.",
    "email_verification_success" => "Спасибо за подтверждение электронной почты! Ваш электронный адрес подтвержден. Мы автоматически авторизуем Вас через 10 секунд ..."
];
