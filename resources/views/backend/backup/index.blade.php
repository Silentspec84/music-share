@extends('backend.index')
@section('content')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ route('backend.dashboard') }}">Control Panel</a>
        </li>
        <li class="breadcrumb-item active">Backup</li>
    </ol>
    <div class="card shadow output-box">
        <div class="card-header py-3">
            <h6 class="float-left m-0 font-weight-bold text-primary">Existing backups</h6>
            <a class="float-right font-weight-bold text-primary backup-run" href="{{ route('backend.backup-run') }}">Backup</a>
        </div>
        <div class="card-body">
            <table class="table table-striped datatables table-hover">
                <thead>
                    <tr>
                        <th class="th-priority">#</th>
                        <th>Name</th>
                        <th>Disk</th>
                        <th>Reachable</th>
                        <th>Healthy</th>
                        <th># of backups</th>
                        <th>Newest backup</th>
                        <th>Used storage</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($backups as $index => $backup)
                    <tr data-toggle="collapse" data-target="#trace-{{$index+1}}">
                        <td>{{ $index+1 }}.</td>
                        <td>{{ $backup[0] }}</td>
                        <td>{{ $backup[1] }}</td>
                        <td>{{ $backup[2] }}</td>
                        <td>{{ isset($backup[3]) ? $backup[3] : '' }}</td>
                        <td>{{ $backup['amount'] }}</td>
                        <td>{{ strip_tags($backup['newest']) }}</td>
                        <td>{{ $backup['usedStorage'] }}</td>
                    </tr>
                    <tr class="collapse" id="trace-{{$index+1}}">
                        <td colspan="8">
                            <table class="table table-hover">
                                <thead>
                                <tr>
                                    <th>Backup name</th>
                                    <th class="th-2action">Action</th>
                                </tr>
                                <tbody>
                                @foreach($backup['files'] as $file)
                                    <tr>
                                        <td>{{ $file }}</td>
                                        <td>
                                            <a target="_blank" href="{{ route('backend.backup-download', ['disk' => $backup[1], 'file' => $backup[0].'/'.$file]) }}"><i class="fa fa-download"></i></a>
                                            <a href="{{ route('backend.backup-delete', ['disk' => $backup[1], 'file' => $backup[0].'/'.$file]) }}" class="backup-delete"><i class="fa fa-trash"></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>

                            </table>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <div class="card shadow output-box d-none">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Output</h6>
        </div>
        <div class="card-body">
            <pre class="output-body"></pre>
        </div>
    </div>
@endsection