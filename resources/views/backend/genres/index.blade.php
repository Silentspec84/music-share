@extends('backend.index')
@section('content')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ route('backend.dashboard') }}">Control Panel</a>
        </li>
        <li class="breadcrumb-item active">Create and Manage Music Genres</li>
    </ol>
    <div class="row">
        <div class="col-lg-12">
            <a class="btn btn-primary" href="{{ route('backend.genres.add') }}">Add new genre</a>
            <form method="post" action="{{ route('backend.genres.sort.post') }}">
                @csrf
                <table class="mt-4 table table-striped table-sortable">
                    <thead>
                    <tr>
                        <th class="th-handle"></th>
                        <th class="th-priority">Priority</th>
                        <th class="th-wide-image"></th>
                        <th>Name</th>
                        <th class="th-2action">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($genres as $index => $genre)
                        <tr>
                            <td><i class="handle fas fa-fw fa-arrows-alt"></i></td>
                            <td><input type="hidden" name="genreIds[]" value="{{ $genre->id }}"></td>
                            <td><img src="{{ $genre->artwork_url }}"></td>
                            <td><a href="{{ url('admin/genres/edit/' . $genre->id) }}">{{ $genre->name }}</a></td>
                            <td>
                                <a href="{{ url('admin/genres/edit/' . $genre->id) }}" class="row-button edit"><i class="fas fa-fw fa-edit"></i></a>
                                <a href="{{ url('admin/genres/delete/' . $genre->id) }}" onclick="return confirm('Are you sure?')" class="row-button delete"><i class="fas fa-fw fa-trash"></i></a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <button type="submit" class="btn btn-primary mt-4">Save sort order</button>
            </form>
        </div>
    </div>
@endsection