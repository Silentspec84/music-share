@extends('backend.install.index')
@section('content')
<h3 class="text-center">Finally let set your site url</h3>
<form method="POST" action="">
    <div class="form-group">
        <label>Your website full url (with http or https)</label>
        <input class="form-control" name="siteUrl" type="text" value="{{ $request->getSchemeAndHttpHost() }}">
    </div>
    <button class="btn btn-primary btn-block" type="submit">Im's Finished</button>
</form>
@endsection