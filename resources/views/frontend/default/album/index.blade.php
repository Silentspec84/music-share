@extends('index')
@section('content')
    @include('album.nav', ['album' => $album])
    <script>var album_data_{{ $album->id }} = {!! json_encode($album->makeHidden('songs')) !!}</script>
    <div id="page-content" class="bluring-helper">
        <div class="container">
            <div class="blurimg">
                <img src="{{ $album->artwork_url }}" alt="{{ $album->title }}">
            </div>
            <div class="page-header album main medium ">
                <div class="img"> <img id="page-cover-art" src="{{ $album->artwork_url }}" alt="{{ $album->title }}"> </div>
                <div class="inner">
                    <h1 title="{{ $album->title }}">{{ $album->title }}</h1>
                        @if(!$album->visibility)<span class="private" data-translate-text="PRIVATE">{{ __('web.PRIVATE') }}</span>@endif
                    <div class="byline">
                        <span>Album by @foreach($album->artists as $artist)<a href="{{$artist->permalink_url}}" class="artist-link" title="{{ $artist->name }}">{{$artist->name}}</a>@if(!$loop->last), @endif @endforeach</span>
                    </div>
                    <div class="actions-primary">
                        <a class="btn play play-object desktop" data-type="album" data-id="{{ $album->id }}">
                            <svg height="26" viewBox="0 0 24 24" width="18" xmlns="http://www.w3.org/2000/svg"><path d="M8 5v14l11-7z"/><path d="M0 0h24v24H0z" fill="none"/></svg>
                            <span data-translate-text="PLAY_ALBUM">{{ __('web.PLAY_ALBUM') }}</span>
                        </a>
                        <a class="btn share desktop" data-type="album" data-id="{{ $album->id }}">
                            <svg height="26" viewBox="0 0 24 24" width="14" xmlns="http://www.w3.org/2000/svg"><path d="M0 0h24v24H0z" fill="none"/><path d="M18 16.08c-.76 0-1.44.3-1.96.77L8.91 12.7c.05-.23.09-.46.09-.7s-.04-.47-.09-.7l7.05-4.11c.54.5 1.25.81 2.04.81 1.66 0 3-1.34 3-3s-1.34-3-3-3-3 1.34-3 3c0 .24.04.47.09.7L8.04 9.81C7.5 9.31 6.79 9 6 9c-1.66 0-3 1.34-3 3s1.34 3 3 3c.79 0 1.5-.31 2.04-.81l7.12 4.16c-.05.21-.08.43-.08.65 0 1.61 1.31 2.92 2.92 2.92 1.61 0 2.92-1.31 2.92-2.92s-1.31-2.92-2.92-2.92z"/></svg>
                            <span class="desktop" data-translate-text="SHARE">Share</span>
                        </a>

                        <a class="btn fav mobile" data-album-id="{{ $album->id }}">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M0 0h24v24H0z" fill="none"/><path d="M16.5 3c-1.74 0-3.41.81-4.5 2.09C10.91 3.81 9.24 3 7.5 3 4.42 3 2 5.42 2 8.5c0 3.78 3.4 6.86 8.55 11.54L12 21.35l1.45-1.32C18.6 15.36 22 12.28 22 8.5 22 5.42 19.58 3 16.5 3zm-4.4 15.55l-.1.1-.1-.1C7.14 14.24 4 11.39 4 8.5 4 6.5 5.5 5 7.5 5c1.54 0 3.04.99 3.57 2.36h1.87C13.46 5.99 14.96 5 16.5 5c2 0 3.5 1.5 3.5 3.5 0 2.89-3.14 5.74-7.9 10.05z"/></svg>
                        </a>
                        <a class="btn play mobile" data-album-id="{{ $album->id }}">
                            <span data-translate-text="PLAY_ALBUM">{{ __('web.PLAY_ALBUM') }}</span>
                        </a>
                        <a class="btn options mobile" data-toggle="contextmenu" data-trigger="left" data-type="album" data-id="{{ $album->id }}">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M0 0h24v24H0z" fill="none"/><path d="M6 10c-1.1 0-2 .9-2 2s.9 2 2 2 2-.9 2-2-.9-2-2-2zm12 0c-1.1 0-2 .9-2 2s.9 2 2 2 2-.9 2-2-.9-2-2-2zm-6 0c-1.1 0-2 .9-2 2s.9 2 2 2 2-.9 2-2-.9-2-2-2z"/></svg>
                        </a>
                    </div>
                    <ul class="stat-summary">
                        <li>
                            <span id="song-count" class="num">{{ $album->song_count }}</span>
                            <span class="label" data-translate-text="SONGS">Songs</span>
                        </li>
                        @if(! $album->approved)
                            <li>
                                <span class="badge badge-warning">{{ __('web.PREVIEW') }}</span>
                            </li>
                        @endif
                        <li class="tags">
                            @foreach($album->genres as $index => $genre)
                                <a class="genre-link" href="{{ $genre->permalink_url }}"><span class="tag">{{ $genre->name }}</span></a>
                            @endforeach
                        </li>
                    </ul>
                </div>
            </div>
            <div id="column1">
                <div class="content">
                    @include('commons.toolbar.song', ['type' => 'album', 'id' => $album->id])
                    <div id="songs-grid" class="no-album-column profile">
                        @include('commons.song', ['songs' => $album->songs, 'element' => 'genre'])
                    </div>
                    <div class="show-more-songs-wrapper"> <a id="show-more-songs" class="hide" data-translate-text="SHOW_MORE_SONGS">{{ __('web.SHOW_MORE_SONGS') }}</a> </div>
                    <div id="extra-grid" class="no-album-column profile"></div>
                </div>
            </div>
            <div id="column2">
                <div class="content">
                    <div class="sub-header">
                        <h3 data-translate-text="COMMENTS">Comments</h3>
                    </div>
                    <div id="comments">
                        @if(config('settings.album_comments') && $album->allow_comments)
                            @include('comments.index', ['object' => (Object) ['id' => $album->id, 'type' => 'App\Album', 'title' => $album->title]])
                        @else
                            <p class="text-center mt-5">Comments are turned off.</p>
                        @endif
                    </div>
                </div>
                @if(isset($artistTopSongs) && count($artistTopSongs))
                    <div id="topArtistSongs-digest">
                        <div class="sub-header">
                            <h3 data-translate-text="ARTIST_TOP_SONGS">Artist's Top Songs</h3><a href="{{ $artist->permalink_url }}" class="view-more" data-translate-text="SEE_ALL">{{ __('web.SEE_ALL') }}</a></div>
                        <ul class="snapshot">
                            @include('commons.song', ['songs' => $artistTopSongs, 'element' => 'snapshot'])
                        </ul>
                        <div class="divider"></div>
                    </div>
                @endif
                @if(isset($artists) && count($artists))
                    <div id="similarArtists-digest">
                        <div class="sub-header">
                            <h3 data-translate-text="SIMILAR_ARTISTS">Related Artists</h3>
                            <a href="{{ route('frontend.artist.similar', ['id' => $album->artists->first()->id, 'slug' => str_slug($album->artists->first()->name)]) }}" class="view-more" data-translate-text="SEE_ALL">{{ __('web.SEE_ALL') }}</a>
                        </div>
                        <ul class="snapshot">
                            @include('commons.artist', ['artists' => $artists, 'element' => 'search'])
                        </ul>
                    </div>
                @endif
            </div>
            <div id="events-digest"></div>
        </div>
    </div>
@endsection