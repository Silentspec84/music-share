<div id="page-nav">
    <div class="outer">
        <ul>
            <li><a href="{{ route('frontend.auth.user.artist.manager') }}" class="page-nav-link @if(Route::currentRouteName() == 'frontend.auth.user.artist.manager') active @endif" data-translate-text="DASHBOARD">{{ __('web.DASHBOARD') }}<div class="arrow"></div></a></li>
            <li><a href="{{ route('frontend.auth.user.artist.manager.uploaded') }}" class="page-nav-link @if(Route::currentRouteName() == 'frontend.auth.user.artist.manager.uploaded') active @endif" data-translate-text="UPLOADED">{{ __('web.UPLOADED') }}<div class="arrow"></div></a></li>
            <li><a href="{{ route('frontend.auth.user.artist.manager.albums') }}" class="page-nav-link @if(Route::currentRouteName() == 'frontend.auth.user.artist.manager.albums') active @endif" data-translate-text="ALBUMS">{{ __('web.ALBUMS') }}<div class="arrow"></div></a></li>
            <li><a href="{{ route('frontend.auth.user.artist.manager.events') }}" class="page-nav-link @if(Route::currentRouteName() == 'frontend.auth.user.artist.manager.events') active @endif" data-translate-text="EVENTS">{{ __('web.EVENTS') }}<div class="arrow"></div></a></li>
            <li><a href="{{ route('frontend.auth.user.artist.manager.profile') }}" class="page-nav-link @if(Route::currentRouteName() == 'frontend.auth.user.artist.manager.profile') active @endif" data-translate-text="PROFILE">{{ __('web.PROFILE') }}<div class="arrow"></div></a></li>
        </ul>
    </div>
</div>
<script>var artist_data_{{ $artist->id }} = {!! json_encode($artist->makeHidden('songs')->makeHidden('activities')) !!}</script>