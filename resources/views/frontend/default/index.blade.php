<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>{{ MetaTag::get('title') }}</title>
    {!! MetaTag::tag('description') !!}
    {!! MetaTag::tag('keywords') !!}
    {!! MetaTag::get('image') ? MetaTag::tag('image') : '' !!}
    {!! MetaTag::openGraph() !!}
    {!! MetaTag::twitterCard() !!}

    <meta http-equiv="Content-type" content="text/html;charset={{ config('settings.charset', 'utf-8') }}">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1,IE=9,10">

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="index, follow">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <meta name="google-signin-client_id" content="{{ config('settings.google_client_id') }}">
    <link id="favicon" rel="icon" href="{{ asset('skins/default/images/favicon.png') }}" type="image/png" sizes="16x16 32x32">
    <link rel="stylesheet" href="{{ asset('skins/default/css/bootstrap.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('skins/default/css/swiper.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('skins/default/css/select2.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('skins/default/css/jqueryui.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('skins/default/css/app.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('skins/default/css/comments.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('skins/default/css/reaction.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('skins/default/css/jquery-contextmenu.css') }}">
    <link rel="stylesheet" href="{{ asset('skins/default/css/loading.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('skins/default/css/theme.css') }}" type="text/css">
</head>
<body class="@if((isset($_COOKIE['darkMode']) && $_COOKIE['darkMode'] == 'true') || ! isset($_COOKIE['darkMode'])) dark-theme @endif">
<div id="fb-root"></div>
<div id="header-container">
    <div id="logo" class="desktop">
        <a href="{{ route('frontend.homepage') }}" class="logo-link"></a>
    </div>
    <div id="header-search-container" class="desktop">
        <form id="header-search">
            <span class="prediction"></span>
            <input class="search" name="q" value="" autocomplete="off" type="text" placeholder="Search for songs, artists, genres">
            <svg class="icon search" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M15.5 14h-.79l-.28-.27C15.41 12.59 16 11.11 16 9.5 16 5.91 13.09 3 9.5 3S3 5.91 3 9.5 5.91 16 9.5 16c1.61 0 3.09-.59 4.23-1.57l.27.28v.79l5 4.99L20.49 19l-4.99-5zm-6 0C7.01 14 5 11.99 5 9.5S7.01 5 9.5 5 14 7.01 14 9.5 11.99 14 9.5 14z"/><path d="M0 0h24v24H0z" fill="none"/></svg>
        </form>
        <div class="tooltip suggest hide">
            <div class="search-suggest-content-scroll">
                <div id="search-suggest-content-container"></div>
            </div>
        </div>
    </div>
    <div id="header-user-assets" class="session desktop">
        <div id="header-account-group" class="user-asset hide">
            <a id="profile-button" class="">
                <img class="profile-img" width="16" height="16">
                <span class="caret"></span>
            </a>
        </div>
        <a id="header-signup-btn" class="create-account" data-translate-text="BECOME_A_MEMBER">{{ __('web.BECOME_A_MEMBER') }}</a>
        <div id="account-buttons" class="user-asset">
            <div class="btn-group no-border-left">
                <a id="settings-button" class="btn">
                    <svg height="29" viewBox="0 0 24 24" width="15" xmlns="http://www.w3.org/2000/svg">
                        <path d="M0 0h24v24H0z" fill="none"/>
                        <path d="M19.43 12.98c.04-.32.07-.64.07-.98s-.03-.66-.07-.98l2.11-1.65c.19-.15.24-.42.12-.64l-2-3.46c-.12-.22-.39-.3-.61-.22l-2.49 1c-.52-.4-1.08-.73-1.69-.98l-.38-2.65C14.46 2.18 14.25 2 14 2h-4c-.25 0-.46.18-.49.42l-.38 2.65c-.61.25-1.17.59-1.69.98l-2.49-1c-.23-.09-.49 0-.61.22l-2 3.46c-.13.22-.07.49.12.64l2.11 1.65c-.04.32-.07.65-.07.98s.03.66.07.98l-2.11 1.65c-.19.15-.24.42-.12.64l2 3.46c.12.22.39.3.61.22l2.49-1c.52.4 1.08.73 1.69.98l.38 2.65c.03.24.24.42.49.42h4c.25 0 .46-.18.49-.42l.38-2.65c.61-.25 1.17-.59 1.69-.98l2.49 1c.23.09.49 0 .61-.22l2-3.46c.12-.22.07-.49-.12-.64l-2.11-1.65zM12 15.5c-1.93 0-3.5-1.57-3.5-3.5s1.57-3.5 3.5-3.5 3.5 1.57 3.5 3.5-1.57 3.5-3.5 3.5z"/>
                    </svg>
                </a>
                <a id="upload-button" href="{{ route('frontend.auth.upload') }}" class="btn upload-music hide">
                    <svg height="29" viewBox="0 0 24 24" width="15" xmlns="http://www.w3.org/2000/svg">
                        <path d="M0 0h24v24H0z" fill="none"/>
                        <path d="M9 16h6v-6h4l-7-7-7 7h4zm-4 2h14v2H5z"/>
                    </svg>
                </a>
                <a id="notification-button" class="btn hide">
                    <span id="header-notification-pill" class="hide"><span id="header-notification-count">0</span></span>
                    <svg height="29" viewBox="0 0 24 24" width="15" xmlns="http://www.w3.org/2000/svg">
                        <path d="M0 0h24v24H0z" fill="none"/>
                        <path d="M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm-1 17.93c-3.95-.49-7-3.85-7-7.93 0-.62.08-1.21.21-1.79L9 15v1c0 1.1.9 2 2 2v1.93zm6.9-2.54c-.26-.81-1-1.39-1.9-1.39h-1v-3c0-.55-.45-1-1-1H8v-2h2c.55 0 1-.45 1-1V7h2c1.1 0 2-.9 2-2v-.41c2.93 1.19 5 4.06 5 7.41 0 2.08-.8 3.97-2.1 5.39z"/>
                    </svg>
                </a>
            </div>
        </div>
        <a id="header-login-btn" class="login" data-translate-text="SIGN_IN">{{ __('web.SIGN_IN') }}</a>
    </div>

    <!-- mobile nav  -->
    <div id="header-nav-btn" class="mobile">
        <svg class="menu hide" xmlns="http://www.w3.org/2000/svg" width="24px" height="24px" viewBox="0 0 24.75 24.75" xml:space="preserve"><path d="M0,3.875c0-1.104,0.896-2,2-2h20.75c1.104,0,2,0.896,2,2s-0.896,2-2,2H2C0.896,5.875,0,4.979,0,3.875z M22.75,10.375H2c-1.104,0-2,0.896-2,2c0,1.104,0.896,2,2,2h20.75c1.104,0,2-0.896,2-2C24.75,11.271,23.855,10.375,22.75,10.375z M22.75,18.875H2c-1.104,0-2,0.896-2,2s0.896,2,2,2h20.75c1.104,0,2-0.896,2-2S23.855,18.875,22.75,18.875z"></path></svg>
        <svg class="back hide" width="24" height="24" viewBox="0 0 24 24"><path d="M19.7 11H7.5l5.6-5.6L11.7 4l-8 8 8 8 1.4-1.4L7.5 13h12.2z"></path></svg>
    </div>
    <div id="header-nav-logo" class="mobile">
        <svg fill="#e72c30" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32.4 10.9" xml:space="preserve"><g transform="translate(-84 -28)"><g><path class="st0" d="M91.9,29.3c0.4,0.6,0.7,1.6,0.7,2.8c0,0.3,0,0.5,0,0.7l-0.4,6.2h-2.3l0.4-6c0-0.1,0-0.2,0-0.4c0-0.6-0.1-1-0.3-1.3c-0.2-0.3-0.5-0.5-0.9-0.5c-0.5,0-0.9,0.2-1.2,0.6s-0.5,1-0.5,1.8l-0.4,5.8h-2.3l0.4-7.4c0-0.6,0.1-1.2,0.1-2c0-0.4,0-0.8,0-1h2.2l0,1.4c0.3-0.5,0.6-0.9,1.1-1.2c0.5-0.3,1-0.4,1.5-0.4C90.8,28.3,91.5,28.7,91.9,29.3z"/><path class="st0" d="M94.1,38.9l0.6-10.3H97l-0.6,10.3H94.1z"/><path class="st0" d="M105.5,29.3c0.4,0.6,0.7,1.6,0.7,2.8c0,0.3,0,0.5,0,0.7l-0.4,6.2h-2.3l0.4-6c0-0.1,0-0.2,0-0.4c0-0.6-0.1-1-0.3-1.3c-0.2-0.3-0.5-0.5-0.9-0.5c-0.5,0-0.9,0.2-1.2,0.6s-0.5,1-0.5,1.8l-0.4,5.8h-2.3l0.4-7.4c0-0.6,0.1-1.2,0.1-2c0-0.4,0-0.8,0-1h2.2l0,1.4c0.3-0.5,0.6-0.9,1.1-1.2c0.5-0.3,1-0.4,1.5-0.4C104.4,28.3,105.1,28.7,105.5,29.3z"/><path class="st0" d="M116.1,28.6l-0.6,10.3h-2.3l0.1-1.3c-0.3,0.5-0.6,0.8-1,1.1c-0.4,0.3-0.9,0.4-1.4,0.4c-0.7,0-1.2-0.2-1.7-0.6s-0.9-1-1.2-1.7c-0.3-0.7-0.4-1.6-0.4-2.6c0-1.1,0.2-2.1,0.5-3c0.3-0.9,0.7-1.6,1.3-2c0.6-0.5,1.2-0.7,1.9-0.7c0.5,0,1,0.1,1.4,0.4s0.7,0.7,1,1.2l0.1-1.4H116.1z M113,35.9c0.3-0.6,0.5-1.4,0.5-2.3c0-0.9-0.1-1.7-0.4-2.1s-0.7-0.7-1.3-0.7c-0.6,0-1,0.3-1.4,0.9c-0.3,0.6-0.5,1.4-0.5,2.4c0,0.9,0.2,1.6,0.5,2.1s0.7,0.7,1.3,0.7C112.2,36.7,112.7,36.5,113,35.9z"/></g></g></svg>
    </div>
    <div id="header-user-menu" class="mobile">
        <svg class="un-auth" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M12 12c2.21 0 4-1.79 4-4s-1.79-4-4-4-4 1.79-4 4 1.79 4 4 4zm0 2c-2.67 0-8 1.34-8 4v2h16v-2c0-2.66-5.33-4-8-4z"/><path d="M0 0h24v24H0z" fill="none"/></svg>
        <img class="user-auth hide">
    </div>
    <div id="header-settings-menu" class="mobile">
        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 20 20"><path fill="none" d="M0 0h20v20H0V0z"/><path d="M15.95 10.78c.03-.25.05-.51.05-.78s-.02-.53-.06-.78l1.69-1.32c.15-.12.19-.34.1-.51l-1.6-2.77c-.1-.18-.31-.24-.49-.18l-1.99.8c-.42-.32-.86-.58-1.35-.78L12 2.34c-.03-.2-.2-.34-.4-.34H8.4c-.2 0-.36.14-.39.34l-.3 2.12c-.49.2-.94.47-1.35.78l-1.99-.8c-.18-.07-.39 0-.49.18l-1.6 2.77c-.1.18-.06.39.1.51l1.69 1.32c-.04.25-.07.52-.07.78s.02.53.06.78L2.37 12.1c-.15.12-.19.34-.1.51l1.6 2.77c.1.18.31.24.49.18l1.99-.8c.42.32.86.58 1.35.78l.3 2.12c.04.2.2.34.4.34h3.2c.2 0 .37-.14.39-.34l.3-2.12c.49-.2.94-.47 1.35-.78l1.99.8c.18.07.39 0 .49-.18l1.6-2.77c.1-.18.06-.39-.1-.51l-1.67-1.32zM10 13c-1.65 0-3-1.35-3-3s1.35-3 3-3 3 1.35 3 3-1.35 3-3 3z"/></svg>
    </div>
    <div id="header-nav-title" class="mobile"></div>
</div>


<!-- mobile search and side menu  and login box-->

<div id="sticky_header" class="mobile">
    <div class="sticky_wrapper">
        <div class="left_menu">
            <a class="sticky-menu-btn">
                <svg xmlns="http://www.w3.org/2000/svg" width="24px" height="24px" viewBox="0 0 24.75 24.75" xml:space="preserve"><path d="M0,3.875c0-1.104,0.896-2,2-2h20.75c1.104,0,2,0.896,2,2s-0.896,2-2,2H2C0.896,5.875,0,4.979,0,3.875z M22.75,10.375H2c-1.104,0-2,0.896-2,2c0,1.104,0.896,2,2,2h20.75c1.104,0,2-0.896,2-2C24.75,11.271,23.855,10.375,22.75,10.375z M22.75,18.875H2c-1.104,0-2,0.896-2,2s0.896,2,2,2h20.75c1.104,0,2-0.896,2-2S23.855,18.875,22.75,18.875z"></path></svg>
            </a>
        </div>
        <div class="sticky_search">
            <input type="text" id="sticky_search" placeholder="Search songs, artists, playlists.." autocomplete="off">
            <span class="s_icon">
                    <svg class="icon" viewBox="0 0 13.141 14.398" xmlns="http://www.w3.org/2000/svg"><path data-name="Path 144" d="M12.634 14.3a.4.4 0 0 1-.3-.129l-3.58-3.926-.223.172a5.152 5.152 0 0 1-3.068 1.029A5.546 5.546 0 0 1 .1 5.762 5.513 5.513 0 0 1 5.463.1a5.513 5.513 0 0 1 5.363 5.662 5.889 5.889 0 0 1-1.26 3.646l-.183.236 3.535 3.882a.486.486 0 0 1 0 .643.391.391 0 0 1-.284.131zM5.463 1a4.657 4.657 0 0 0-4.51 4.762 4.643 4.643 0 0 0 4.51 4.761 4.644 4.644 0 0 0 4.51-4.761A4.644 4.644 0 0 0 5.463 1z"></path></svg>
                </span>
        </div>
        <div class="right_menu">
            <a id="static-header-user-menu" class="login_icon">
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M12 12c2.21 0 4-1.79 4-4s-1.79-4-4-4-4 1.79-4 4 1.79 4 4 4zm0 2c-2.67 0-8 1.34-8 4v2h16v-2c0-2.66-5.33-4-8-4z"/><path d="M0 0h24v24H0z" fill="none"/></svg>
            </a>
        </div>
    </div>
</div>

<aside id="sideMenu">
    <div id="side-logo" class="mobile">
        <a href="{{ route('frontend.homepage') }}">
            <img src="{{ asset('skins/default/images/favicon.png') }}">
            <span data-translate-text="HOME">{{ __('web.HOME') }}</span>
        </a>
    </div>
    <ul id="aside_ul">
        <li class="side-menu-home desktop">
            <a href="{{ route('frontend.homepage') }}">
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M10 20v-6h4v6h5v-8h3L12 3 2 12h3v8z"/><path d="M0 0h24v24H0z" fill="none"/></svg>
                <span data-translate-text="HOME">{{ __('web.HOME') }}</span>
            </a>
        </li>
        <li class="side-menu-discover">
            <a href="{{ route('frontend.discover') }}">
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M0 0h24v24H0z" fill="none"/><path d="M12 10c-3.86 0-7 3.14-7 7h2c0-2.76 2.24-5 5-5s5 2.24 5 5h2c0-3.86-3.14-7-7-7zm0-4C5.93 6 1 10.93 1 17h2c0-4.96 4.04-9 9-9s9 4.04 9 9h2c0-6.07-4.93-11-11-11z"/></svg>
                <span data-translate-text="DISCOVER">{{ __('web.DISCOVER') }}</span>
            </a>
        </li>
        <li class="side-menu-radio">
            <a href="{{ route('frontend.radio') }}">
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M3.24 6.15C2.51 6.43 2 7.17 2 8v12c0 1.1.89 2 2 2h16c1.11 0 2-.9 2-2V8c0-1.11-.89-2-2-2H8.3l8.26-3.34L15.88 1 3.24 6.15zM7 20c-1.66 0-3-1.34-3-3s1.34-3 3-3 3 1.34 3 3-1.34 3-3 3zm13-8h-2v-2h-2v2H4V8h16v4z"/><path d="M0 0h24v24H0z" fill="none"/></svg>
                <span data-translate-text="RADIO">{{ __('web.RADIO') }}</span>
            </a>
        </li>
        <li class="side-menu-community">
            <a href="{{ route('frontend.community') }}">
                <svg height="24" width="24" viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg"><g><g><path d="m128.636 362.74-.908-.055c-7.128-.432-14.002-1.741-20.53-3.796l-45.624 78.997c-4.18 7.237-1.637 16.493 5.653 20.582l44.91 25.187c7.164 4.018 16.229 1.53 20.336-5.582l66.61-115.333z"/><g><path d="m381.991 28.656-135.527 82.972v238.94l135.698 80.615c10.002 5.942 22.671-1.264 22.671-12.895v-376.84c0-11.721-12.844-18.913-22.842-12.792z"/><g><path d="m48.53 280.042v-100.761c0-5.774.625-11.399 1.782-16.826h-21.816c-15.738 0-28.496 12.754-28.496 28.487v77.439c0 15.733 12.758 28.487 28.496 28.487h21.808c-1.153-5.442-1.774-11.067-1.774-16.826z"/><path d="m216.454 332.74h-86.908c-28.17-1.708-51.006-24.537-51.006-52.698v-100.76c0-28.161 22.836-50.989 51.006-49.28h86.908z"/></g></g></g><g><path d="m470.72 352.291c-3.84 0-7.68-1.465-10.61-4.394l-30.269-30.259c-5.86-5.857-5.86-15.355 0-21.213 5.859-5.857 15.361-5.857 21.22 0l30.269 30.259c5.86 5.857 5.86 15.355 0 21.213-2.93 2.929-6.77 4.394-10.61 4.394z"/><path d="m440.45 172.291c-3.841 0-7.68-1.464-10.61-4.394-5.86-5.857-5.86-15.355 0-21.213l30.269-30.259c5.86-5.858 15.36-5.858 21.22 0 5.859 5.857 5.859 15.355 0 21.213l-30.269 30.259c-2.929 2.929-6.77 4.394-10.61 4.394z"/><path d="m496.995 247.161h-42.807c-8.287 0-15.005-6.716-15.005-15s6.718-15 15.005-15h42.807c8.287 0 15.005 6.716 15.005 15s-6.718 15-15.005 15z"/></g></g></svg>
                <span data-translate-text="COMMUNITY">{{ __('web.COMMUNITY') }}</span>
            </a>
        </li>
        <li class="side-menu-trending">
            <a href="{{ route('frontend.trending') }}">
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M16 6l2.29 2.29-4.88 4.88-4-4L2 16.59 3.41 18l6-6 4 4 6.3-6.29L22 12V6z"/><path d="M0 0h24v24H0z" fill="none"/></svg>
                <span data-translate-text="TRENDING">{{ __('web.TRENDING') }}</span>
            </a>
        </li>
        <li class="side-menu-blog">
            <a href="{{ route('frontend.blog') }}">
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M0 0h24v24H0z" fill="none"/><path d="M4 6H2v14c0 1.1.9 2 2 2h14v-2H4V6zm16-4H8c-1.1 0-2 .9-2 2v12c0 1.1.9 2 2 2h12c1.1 0 2-.9 2-2V4c0-1.1-.9-2-2-2zm-1 9H9V9h10v2zm-4 4H9v-2h6v2zm4-8H9V5h10v2z"/></svg>
                <span data-translate-text="HOME_BLOG">{{ __('web.HOME_BLOG') }}</span>
            </a>
        </li>
    </ul>
    <div id="sidebar" class="scrollable">
        <div id="sidebar-playlists">
            <div id="sidebar-playlists-title" class="sidebar-title">
                <span data-translate-text="PLAYLISTS">{{ __('web.PLAYLISTS') }}</span>
            </div>
            <div id="sidebar-no-playlists" class="hide sidebar-empty">
                <p class="label" data-translate-text="SIDEBAR_NO_PLAYLISTS">{{ __('web.SIDEBAR_NO_PLAYLISTS') }}</p>
                <a class="btn btn-secondary new-playlist create-playlist" data-translate-text="CREATE_A_PLAYLIST">{{ __('web.CREATE_PLAYLIST') }}</a>
            </div>
            <div id="playlists-failed" class="hide">
                <p data-translate-text="POPUP_ERROR_LOAD_PLAYLISTS">{{ __('web.POPUP_ERROR_LOAD_PLAYLISTS') }}</p>
            </div>
            <div id="sidebar-have-playlists">
                <div class="sidebar-item">
                    <a class="sidebar-link create-playlist">
                        <svg class="icon" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 24 24">
                            <path d="M0 0h24v24H0z" fill="none"/>
                            <path d="M14 10H2v2h12v-2zm0-4H2v2h12V6zm4 8v-4h-2v4h-4v2h4v4h2v-4h4v-2h-4zM2 16h8v-2H2v2z"/>
                        </svg>
                        <span data-translate-text="CREATE_PLAYLIST">{{ __('web.CREATE_PLAYLIST') }}</span>
                    </a>
                </div>
                <a class="collapser" data-target="#sidebar-playlists-grid">
                    <span class="caret"></span>
                    <span class="title" data-translate-text="YOURS">{{ __('web.YOURS') }}</span>
                </a>
                <div id="sidebar-playlists-grid" class="collapsable">
                    <!-- sidebar playlist item --->
                    <div class="sidebar-playlist hide">
                        <div class="inner">
                            <div class="icon playlist">
                                <div class="img-container">
                                    <img>
                                </div>
                            </div>
                            <a class="btn play play-object" data-type="playlist">
                                <div class="icon play">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 24 24">
                                        <path d="M8 5v14l11-7z"/>
                                        <path d="M0 0h24v24H0z" fill="none"/>
                                    </svg>
                                </div>
                            </a>
                            <a class="playlist-link">
                                <span class="name"></span>
                            </a>
                        </div>
                    </div>
                </div>
                <a class="collapser" data-target="#sidebar-collab-playlists-grid">
                    <span class="caret"></span>
                    <span class="title" data-translate-text="COLLABORATIVE">{{ __('web.COLLABORATIVE') }}</span>
                </a>
                <div id="sidebar-collab-playlists-grid" class="collapsable"></div>
                <a class="collapser" data-target="#sidebar-subbed-playlists-grid">
                    <span class="caret"></span>
                    <span class="title" data-translate-text="SUBSCRIBED">{{ __('web.SUBSCRIBED') }}</span>
                </a>
                <div id="sidebar-subbed-playlists-grid" class="collapsable"></div>
            </div>
        </div>
    </div>
</aside>
<div class="contact-sidebar">
    <div id="sidebar-community">
        <div class="sidebar-title">
            <span class="community-link" data-translate-text="COMMUNITY">{{ __('web.COMMUNITY') }}</span>
            <span class="drag-handle"></span>
        </div>
        <div id="chat-disconnected" class="hide">
            <p data-translate-text="MANATEE_DISCONNECTED">{{ __('web.MANATEE_DISCONNECTED') }}</p> <a id="manatee-reconnect" class="btn" data-translate-text="MANATEE_RECONNECT">{{ __('web.MANATEE_RECONNECT') }}</a> </div>
        <div id="friends-failed" class="hide">
            <p data-translate-text="POPUP_ERROR_LOAD_FRIENDS">{{ __('web.POPUP_ERROR_LOAD_FRIENDS') }}</p>
        </div>
        <div id="sidebar-friends" class="friend-list hide">

        </div>
        <div id="sidebar-no-friends" class="sidebar-empty">
            <p class="label" data-translate-text="SIDEBAR_NO_FRIENDS">{{ __('web.SIDEBAR_NO_FRIENDS') }}</p>
            <a class="btn btn-secondary share share-profile" data-translate-text="SHARE_YOUR_PROFILE" data-type="user">{{ __('web.SHARE_YOUR_PROFILE') }}</a>
        </div>
        <div id="sidebar-invite-cta" class="hide">
            <p data-translate-text="INVITE_YOUR_FRIENDS">{{ __('web.INVITE_YOUR_FRIENDS') }}</p>
            <a class="btn invite-friends share" data-type="user" data-translate-text="INVITE_FRIENDS">{{ __('web.INVITE_FRIENDS') }}</a>
        </div>
    </div>
</div>
<div id="user-settings-menu" class="mobile">
    <div class="user-section">
        <div class="inner-us">
            <a class="back-arrow ripple-wrap" data-icon="arrowBack">
                <svg width="26" height="26" viewBox="0 0 24 24">
                    <path fill-rule="evenodd" d="M19.7 11H7.5l5.6-5.6L11.7 4l-8 8 8 8 1.4-1.4L7.5 13h12.2z"></path>
                </svg>
            </a>

            <div class="user-subscription-helper after-login hide">
                <div class="user-subscription-text" data-translate-text="USER_SUBSCRIBED_DESCRIPTION">{{ __('web.USER_SUBSCRIBED_DESCRIPTION') }}</div>
                <a href="{{ route('frontend.settings.subscription') }}" class="user-subscription-button" data-translate-text="SUBSCRIBE">{{ __('web.SUBSCRIBE') }}</a>
            </div>

            <div class="user-auth user-info hide">
                <div class="info-profile">
                    <p class="info-name"></p>
                    <a class="info-link">View Profile</a>
                </div>
                <a class="info-artwork">
                    <img>
                </a>
            </div>
            <div class="setting-wrap separate">
                <ul class="user_options">
                    <li>
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M10 20v-6h4v6h5v-8h3L12 3 2 12h3v8z"/><path d="M0 0h24v24H0z" fill="none"/></svg>
                        <a href="{{ route('frontend.homepage') }}" data-translate-text="HOME">{{ __('web.HOME') }}</a>
                    </li>
                    <li>
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M0 0h24v24H0z" fill="none"/><path d="M11.99 2C6.47 2 2 6.48 2 12s4.47 10 9.99 10C17.52 22 22 17.52 22 12S17.52 2 11.99 2zm6.93 6h-2.95c-.32-1.25-.78-2.45-1.38-3.56 1.84.63 3.37 1.91 4.33 3.56zM12 4.04c.83 1.2 1.48 2.53 1.91 3.96h-3.82c.43-1.43 1.08-2.76 1.91-3.96zM4.26 14C4.1 13.36 4 12.69 4 12s.1-1.36.26-2h3.38c-.08.66-.14 1.32-.14 2 0 .68.06 1.34.14 2H4.26zm.82 2h2.95c.32 1.25.78 2.45 1.38 3.56-1.84-.63-3.37-1.9-4.33-3.56zm2.95-8H5.08c.96-1.66 2.49-2.93 4.33-3.56C8.81 5.55 8.35 6.75 8.03 8zM12 19.96c-.83-1.2-1.48-2.53-1.91-3.96h3.82c-.43 1.43-1.08 2.76-1.91 3.96zM14.34 14H9.66c-.09-.66-.16-1.32-.16-2 0-.68.07-1.35.16-2h4.68c.09.65.16 1.32.16 2 0 .68-.07 1.34-.16 2zm.25 5.56c.6-1.11 1.06-2.31 1.38-3.56h2.95c-.96 1.65-2.49 2.93-4.33 3.56zM16.36 14c.08-.66.14-1.32.14-2 0-.68-.06-1.34-.14-2h3.38c.16.64.26 1.31.26 2s-.1 1.36-.26 2h-3.38z"/></svg>
                        <a class="show-lightbox" data-lightbox="lightbox-locale" data-translate-text="LANGUAGE">{{ __('web.LANGUAGE') }}</a>
                    </li>
                    <li>
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M20 4H4c-1.1 0-1.99.9-1.99 2L2 18c0 1.1.9 2 2 2h16c1.1 0 2-.9 2-2V6c0-1.1-.9-2-2-2zm0 4l-8 5-8-5V6l8 5 8-5v2z"/><path d="M0 0h24v24H0z" fill="none"/></svg>
                        <a class="show-lightbox" data-lightbox="lightbox-feedback" data-translate-text="FEEDBACK">{{ __('web.FEEDBACK') }}</a>
                    </li>
                    <li>
                        <span class="th-ic _ic" data-icon="theme_icon">
                            <svg xmlns="http://www.w3.org/2000/svg" width="22" height="22" viewBox="0 0 24 24"><path d="M9 2c-1.05 0-2.05.16-3 .46 4.06 1.27 7 5.06 7 9.54 0 4.48-2.94 8.27-7 9.54.95.3 1.95.46 3 .46 5.52 0 10-4.48 10-10S14.52 2 9 2z"/><path d="M0 0h24v24H0z" fill="none"/></svg>
                        </span>
                        <span data-translate-text="BLACK_THEME">{{ __('web.DARK_MODE') }}</span>
                        <label class="switch"><input type="checkbox" class="themeSwitch" @if(isset($_COOKIE['darkMode']) &&  $_COOKIE['darkMode'] == 'true') checked @endif><span class="slider round"></span></label>
                    </li>
                </ul>
            </div>
            <div class="user-auth hide user-setting-wrap separate">
                <ul class="user_options">
                    <li>
                        <svg height="24" viewBox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                            <path d="M0 0h24v24H0z" fill="none"></path>
                            <path d="M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm-1 17.93c-3.95-.49-7-3.85-7-7.93 0-.62.08-1.21.21-1.79L9 15v1c0 1.1.9 2 2 2v1.93zm6.9-2.54c-.26-.81-1-1.39-1.9-1.39h-1v-3c0-.55-.45-1-1-1H8v-2h2c.55 0 1-.45 1-1V7h2c1.1 0 2-.9 2-2v-.41c2.93 1.19 5 4.06 5 7.41 0 2.08-.8 3.97-2.1 5.39z"></path>
                        </svg>
                        <a class="auth-notifications-link">
                            <span>Notifications</span>
                        </a>
                        <span class="header-notification-count">0</span>
                    </li>
                    <li>
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M0 0h24v24H0z" fill="none"/><path d="M20 2H8c-1.1 0-2 .9-2 2v12c0 1.1.9 2 2 2h12c1.1 0 2-.9 2-2V4c0-1.1-.9-2-2-2zm-2 5h-3v5.5c0 1.38-1.12 2.5-2.5 2.5S10 13.88 10 12.5s1.12-2.5 2.5-2.5c.57 0 1.08.19 1.5.51V5h4v2zM4 6H2v14c0 1.1.9 2 2 2h14v-2H4V6z"/></svg>
                        <a class="auth-my-music-link">My Music</a>
                    </li>
                    <li>
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 512 512" xml:space="preserve"><path d="M497,128.533H15c-8.284,0-15,6.716-15,15V497c0,8.284,6.716,15,15,15h482c8.284,0,15-6.716,15-15V143.533C512,135.249,505.284,128.533,497,128.533z M340.637,332.748l-120.5,80.334c-2.51,1.672-5.411,2.519-8.321,2.519c-2.427,0-4.859-0.588-7.077-1.774c-4.877-2.611-7.922-7.693-7.922-13.226V239.934c0-5.532,3.045-10.615,7.922-13.225c4.879-2.611,10.797-2.324,15.398,0.744l120.5,80.334c4.173,2.781,6.68,7.465,6.68,12.48S344.81,329.967,340.637,332.748z"/><path d="M448.801,64.268h-385.6c-8.284,0-15,6.716-15,15s6.716,15,15,15h385.6c8.284,0,15-6.716,15-15S457.085,64.268,448.801,64.268z"/><path d="M400.6,0H111.4c-8.284,0-15,6.716-15,15s6.716,15,15,15h289.2c8.284,0,15-6.716,15-15S408.884,0,400.6,0z"/></svg>
                        <a class="auth-my-playlists-link">My Playlists</a>
                    </li>
                    <li>
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 20 20"><path fill="none" d="M0 0h20v20H0V0z"></path><path d="M15.95 10.78c.03-.25.05-.51.05-.78s-.02-.53-.06-.78l1.69-1.32c.15-.12.19-.34.1-.51l-1.6-2.77c-.1-.18-.31-.24-.49-.18l-1.99.8c-.42-.32-.86-.58-1.35-.78L12 2.34c-.03-.2-.2-.34-.4-.34H8.4c-.2 0-.36.14-.39.34l-.3 2.12c-.49.2-.94.47-1.35.78l-1.99-.8c-.18-.07-.39 0-.49.18l-1.6 2.77c-.1.18-.06.39.1.51l1.69 1.32c-.04.25-.07.52-.07.78s.02.53.06.78L2.37 12.1c-.15.12-.19.34-.1.51l1.6 2.77c.1.18.31.24.49.18l1.99-.8c.42.32.86.58 1.35.78l.3 2.12c.04.2.2.34.4.34h3.2c.2 0 .37-.14.39-.34l.3-2.12c.49-.2.94-.47 1.35-.78l1.99.8c.18.07.39 0 .49-.18l1.6-2.77c.1-.18.06-.39-.1-.51l-1.67-1.32zM10 13c-1.65 0-3-1.35-3-3s1.35-3 3-3 3 1.35 3 3-1.35 3-3 3z"></path></svg>
                        <a href="{{ route('frontend.settings') }}">Settings</a>
                    </li>
                </ul>
            </div>
            <div class="user-auth hide logout-wrap separate">
                <ul class="user_options">
                    <li class="lgout ripple-wrap">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path fill="none" d="M0 0h24v24H0z"/><path d="M13 3h-2v10h2V3zm4.83 2.17l-1.42 1.42C17.99 7.86 19 9.81 19 12c0 3.87-3.13 7-7 7s-7-3.13-7-7c0-2.19 1.01-4.14 2.58-5.42L6.17 5.17C4.23 6.82 3 9.26 3 12c0 4.97 4.03 9 9 9s9-4.03 9-9c0-2.74-1.23-5.18-3.17-6.83z"/></svg>
                        <a id="mobile-user-sign-out">Logout</a>
                    </li>
                </ul>
            </div>
            <div class="un-auth reg-wrap separate">
                <p>Login to make your Collection, Create Playlists and Favourite Songs</p>
                <div id="mobile-reg-btn" class="reg_btn red_btn">Login/ Register</div>
            </div>
        </div>
    </div>
</div>
<div id="main">
    <div id="page">
        @yield('content')
    </div>
</div>

<div id="notifications"></div>
<!-- song tooltip -->



<!-- notification show up when click to world button -->
<div class="tooltip header-notifications hide">
    <div class="header-notifications-scroll">
        <div id="notifications-container"></div>
    </div>
    <a id="see-more-notifications" class="see-all">
        <span class="label" data-translate-text="HEADER_NOTIFICATION_MORE">View All Notifications</span><span class="caret"></span>
    </a>
</div>
<!-- emoji box -->
<div class="tooltip emoji-tooltip hide">
    <div class="emoji">
        <div class="content emojis-scroll"></div>
    </div>
</div>

@include('loading')
@include('lightbox')
@include('jstemplate')

<!-- Third-Party javascript -->
<script>
    var payment_stripe_publishable_key = '{{ config('settings.payment_stripe_publishable_key') }}';
</script>
<script src="https://www.gstatic.com/firebasejs/6.1.0/firebase-app.js"></script>
<script src="https://www.gstatic.com/firebasejs/6.1.0/firebase-database.js"></script>
<script type="text/javascript">
    // Firebase Config
    var config = {
        apiKey: "{{ config('settings.firebase_api_key') }}",
        authDomain: "{{ config('settings.firebase_auth_domain') }}",
        databaseURL: "{{ config('settings.firebase_database_url') }}",
    };
    firebase.initializeApp(config);
</script>
@if(config('settings.payment_stripe'))
    <script src="https://js.stripe.com/v3/" crossorigin="anonymous"></script>
@endif

<!-- polyfill for IE 11 only -->
<script type="text/javascript">
    if(/MSIE \d|Trident.*rv:/.test(navigator.userAgent)) {
        document.write('<script type="text/javascript" src="{{ asset('js/ie/functions.js') }}"><\/script><script type="text/javascript" src="{{ asset('js/ie/polyfill.min.js') }}"><\/script>');
    }
</script>

<script src="{{ asset('js/core.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/utils.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/history.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/route.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/common.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/account.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/comment.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/share.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/nowplaying.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/contextmenu.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/player.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/artist.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/payment.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/artistCore.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/uploadApp.js') }}" type="text/javascript"></script>
<script src="{{ asset('skins/default/js/custom.js') }}" type="text/javascript"></script>
<script src="{{ asset('embed/embed.js?skin=embedplayer10&icon_set=radius') }}" type="text/javascript"></script>

</body>
</html>