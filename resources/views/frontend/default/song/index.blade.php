@extends('index')
@section('content')
    <script>var song_data_{{ $song->id }} = {!! json_encode($song) !!}</script>
    <!--
    @-if(isset($song->artists[0]) &&  auth()->check() && auth()->user()->artist_id == $song->artists[0]->id)
        <div id="artist-management-chart" class="morris-charts">
            <div class="loader">Loading...</div>
        </div>
    @-endif
    -->
    <div id="page-content" class="bluring-helper">
        <div class="container">
            <div class="blurimg">
                <img src="{{ $song->artwork_url }}" alt="{{ $song->title }}">
            </div>
            <div class="page-header main medium song-header">
                <div class="img song" data-id="{{ $song->id }}">
                    <img src="{{ $song->artwork_url }}">
                </div>
                <div class="inner">
                    <h1 title="{{ $song->title }}">{{ $song->title }}</h1>
                    @if(!$song->visibility)<span class="private" data-translate-text="PRIVATE">{{ __('web.PRIVATE') }}</span>@endif
                    <div class="byline">
                        <span>Song by @foreach($song->artists as $artist)<a href="{{$artist->permalink_url}}" class="artist-link" title="{{ $artist->name }}">{{$artist->name}}</a>@if(!$loop->last), @endif @endforeach @if($song->album_name) on <a href="{{ $song->album_url }}" class="album-link">{{ $song->album_name }}</a> @endif </span>
                    </div>
                    <div class="actions-primary">
                        <!-- desktop button -->
                        <div class="btn-group desktop">
                            <a class="btn play play-object" data-type="song" data-id="{{ $song->id }}">
                                <svg height="26" viewBox="0 0 24 24" width="18" xmlns="http://www.w3.org/2000/svg"><path d="M8 5v14l11-7z"/><path d="M0 0h24v24H0z" fill="none"/></svg> <span data-translate-text="PLAY_SONG">{{ __('web.PLAY_SONG') }}</span>
                            </a>
                            <a class="btn play-menu" data-type="song" data-id="{{ $song->id }}" data-target=".song-header">
                                <span class="caret"></span>
                            </a>
                        </div>
                        <div class="btn-group desktop">
                            <a class="btn add-song" data-type="song" data-id="{{ $song->id }}">
                                <svg height="26" viewBox="0 0 24 24" width="18" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M19 13h-6v6h-2v-6H5v-2h6V5h2v6h6v2z"/><path d="M0 0h24v24H0z" fill="none"/>
                                </svg>
                                <span data-translate-text="ADD_SONG">{{ __('web.ADD_SONG') }}</span>
                                <span class="caret"></span>
                            </a>
                        </div>
                        <a class="btn play-station desktop" data-type="song" data-id="{{ $song->id }}">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="26" viewBox="0 0 24 24">
                                <path d="M6.043 19.496l-1.482 1.505c-2.791-2.201-4.561-5.413-4.561-9.001s1.77-6.8 4.561-9l1.482 1.504c-2.326 1.835-3.804 4.512-3.804 7.496s1.478 5.661 3.804 7.496zm.675-7.496c0-1.791.887-3.397 2.282-4.498l-1.481-1.502c-1.86 1.467-3.04 3.608-3.04 6s1.18 4.533 3.04 6l1.481-1.502c-1.396-1.101-2.282-2.707-2.282-4.498zm15.043 0c0-2.984-1.478-5.661-3.804-7.496l1.482-1.504c2.791 2.2 4.561 5.412 4.561 9s-1.77 6.8-4.561 9.001l-1.482-1.505c2.326-1.835 3.804-4.512 3.804-7.496zm-6.761 4.498l1.481 1.502c1.86-1.467 3.04-3.608 3.04-6s-1.18-4.533-3.04-6l-1.481 1.502c1.396 1.101 2.282 2.707 2.282 4.498s-.886 3.397-2.282 4.498zm-3-7.498c-1.656 0-3 1.343-3 3s1.344 3 3 3 3-1.343 3-3-1.344-3-3-3z"/>
                            </svg>
                            <span data-translate-text="START_STATION">{{ __('web.START_STATION') }}</span>
                        </a>
                        <!-- mobile button -->
                        <a class="btn fav favorite mobile @if(auth()->check() && $song->favorite) on @endif" data-type="song" data-id="{{$song->id}}">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M0 0h24v24H0z" fill="none"/><path d="M16.5 3c-1.74 0-3.41.81-4.5 2.09C10.91 3.81 9.24 3 7.5 3 4.42 3 2 5.42 2 8.5c0 3.78 3.4 6.86 8.55 11.54L12 21.35l1.45-1.32C18.6 15.36 22 12.28 22 8.5 22 5.42 19.58 3 16.5 3zm-4.4 15.55l-.1.1-.1-.1C7.14 14.24 4 11.39 4 8.5 4 6.5 5.5 5 7.5 5c1.54 0 3.04.99 3.57 2.36h1.87C13.46 5.99 14.96 5 16.5 5c2 0 3.5 1.5 3.5 3.5 0 2.89-3.14 5.74-7.9 10.05z"/></svg>
                        </a>
                        <a class="btn play play-object mobile" data-type="song" data-id="{{ $song->id }}">
                            <span data-translate-text="PLAY_SONG">{{ __('web.PLAY_SONG') }}</span>
                        </a>
                        <a class="btn options mobile" data-toggle="contextmenu" data-trigger="left" data-type="song" data-id="{{ $song->id }}">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M0 0h24v24H0z" fill="none"/><path d="M6 10c-1.1 0-2 .9-2 2s.9 2 2 2 2-.9 2-2-.9-2-2-2zm12 0c-1.1 0-2 .9-2 2s.9 2 2 2 2-.9 2-2-.9-2-2-2zm-6 0c-1.1 0-2 .9-2 2s.9 2 2 2 2-.9 2-2-.9-2-2-2z"/></svg>
                        </a>
                    </div>
                    <ul class="stat-summary">
                        <li>
                            <span id="fans-count" class="num">@if($song->collectors) {{ $song->collectors }} @else - @endif</span> <span class="label" data-translate-text="FANS">{{ __('web.FANS') }}</span>
                        </li>
                        <li>
                            <span id="listen-count" class="num">@if($song->plays) {{ $song->plays }} @else - @endif</span> <span class="label" data-translate-text="PLAYS">{{ __('web.PLAYS') }}</span>
                        </li>
                        @if(count($song->genres))
                            <li class="tags">
                                @foreach($song->genres as $genre)
                                    <a class="genre-link" href="{{ $genre->permalink_url }}"><span class="tag">{{ $genre->name }}</span></a>
                                @endforeach
                            </li>
                        @endif
                    </ul>
                </div>
            </div>
            <div id="column1">
                <div class="content share-content">
                    <div class="sub-header">
                        <h2 data-translate-text="SHARE_SONG">Share Song</h2>
                    </div>
                    <div class="share-box">
                        <div class="share-box-content">
                            <div class="control">
                                <div class="floated-links">
                                    <label class="control-label" data-translate-text="EMBED">{{ __('web.EMBED') }}</label>
                                    <a class="copy-link" id="copy-song-embed" data-translate-text="SHARE_COPY">Copy</a>
                                </div>
                                <textarea class="select-all widget-code"><iframe width="100%" height="180" frameborder="0" src="{{ route('frontend.share.embed', ['theme' => 'dark', 'type' => 'song', 'id' => $song->id]) }}"></iframe></textarea>
                            </div>
                        </div>
                        <span class="third-party-btn-head" data-translate-text="SHARE_WITH_FRIENDS">{{ __('web.SHARE_WITH_FRIENDS') }}</span>
                        <a class="btn share-btn third-party twitter" target="_blank" href="https://twitter.com/intent/tweet?url={{ $song->permalink_url }}">
                            <svg class="icon" width="24" height="32" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 510 510" xml:space="preserve"><path d="M459,0H51C22.95,0,0,22.95,0,51v408c0,28.05,22.95,51,51,51h408c28.05,0,51-22.95,51-51V51C510,22.95,487.05,0,459,0z M400.35,186.15c-2.55,117.3-76.5,198.9-188.7,204C165.75,392.7,132.6,377.4,102,359.55c33.15,5.101,76.5-7.649,99.45-28.05c-33.15-2.55-53.55-20.4-63.75-48.45c10.2,2.55,20.4,0,28.05,0c-30.6-10.2-51-28.05-53.55-68.85c7.65,5.1,17.85,7.65,28.05,7.65c-22.95-12.75-38.25-61.2-20.4-91.8c33.15,35.7,73.95,66.3,140.25,71.4c-17.85-71.4,79.051-109.65,117.301-61.2c17.85-2.55,30.6-10.2,43.35-15.3c-5.1,17.85-15.3,28.05-28.05,38.25c12.75-2.55,25.5-5.1,35.7-10.2C425.85,165.75,413.1,175.95,400.35,186.15z"></path></svg>
                            <span class="text" data-translate-text="SHARE_ON_TWITTER">{{ __('web.SHARE_ON_TWITTER') }}</span></a>
                        <a class="btn share-btn third-party facebook" target="_blank" href="https://www.facebook.com/share.php?u={{ $song->permalink_url }}&ref=songShare">
                            <svg class="icon" width="16" height="16" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" xml:space="preserve"><path d="M448,0H64C28.704,0,0,28.704,0,64v384c0,35.296,28.704,64,64,64h192V336h-64v-80h64v-64c0-53.024,42.976-96,96-96h64v80h-32c-17.664,0-32-1.664-32,16v64h80l-32,80h-48v176h96c35.296,0,64-28.704,64-64V64C512,28.704,483.296,0,448,0z"></path></svg>
                            <span class="text" data-translate-text="SHARE_ON_FACEBOOK">Share on Facebook</span>
                        </a>
                        <a class="btn share-btn more-menu share" data-type="song" data-id="{{ $song->id }}">
                            <span class="btn-text" data-translate-text="MORE">{{ __('web.MORE') }}</span>
                        </a>
                    </div>
                </div>
                <div class="comments-container">
                    @if(config('settings.song_comments') && $song->allow_comments)
                        <div class="sub-header">
                            <h2 data-translate-text="COMMENTS">Comments</h2>
                        </div>
                        <div id="comments">
                            @include('comments.index', ['object' => (Object) ['id' => $song->id, 'type' => 'App\Song', 'title' => $song->title]])
                        </div>
                    @else
                        <p class="text-center">Comments are turned off.</p>
                    @endif
                </div>
            </div>
            <div id="column2">
                <div id="artist-top-songs" class="content">
                    <div class="sub-header">
                        <h3 data-translate-text="ARTIST_TOP_SONGS">Artist's Top Songs</h3>
                    </div>
                    <ul class="snapshot">
                        @include('commons.song', ['songs' => $related->songs, 'element' => 'snapshot'])
                    </ul>
                    <div class="divider"></div>
                </div>
            </div>
        </div>
    </div>
@endsection