@extends('index')
@section('content')
    @if(\App\Role::getValue('artist_allow_upload'))
        <div id="page-content">
            <form id="fileupload" method="POST" enctype="multipart/form-data">
                <div class="upload-container">
                    <div id="upload-file-button" class="btn btn-primary">
                        <svg class="icon" height="26" width="18" viewBox="0 0 24 24"  xmlns="http://www.w3.org/2000/svg">
                            <path d="M0 0h24v24H0z" fill="none"></path>
                            <path d="M9 16h6v-6h4l-7-7-7 7h4zm-4 2h14v2H5z"></path>
                        </svg>
                        <span data-translate-text="UPLOAD_SONGS">{{ __('web.UPLOAD_SONGS') }}</span>
                        <input id="upload-file-input" type="file" accept="audio/*" name="file" multiple>
                    </div>
                    <p>Browse for files to upload or drag and drop them here</p>
                </div>
                <!--
                <div class="artist-management-chart-block upload">
                    <canvas id="uploadSpeedChart" class="artist-management-chart"></canvas>
                </div>
                -->
            </form>
            <div class="uploaded-files card-columns card-2-columns"></div>
        </div>
        @include('commons.upload-item', ['genres' => $allowGenres, 'moods' => $allowMoods])
    @else
        <div id="page-content">
            <div class="empty-page no-nav">
                <div class="empty-inner">
                    <h2 data-translate-text="PAGE_NOT_FOUND">You don't have permission to upload media!</h2>
                    <p data-translate-text="Try searching below.">Try searching below.</p>
                    <form class="empty-search do-search" data-search-type="song">
                        <input class="search-input" type="text">
                        <div class="icon search">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 24 24">
                                <path d="M15.5 14h-.79l-.28-.27C15.41 12.59 16 11.11 16 9.5 16 5.91 13.09 3 9.5 3S3 5.91 3 9.5 5.91 16 9.5 16c1.61 0 3.09-.59 4.23-1.57l.27.28v.79l5 4.99L20.49 19l-4.99-5zm-6 0C7.01 14 5 11.99 5 9.5S7.01 5 9.5 5 14 7.01 14 9.5 11.99 14 9.5 14z"></path>
                                <path d="M0 0h24v24H0z" fill="none"></path>
                            </svg>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    @endif
@endsection