<?php
/**
 * Created by NiNaCoder.
 * Date: 2019-06-23
 * Time: 09:57
 */

/*
 * Edit Radio
*/

Route::group(['middleware' => 'role:admin_radio'], function() {
    Route::get('countries', 'CountriesController@index')->name('countries');
    Route::get('countries/add', 'CountriesController@add')->name('countries.add');
    Route::post('countries/add', 'CountriesController@savePost')->name('countries.add.post');
    Route::get('countries/{code}/edit', 'CountriesController@edit')->name('countries.edit');
    Route::post('countries/{code}/edit', 'CountriesController@savePost')->name('countries.edit.post');
    Route::get('countries/{code}/delete', 'CountriesController@delete')->name('countries.delete');
});